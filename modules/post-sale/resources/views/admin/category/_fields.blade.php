<ul class="nav nav-tabs scrollable">
    <li class="nav-item">
        <a class="nav-link active save-tab" data-toggle="pill" href="#postsaleCategoryInfo">
            {{ __('post-sale::category.tabs.info') }}
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link save-tab" data-toggle="pill" href="#postsaleCategorySeo">
            {{ __('post-sale::category.tabs.seo') }}
        </a>
    </li>
</ul>

<div class="tab-content mt-3">
    <div class="tab-pane fade show active" id="postsaleCategoryInfo">
        <div class="row">
            <div class="col-12 col-md-9">
                @input(['name' => 'name', 'label' => __('post-sale::category.name')])
                @textarea(['name' => 'description', 'label' => __('post-sale::category.description')])
                @editor(['name' => 'content', 'label' => __('post-sale::category.content')])
            </div>
            <div class="col-12 col-md-3">
                @checkbox(['name' => 'is_active', 'label' => __('post-sale::category.is_active'), 'default' => true])
                @select(['name' => 'parent_id', 'label' => __('post-sale::category.parent'), 'options' => get_post_sale_category_parent_options()])
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="postsaleCategorySeo">
        @seo
    </div>
</div>
