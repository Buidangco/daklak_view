@input(['name' => 'name', 'label' => __('post-sale::status.name')])
@input(['name' => 'sort_order', 'label' => __('post-sale::status.sort_order')])
@checkbox(['name' => 'is_active', 'label' => __('post-sale::status.is_active'), 'default' => true])
@checkbox(['name' => 'is_default', 'label' => __('post-sale::status.is_default')])
