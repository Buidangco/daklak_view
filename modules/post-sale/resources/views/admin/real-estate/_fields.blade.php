<ul class="nav nav-tabs scrollable">
    <li class="nav-item">
        <a class="nav-link active save-tab" data-toggle="pill" href="#postsalePostInfo">
            {{ __('post-sale::real-estate.tabs.info') }}
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link save-tab" data-toggle="pill" href="#postsalePostSeo">
            {{ __('post-sale::real-estate.tabs.seo') }}
        </a>
    </li>
</ul>

<div class="tab-content mt-3">
    <div class="tab-pane fade show active" id="postsalePostInfo">
        <div class="row">
            <div class="col-12 col-md-9">
                @input(['name' => 'name', 'label' => __('post-sale::real-estate.name')])
                @textarea(['name' => 'description', 'label' => __('post-sale::real-estate.description'), 'autoResize' => true])
                @editor(['name' => 'content', 'label' => __('post-sale::real-estate.content')])
                @gallery(['name' => 'images', 'label' => __('post-sale::real-estate.images')])

                @if(object_get($item, 'customer_name'))
                    <h3>{{ __('post-sale::real-estate.tabs.customer') }}</h3>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>{{ __('post-sale::real-estate.customer_name') }}</td>
                            <td>{{ object_get($item, 'customer_name') }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('post-sale::real-estate.customer_phone') }}</td>
                            <td>{{ object_get($item, 'customer_phone') }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('post-sale::real-estate.customer_email') }}</td>
                            <td>{{ object_get($item, 'customer_email') }}</td>
                        </tr>
                        <tr>
                            <td>{{ __('post-sale::real-estate.customer_address') }}</td>
                            <td>{{ object_get($item, 'customer_address') }}</td>
                        </tr>
                        </tbody>
                    </table>
                @endif
            </div>
            <div class="col-12 col-md-3">
                @checkbox(['name' => 'is_active', 'label' => __('post-sale::real-estate.is_active'), 'default' => true])
                @checkbox(['name' => 'is_sticky', 'label' => __('post-sale::real-estate.is_sticky')])
                @select(['name' => 'status_id', 'label' => __('post-sale::real-estate.status'), 'default' => empty($item) ? get_post_sale_default_status_id() : null, 'options' => get_post_sale_status_options()])
                {{--<div class="allway-open-sumoselect">
                    @sumoselect(['name' => 'categories', 'label' => __('post-sale::real-estate.category'), 'multiple' => true, 'options' => get_post_sale_category_parent_options()])
                </div>--}}
                {{--@mediafile(['name' => 'image', 'label' => __('post-sale::real-estate.image')])--}}
                @input(['name' => 'price', 'label' => __('post-sale::real-estate.price'), 'mask' => 'money'])
                @input(['name' => 'area', 'label' => __('post-sale::real-estate.area'), 'helper' => __('post-sale::real-estate.area_helper')])

                <div id="crmCustomerAddress">
                    <input type="hidden" id="old_district_id" value="{{ object_get($item, 'district_id') }}">
                    <input type="hidden" id="old_township_id" value="{{ object_get($item, 'township_id') }}">

                    @select(['name' => 'province_id', 'label' => __('post-sale::real-estate.province'), 'options' => get_zone_provice_options()])
                    @select(['name' => 'district_id', 'label' => __('post-sale::real-estate.district'), 'options' => []])
                    @select(['name' => 'township_id', 'label' => __('post-sale::real-estate.township'), 'options' => []])
                    @input(['name'  => 'street', 'label' => __('post-sale::real-estate.street')])
                </div>
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="postsalePostSeo">
        @seo(['slugPrefix' => config('post_sale.real_estate.slug_prefix')])
    </div>
</div>

@assetadd('zone_address', 'vendor/post-sale/admin/js/zone_address.js', ['jquery'])
