@extends('core::admin.master')

@section('meta_title', __('post-sale::real-estate.index.page_title'))

@section('page_title', __('post-sale::real-estate.index.page_title'))

@section('page_subtitle', __('post-sale::real-estate.index.page_subtitle'))

@section('breadcrumb')
    <nav aria-label="breadcrumb" class="col-sm-4 order-sm-last mb-3 mb-sm-0 p-0 ">
        <ol class="breadcrumb d-inline-flex font-weight-600 fs-13 bg-white mb-0 float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard.index') }}">{{ trans('dashboard::message.index.breadcrumb') }}</a></li>
            <li class="breadcrumb-item active">{{ trans('post-sale::real-estate.index.breadcrumb') }}</li>
        </ol>
    </nav>
@stop

@section('content')
    <div class="card mb-4">
        <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">
                <div>
                    <h6 class="fs-17 font-weight-600 mb-0">
                        {{ __('post-sale::real-estate.index.page_title') }}
                    </h6>
                </div>
                <div class="text-right">
                    <div class="actions">
	                    @admincan('post-sale.admin.real-estate.create')
	                        <a href="{{ route('post-sale.admin.real-estate.create') }}" class="action-item">
	                            <i class="fa fa-plus"></i>
	                            {{ __('core::button.add') }}
	                        </a>
                        @endadmincan
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped table-bordered dt-responsive nowrap bootstrap4-styling">
                <thead>
                <tr>
                    <th>{{ __('ID') }}</th>
                    <th>{{ __('post-sale::real-estate.name') }}</th>
                    <th>{{ __('post-sale::real-estate.price') }}</th>
                    <th>{{ __('post-sale::real-estate.area') }}</th>
                    <th>{{ __('post-sale::real-estate.is_active') }}</th>
                    <th>{{ __('post-sale::real-estate.is_sticky') }}</th>
                    <th>{{ __('post-sale::real-estate.status') }}</th>
                    <th>{{ __('post-sale::real-estate.created_at') }}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>
                            <a href="{{ route('post-sale.admin.real-estate.edit', $item->id) }}">
                                {{ $item->name }}
                            </a>
                            <a href="{{ $item->url }}" target="_blank" title="{{ __('core::button.view') }}">
                                <i class="fas fa-external-link-alt"></i>
                            </a>
                        </td>
                        <td>@vnmoney($item->price)₫</td>
                        <td>{{ $item->area }} ㎡</td>
                        <td>
                            @if($item->is_active)
                                <i class="fas fa-check text-success"></i>
                            @endif
                        </td>
                        <td>
                            @if($item->is_sticky)
                                <i class="fas fa-check text-success"></i>
                            @endif
                        </td>
                        <td>{{ object_get($item, 'status.name') }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td class="text-right">
                        	@admincan('post-sale.admin.real-estate.edit')
	                            <a href="{{ route('post-sale.admin.real-estate.edit', $item->id) }}" class="btn btn-success-soft btn-sm mr-1">
	                                <i class="fas fa-pencil-alt"></i>
	                            </a>
                            @endadmincan

                            @admincan('post-sale.admin.real-estate.destroy')
                            	<table-button-delete url-delete="{{ route('post-sale.admin.real-estate.destroy', $item->id) }}"></table-button-delete>
                            @endadmincan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $items->appends(Request::all())->render() !!}
        </div>
    </div>
@stop
