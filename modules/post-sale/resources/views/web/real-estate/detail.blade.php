@extends('master')

@section('meta_title', object_get($realEstate, 'seometa.title', $realEstate->name))

@section('meta')
    @seometa(['item' => $realEstate])
@stop

@section('body-class', 'real-estate-detail')

@section('content')
    @includeFirst(["post-sale::web.real-estate.{$realEstate->id}", 'post-sale::web.real-estate.content'])
@stop
