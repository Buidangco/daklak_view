<div class="container">
    <h1>{{ $realEstate->name }}</h1>
    <div class="content page-content">
        {!! $realEstate->content !!}
    </div>
    <div class="clearfix"></div>
</div>
