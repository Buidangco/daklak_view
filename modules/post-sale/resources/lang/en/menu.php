<?php
return [
    'post-sale' => [
        'index' => 'Post Sale',
    ],

    'real-estate' => [
        'index' => 'Real Estate',
    ],

    'category' => [
        'index' => 'Category',
    ],

    'status' => [
        'index' => 'Status',
    ],
];
