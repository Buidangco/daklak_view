<?php
return [
    'name'       => 'Name',
    'sort_order' => 'Sort Order',
    'is_active'  => 'Is Active',
    'is_default' => 'Is Default',
    'created_at' => 'Created At',

    'index' => [
        'page_title'    => 'Status',
        'page_subtitle' => 'Status',
        'breadcrumb'    => 'Status',
    ],

    'create' => [
        'page_title'    => 'Add Status',
        'page_subtitle' => 'Add Status',
        'breadcrumb'    => 'Add',
    ],

    'edit' => [
        'page_title'    => 'Edit Status',
        'page_subtitle' => 'Edit Status',
        'breadcrumb'    => 'Edit',
    ],

    'notification' => [
        'created' => 'Status successfully created!',
        'updated' => 'Status successfully updated!',
        'deleted' => 'Status successfully deleted!',
    ],
];
