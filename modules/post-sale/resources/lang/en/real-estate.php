<?php
return [
    'name'             => 'Name',
    'description'      => 'Description',
    'content'          => 'Content',
    'is_active'        => 'Is Active',
    'is_sticky'        => 'Is Sticky',
    'status'           => 'Status',
    'category'         => 'Category',
    'image'            => 'Image',
    'customer_name'    => 'Customer Name',
    'customer_phone'   => 'Customer Phone',
    'customer_email'   => 'Customer Email',
    'customer_address' => 'Customer Address',
    'attached'         => 'Attached',
    'created_at'       => 'Created At',

    'index' => [
        'page_title'    => 'Real Estate',
        'page_subtitle' => 'Real Estate',
        'breadcrumb'    => 'Real Estate',
    ],

    'create' => [
        'page_title'    => 'Add Real Estate',
        'page_subtitle' => 'Add Real Estate',
        'breadcrumb'    => 'Add',
    ],

    'edit' => [
        'page_title'    => 'Edit Real Estate',
        'page_subtitle' => 'Edit Real Estate',
        'breadcrumb'    => 'Edit',
    ],

    'notification' => [
        'created'          => 'Real Estate successfully created!',
        'updated'          => 'Real Estate successfully updated!',
        'deleted'          => 'Real Estate successfully deleted!',
        'frontend-created' => 'Real Estate successfully deleted!',
    ],

    'tabs' => [
        'info'     => 'Information',
        'customer' => 'Customer',
        'seo'      => 'SEO',
    ],
];
