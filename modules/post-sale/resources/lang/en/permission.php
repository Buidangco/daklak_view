<?php
return [
    'real-estate' => [
        'index'   => 'List Real Estate',
        'create'  => 'Create Real Estate',
        'edit'    => 'Edit Real Estate',
        'destroy' => 'Delete Real Estate',
    ],

    'category' => [
        'index'   => 'List Category',
        'create'  => 'Create Category',
        'edit'    => 'Edit Category',
        'destroy' => 'Delete Category',
    ],

    'status' => [
        'index'   => 'List Status',
        'create'  => 'Create Status',
        'edit'    => 'Edit Status',
        'destroy' => 'Delete Status',
    ],
];
