<?php
return [
    'name'        => 'Title',
    'slug'        => 'Slug',
    'description' => 'Description',
    'content'     => 'Content',
    'is_active'   => 'Active',
    'created_at'  => 'Created At',
    'parent'      => 'Parent',

    'index' => [
        'page_title'    => 'Real Estate',
        'page_subtitle' => 'Real Estate',
        'breadcrumb'    => 'Real Estate',
    ],

    'create' => [
        'page_title'    => 'Add Real Estate',
        'page_subtitle' => 'Add Real Estate',
        'breadcrumb'    => 'Add',
    ],

    'edit' => [
        'page_title'    => 'Edit Real Estate',
        'page_subtitle' => 'Edit Real Estate',
        'breadcrumb'    => 'Edit',
    ],

    'notification' => [
        'created' => 'Real Estate successfully created!',
        'updated' => 'Real Estate successfully updated!',
        'deleted' => 'Real Estate successfully deleted!',
    ],

    'tabs' => [
        'info' => 'Information',
        'seo'  => 'SEO',
    ],
];
