<?php
return [
    'name'             => 'Tiêu đề',
    'description'      => 'Mô tả',
    'content'          => 'Nội dung',
    'is_active'        => 'Hiển thị',
    'is_sticky'        => 'Nổi bật',
    'status'           => 'Trạng thái',
    'category'         => 'Danh mục',
    'image'            => 'Hình ảnh',
    'customer_name'    => 'Tên người đăng',
    'customer_phone'   => 'Điện thoại người đăng',
    'customer_email'   => 'Email người đăng',
    'customer_address' => 'Địa chỉ người đăng',
    'attached'         => 'Đính kèm',
    'created_at'       => 'Ngày đăng',
    'images'           => 'Hình ảnh',
    'price'            => 'Giá',
    'area'             => 'Diện tích',
    'area_helper'      => 'Đơn vị ㎡',
    'province'         => 'Tỉnh thành phố',
    'district'         => 'Quận huyện',
    'township'         => 'Xã phường',
    'street'           => 'Số nhà, tên đường',

    'index' => [
        'page_title'    => 'Tin đăng',
        'page_subtitle' => 'Tin đăng',
        'breadcrumb'    => 'Tin đăng',
    ],

    'create' => [
        'page_title'    => 'Thêm Tin đăng',
        'page_subtitle' => 'Thêm Tin đăng',
        'breadcrumb'    => 'Thêm',
    ],

    'edit' => [
        'page_title'    => 'Sửa Tin đăng',
        'page_subtitle' => 'Sửa Tin đăng',
        'breadcrumb'    => 'Sửa',
    ],

    'notification' => [
        'created'          => 'Tin đăng đã được tạo!',
        'updated'          => 'Tin đăng đã được cập nhật!',
        'deleted'          => 'Tin đăng đã xóa!',
        'frontend-created' => 'Tin đăng của bạn đã được tạo!',
    ],

    'tabs' => [
        'info'     => 'Thông tin',
        'customer' => 'Khách hàng',
        'seo'      => 'SEO',
    ],
];
