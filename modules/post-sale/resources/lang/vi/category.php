<?php
return [
    'name'        => 'Tiêu đề',
    'description' => 'Mô tả',
    'content'     => 'Nội dung',
    'is_active'   => 'Bật',
    'created_at'  => 'Ngày tạo',
    'parent'      => 'Mục cha',

    'index' => [
        'page_title'    => 'Danh mục',
        'page_subtitle' => 'Danh mục',
        'breadcrumb'    => 'Danh mục',
    ],

    'create' => [
        'page_title'    => 'Thêm Danh mục',
        'page_subtitle' => 'Thêm Danh mục',
        'breadcrumb'    => 'Thêm',
    ],

    'edit' => [
        'page_title'    => 'Sửa Danh mục',
        'page_subtitle' => 'Sửa Danh mục',
        'breadcrumb'    => 'Sửa',
    ],

    'notification' => [
        'created' => 'Danh mục đã được tạo!',
        'updated' => 'Danh mục đã được cập nhật!',
        'deleted' => 'Danh mục đã xóa!',
    ],

    'tabs' => [
        'info' => 'Thông tin',
        'seo'  => 'SEO',
    ],
];
