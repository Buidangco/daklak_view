<?php
return [
    'post-sale' => [
        'index' => 'Đăng tin mua bán',
    ],

    'real-estate' => [
        'index' => 'Tin đăng',
    ],

    'category' => [
        'index' => 'Danh mục',
    ],

    'status' => [
        'index' => 'Trạng thái',
    ],
];
