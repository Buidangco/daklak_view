<?php
return [
    'name'       => 'Tên',
    'sort_order' => 'Thứ tự',
    'is_active'  => 'Bật',
    'is_default' => 'Mặc đinh',
    'created_at' => 'Ngày tạo',

    'index' => [
        'page_title'    => 'Trạng thái',
        'page_subtitle' => 'Trạng thái',
        'breadcrumb'    => 'Trạng thái',
    ],

    'create' => [
        'page_title'    => 'Thêm Trạng thái',
        'page_subtitle' => 'Thêm Trạng thái',
        'breadcrumb'    => 'Thêm',
    ],

    'edit' => [
        'page_title'    => 'Sửa Trạng thái',
        'page_subtitle' => 'Sửa Trạng thái',
        'breadcrumb'    => 'Sửa',
    ],

    'notification' => [
        'created' => 'Trạng thái đã được tạo!',
        'updated' => 'Trạng thái đã được cập nhật!',
        'deleted' => 'Trạng thái đã xóa!',
    ],
];
