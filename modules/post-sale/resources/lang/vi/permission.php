<?php
return [
    'real-estate' => [
        'index'   => 'List Tin đăng',
        'create'  => 'Create Tin đăng',
        'edit'    => 'Edit Tin đăng',
        'destroy' => 'Delete Tin đăng',
    ],

    'category' => [
        'index'   => 'List Category',
        'create'  => 'Create Category',
        'edit'    => 'Edit Category',
        'destroy' => 'Delete Category',
    ],

    'status' => [
        'index'   => 'List Status',
        'create'  => 'Create Status',
        'edit'    => 'Edit Status',
        'destroy' => 'Delete Status',
    ],
];
