<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostSaleRealEstateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_sale__real_estate_category', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('real_estate_id');
            $table->unsignedBigInteger('category_id');

            $table->foreign('real_estate_id')
                ->references('id')
                ->on('post_sale__real_estates')
                ->cascadeOnDelete();

            $table->foreign('category_id')
                ->references('id')
                ->on('post_sale__categories')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_sale__real_estate_category');
    }
}
