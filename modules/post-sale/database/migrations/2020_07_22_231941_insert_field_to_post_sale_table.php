<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertFieldToPostSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_sale__real_estates', function (Blueprint $table) {
            $table->decimal('price', 20, 2)->nullable();
            $table->decimal('area', 20, 2)->nullable();
            $table->unsignedBigInteger('province_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->unsignedBigInteger('towship_id')->nullable();
            $table->string('type')->nullable();
            $table->timestamp('start_at')->nullable();
            $table->timestamp('stop_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_sale__real_estates', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('area');
            $table->dropColumn('province_id');
            $table->dropColumn('district_id');
            $table->dropColumn('towship_id');
            $table->dropColumn('type');
            $table->dropColumn('start_at');
            $table->dropColumn('stop_at');
        });
    }
}
