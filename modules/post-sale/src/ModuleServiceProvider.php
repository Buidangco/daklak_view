<?php

namespace Modules\PostSale;

use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Support\Facades\Event;
use Modules\PostSale\Models\Category;
use Modules\PostSale\Models\Status;
use Modules\PostSale\Repositories\CategoryRepositoryInterface;
use Modules\PostSale\Repositories\Eloquent\CategoryRepository;
use Modules\PostSale\Repositories\Eloquent\StatusRepository;
use Modules\PostSale\Repositories\StatusRepositoryInterface;
use Newnet\Acl\Facades\Permission;
use Newnet\Core\Facades\AdminMenu;
use Newnet\Core\Support\Module\BaseModuleServiceProvider;
use Modules\PostSale\Repositories\Eloquent\RealEstateRepository;
use Modules\PostSale\Repositories\RealEstateRepositoryInterface;
use Modules\PostSale\Models\RealEstate;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function getModuleNamespace()
    {
        return 'post-sale';
    }

    public function register()
    {
        parent::register();

        $this->app->singleton(RealEstateRepositoryInterface::class, function () {
            return new RealEstateRepository(new RealEstate());
        });

        $this->app->singleton(CategoryRepositoryInterface::class, function () {
            return new CategoryRepository(new Category());
        });

        $this->app->singleton(StatusRepositoryInterface::class, function () {
            return new StatusRepository(new Status());
        });

        require_once __DIR__.'/../helpers/helpers.php';

        $this->mergeConfigFrom(__DIR__.'/../config/post_sale.php', 'post_sale');
    }

    public function registerPermissions()
    {
        Permission::add('post-sale.admin.real-estate.index', __('post-sale::permission.real-estate.index'));
        Permission::add('post-sale.admin.real-estate.create', __('post-sale::permission.real-estate.create'));
        Permission::add('post-sale.admin.real-estate.edit', __('post-sale::permission.real-estate.edit'));
        Permission::add('post-sale.admin.real-estate.destroy', __('post-sale::permission.real-estate.destroy'));

        Permission::add('post-sale.admin.category.index', __('post-sale::permission.category.index'));
        Permission::add('post-sale.admin.category.create', __('post-sale::permission.category.create'));
        Permission::add('post-sale.admin.category.edit', __('post-sale::permission.category.edit'));
        Permission::add('post-sale.admin.category.destroy', __('post-sale::permission.category.destroy'));

        Permission::add('post-sale.admin.status.index', __('post-sale::permission.status.index'));
        Permission::add('post-sale.admin.status.create', __('post-sale::permission.status.create'));
        Permission::add('post-sale.admin.status.edit', __('post-sale::permission.status.edit'));
        Permission::add('post-sale.admin.status.destroy', __('post-sale::permission.status.destroy'));
    }

    public function registerAdminMenus()
    {
        Event::listen(RouteMatched::class, function () {
            AdminMenu::addItem(__('post-sale::menu.post-sale.index'), [
                'id'         => 'post_sale_root',
                'route'      => 'post-sale.admin.real-estate.index',
                'permission' => 'post-sale.admin.real-estate.index',
                'icon'       => 'fad fa-house-night',
                'order'      => 8000,
            ]);

            AdminMenu::addItem(__('post-sale::menu.real-estate.index'), [
                'id'         => 'post_sale_real_estate',
                'parent'     => 'post_sale_root',
                'route'      => 'post-sale.admin.real-estate.index',
                'permission' => 'post-sale.admin.real-estate.index',
                'icon'       => 'far fa-list-alt',
                'order'      => 1,
            ]);

//            AdminMenu::addItem(__('post-sale::menu.category.index'), [
//                'id'         => 'post_sale_category',
//                'parent'     => 'post_sale_root',
//                'route'      => 'post-sale.admin.category.index',
//                'permission' => 'post-sale.admin.category.index',
//                'icon'       => 'fas fa-folder-open',
//                'order'      => 2,
//            ]);

            AdminMenu::addItem(__('post-sale::menu.status.index'), [
                'id'         => 'post_sale_status',
                'parent'     => 'post_sale_root',
                'route'      => 'post-sale.admin.status.index',
                'permission' => 'post-sale.admin.status.index',
                'icon'       => 'fas fa-cogs',
                'order'      => 3,
            ]);
        });
    }
}
