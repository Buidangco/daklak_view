<?php

namespace Modules\PostSale\Http\Controllers\Web;

use Illuminate\Routing\Controller;

class CategoryController extends Controller
{
    public function detail($id)
    {
        return view('post-sale::web.category.detail');
    }
}
