<?php

namespace Modules\PostSale\Http\Controllers\Web;

use Illuminate\Routing\Controller;
use Modules\PostSale\Http\Requests\PostRealEstateRequest;
use Modules\PostSale\Repositories\RealEstateRepositoryInterface;
use Newnet\Media\MediaUploader;

class RealEstateController extends Controller
{
    /**
     * @var RealEstateRepositoryInterface
     */
    private $realEstateRepository;

    public function __construct(RealEstateRepositoryInterface $realEstateRepository)
    {
        $this->realEstateRepository = $realEstateRepository;
    }

    public function detail($id)
    {
        $realEstate = $this->realEstateRepository->findActive($id);

        return view('post-sale::web.real-estate.detail', compact('realEstate'));
    }

    public function create(PostRealEstateRequest $request, MediaUploader $mediaUploader)
    {
        $data = $request->all();

        if ($request->has('images')) {
            $medias = [];
            foreach ($request->file('images') as $item) {
                $media = $mediaUploader->setFile($item)->upload();
                $medias[] = $media->id;
            }

            $data['images'] = $medias;
        }

        $data['content'] = nl2br($request->input('content'));
        $data['status_id'] = get_post_sale_default_status_id();

        if ($price = $request->input('price')) {
            $data['price'] = $price * 1000000;
        }

        $this->realEstateRepository->create($data);

        return redirect()->back()->with('success', __('post-sale::real-estate.notification.frontend-created'));
    }
}
