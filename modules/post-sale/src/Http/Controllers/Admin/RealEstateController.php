<?php

namespace Modules\PostSale\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\PostSale\Http\Requests\RealEstateRequest;
use Modules\PostSale\Repositories\RealEstateRepositoryInterface;

class RealEstateController extends Controller
{
    /**
     * @var RealEstateRepositoryInterface
     */
    private $realEstateRepository;

    public function __construct(RealEstateRepositoryInterface $realEstateRepository)
    {
        $this->realEstateRepository = $realEstateRepository;
    }

    public function index(Request $request)
    {
        $items = $this->realEstateRepository->paginate($request->input('max', 20));

        return view('post-sale::admin.real-estate.index', compact('items'));
    }

    public function create()
    {
        \AdminMenu::activeMenu('post_sale_root');

        return view('post-sale::admin.real-estate.create');
    }

    public function store(RealEstateRequest $request)
    {
        $item = $this->realEstateRepository->create($request->all());

        if ($request->input('continue')) {
            return redirect()
                ->route('post-sale.admin.real-estate.edit', $item->id)
                ->with('success', __('post-sale::real-estate.notification.created'));
        }

        return redirect()
            ->route('post-sale.admin.real-estate.index')
            ->with('success', __('post-sale::real-estate.notification.created'));
    }

    public function edit($id)
    {
        \AdminMenu::activeMenu('post_sale_root');

        $item = $this->realEstateRepository->find($id);

        return view('post-sale::admin.real-estate.edit', compact('item'));
    }

    public function update(RealEstateRequest $request, $id)
    {
        $item = $this->realEstateRepository->updateById($request->all(), $id);

        if ($request->input('continue')) {
            return redirect()
                ->route('post-sale.admin.real-estate.edit', $item->id)
                ->with('success', __('post-sale::real-estate.notification.updated'));
        }

        return redirect()
            ->route('post-sale.admin.real-estate.index')
            ->with('success', __('post-sale::real-estate.notification.updated'));
    }

    public function destroy($id, Request $request)
    {
        $this->realEstateRepository->delete($id);

        if ($request->wantsJson()) {
            Session::flash('success', __('post-sale::real-estate.notification.deleted'));
            return response()->json([
                'success' => true,
            ]);
        }

        return redirect()
            ->route('post-sale.admin.real-estate.index')
            ->with('success', __('post-sale::real-estate.notification.deleted'));
    }
}
