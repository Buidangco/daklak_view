<?php

namespace Modules\PostSale\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\PostSale\Http\Requests\StatusRequest;
use Modules\PostSale\Repositories\StatusRepositoryInterface;

class StatusController extends Controller
{
    /**
     * @var StatusRepositoryInterface
     */
    private $statusRepository;

    public function __construct(StatusRepositoryInterface $statusRepository)
    {
        $this->statusRepository = $statusRepository;
    }

    public function index(Request $request)
    {
        $items = $this->statusRepository->paginate($request->input('max', 20));

        return view('post-sale::admin.status.index', compact('items'));
    }

    public function create()
    {
        \AdminMenu::activeMenu('post_sale_root');

        return view('post-sale::admin.status.create');
    }

    public function store(StatusRequest $request)
    {
        $item = $this->statusRepository->create($request->all());

        if ($request->input('continue')) {
            return redirect()
                ->route('post-sale.admin.status.edit', $item->id)
                ->with('success', __('post-sale::status.notification.created'));
        }

        return redirect()
            ->route('post-sale.admin.status.index')
            ->with('success', __('post-sale::status.notification.created'));
    }

    public function edit($id)
    {
        \AdminMenu::activeMenu('post_sale_root');

        $item = $this->statusRepository->find($id);

        return view('post-sale::admin.status.edit', compact('item'));
    }

    public function update(StatusRequest $request, $id)
    {
        $item = $this->statusRepository->updateById($request->all(), $id);

        if ($request->input('continue')) {
            return redirect()
                ->route('post-sale.admin.status.edit', $item->id)
                ->with('success', __('post-sale::status.notification.updated'));
        }

        return redirect()
            ->route('post-sale.admin.status.index')
            ->with('success', __('post-sale::status.notification.updated'));
    }

    public function destroy($id, Request $request)
    {
        $this->statusRepository->delete($id);

        if ($request->wantsJson()) {
            Session::flash('success', __('post-sale::status.notification.deleted'));
            return response()->json([
                'success' => true,
            ]);
        }

        return redirect()
            ->route('post-sale.admin.status.index')
            ->with('success', __('post-sale::status.notification.deleted'));
    }
}
