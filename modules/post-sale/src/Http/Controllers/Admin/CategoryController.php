<?php

namespace Modules\PostSale\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\PostSale\Http\Requests\CategoryRequest;
use Modules\PostSale\Models\Category;
use Modules\PostSale\Repositories\CategoryRepositoryInterface;
use Modules\PostSale\Repositories\Eloquent\CategoryRepository;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface|CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        $items = $this->categoryRepository->paginateTree($request->input('max', 20));

        return view('post-sale::admin.category.index', compact('items'));
    }

    public function create(Request $request)
    {
        \AdminMenu::activeMenu('post_sale_root');

        $item = new Category();
        $item->is_active = true;
        $item->parent_id = $request->input('parent_id');

        return view('post-sale::admin.category.create', compact('item'));
    }

    public function store(CategoryRequest $request)
    {
        $item = $this->categoryRepository->create($request->all());

        if ($request->input('continue')) {
            return redirect()
                ->route('post-sale.admin.category.edit', $item->id)
                ->with('success', __('post-sale::category.notification.created'));
        }

        return redirect()
            ->route('post-sale.admin.category.index')
            ->with('success', __('post-sale::category.notification.created'));
    }

    public function edit($id)
    {
        \AdminMenu::activeMenu('post_sale_root');

        $item = $this->categoryRepository->find($id);

        return view('post-sale::admin.category.edit', compact('item'));
    }

    public function update($id, CategoryRequest $request)
    {
        $item = $this->categoryRepository->updateById($request->all(), $id);

        if ($request->input('continue')) {
            return redirect()
                ->route('post-sale.admin.category.edit', $item->id)
                ->with('success', __('post-sale::category.notification.created'));
        }

        return redirect()
            ->route('post-sale.admin.category.index')
            ->with('success', __('post-sale::category.notification.updated'));
    }

    public function moveUp($id)
    {
        $this->categoryRepository->moveUp($id);

        return redirect()
            ->route('post-sale.admin.category.index')
            ->with('success', __('post-sale::category.notification.updated'));
    }

    public function moveDown($id)
    {
        $this->categoryRepository->moveDown($id);

        return redirect()
            ->route('post-sale.admin.category.index')
            ->with('success', __('post-sale::category.notification.updated'));
    }

    public function destroy($id, Request $request)
    {
        $this->categoryRepository->delete($id);

        if ($request->wantsJson()) {
            Session::flash('success', __('post-sale::category.notification.deleted'));
            return response()->json([
                'success' => true,
            ]);
        }

        return redirect()
            ->route('post-sale.admin.category.index')
            ->with('success', __('post-sale::category.notification.deleted'));
    }
}
