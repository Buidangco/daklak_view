<?php

namespace Modules\PostSale\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRealEstateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required',
            'customer_name'  => 'required',
            'customer_phone' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name'           => __('post-sale::real-estate.name'),
            'customer_name'  => __('post-sale::real-estate.customer_name'),
            'customer_phone' => __('post-sale::real-estate.customer_phone'),
            'attached'       => __('post-sale::real-estate.attached'),
        ];
    }
}
