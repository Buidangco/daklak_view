<?php

namespace Modules\PostSale\Repositories;

use Newnet\Core\Repositories\BaseRepositoryInterface;

interface RealEstateRepositoryInterface extends BaseRepositoryInterface
{
    public function findActive($id);
}
