<?php

namespace Modules\PostSale\Repositories\Eloquent;

use Modules\PostSale\Repositories\StatusRepositoryInterface;
use Newnet\Core\Repositories\BaseRepository;

class StatusRepository extends BaseRepository implements StatusRepositoryInterface
{
    public function create(array $data)
    {
        if (!empty($data['is_default'])) {
            $this->model
                ->where('is_default', 1)
                ->update([
                    'is_default' => false,
                ]);
        }

        return parent::create($data);
    }

    public function updateById(array $data, $id)
    {
        if (!empty($data['is_default'])) {
            $this->model
                ->where('is_default', 1)
                ->update([
                    'is_default' => false,
                ]);
        }

        return parent::updateById($data, $id);
    }

    public function getDefaultId()
    {
        $defaultProcess = $this->model
            ->where('is_default', 1)
            ->first();

        return $defaultProcess ? $defaultProcess->id : null;
    }
}
