<?php

namespace Modules\PostSale\Repositories\Eloquent;

use Modules\PostSale\Repositories\RealEstateRepositoryInterface;
use Newnet\Core\Repositories\BaseRepository;

class RealEstateRepository extends BaseRepository implements RealEstateRepositoryInterface
{
    public function findActive($id)
    {
        return $this->model
            ->where('is_active', 1)
            ->where('id', $id)
            ->firstOrFail();
    }
}
