<?php

namespace Modules\PostSale\Repositories\Eloquent;

use Modules\PostSale\Repositories\CategoryRepositoryInterface;
use Newnet\Core\Repositories\BaseRepository;
use Newnet\Core\Repositories\NestedRepositoryTrait;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    use NestedRepositoryTrait;
}
