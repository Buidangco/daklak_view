<?php

namespace Modules\PostSale\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Seo\Traits\SeoableTrait;
use Newnet\Core\Support\Traits\TreeCacheableTrait;

/**
 * Modules\PostSale\Models\Category
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $content
 * @property bool $is_active
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Kalnoy\Nestedset\Collection|\Modules\PostSale\Models\Category[] $children
 * @property-read int|null $children_count
 * @property mixed $url
 * @property-read \Modules\PostSale\Models\Category|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PostSale\Models\RealEstate[] $realEstates
 * @property-read int|null $real_estates_count
 * @property \Modules\Seo\Models\Meta|null $seometa
 * @property \Modules\Seo\Models\Url|null $seourl
 * @method static \Kalnoy\Nestedset\Collection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category d()
 * @method static \Kalnoy\Nestedset\Collection|static[] get($columns = ['*'])
 * @method static \Newnet\Core\Support\TreeCacheableQueryBuilder|\Modules\PostSale\Models\Category newModelQuery()
 * @method static \Newnet\Core\Support\TreeCacheableQueryBuilder|\Modules\PostSale\Models\Category newQuery()
 * @method static \Newnet\Core\Support\TreeCacheableQueryBuilder|\Modules\PostSale\Models\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    use TreeCacheableTrait;
    use SeoableTrait;

    protected $table = 'post_sale__categories';

    protected $fillable = [
        'name',
        'description',
        'content',
        'is_active',
        'parent_id',
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function realEstates()
    {
        return $this->belongsToMany(RealEstate::class, 'post_sale__real_estate_category');
    }

    public function getUrl()
    {
        return route('post-sale.web.category.detail', $this->id);
    }

    public function getDescendantIds()
    {
        $catIds = $this->descendants()->pluck('id')->toArray();
        $catIds[] = $this->getKey();

        return $catIds;
    }
}
