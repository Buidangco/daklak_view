<?php

namespace Modules\PostSale\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\PostSale\Models\Status
 *
 * @property int $id
 * @property string $name
 * @property int|null $sort_order
 * @property int $is_active
 * @property int $is_default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PostSale\Models\RealEstate[] $realEstates
 * @property-read int|null $real_estates_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\Status whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Status extends Model
{
    protected $table = 'post_sale__statuses';

    protected $fillable = [
        'name',
        'sort_order',
        'is_active',
        'is_default',
    ];

    public function realEstates()
    {
        return $this->hasMany(RealEstate::class);
    }
}
