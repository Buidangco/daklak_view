<?php

namespace Modules\PostSale\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Seo\Traits\SeoableTrait;
use Modules\ZoneModule\Models\ZoneDistrict;
use Modules\ZoneModule\Models\ZoneProvince;
use Modules\ZoneModule\Models\ZoneTownship;
use Newnet\Media\Traits\HasMediaTrait;
use Newnet\Tags\Traits\TaggableTrait;

/**
 * Modules\PostSale\Models\RealEstate
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $content
 * @property bool $is_active
 * @property bool $is_sticky
 * @property int|null $sort_order
 * @property int|null $category_id
 * @property string|null $author_type
 * @property int|null $author_id
 * @property string|null $customer_name
 * @property string|null $customer_phone
 * @property string|null $customer_email
 * @property string|null $customer_address
 * @property \Illuminate\Support\Carbon|null $published_at
 * @property int|null $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float|null $price
 * @property float|null $area
 * @property int|null $province_id
 * @property int|null $district_id
 * @property int|null $towship_id
 * @property string|null $type
 * @property \Illuminate\Support\Carbon|null $start_at
 * @property \Illuminate\Support\Carbon|null $stop_at
 * @property string|null $price_text
 * @property string|null $street
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $author
 * @property \Kalnoy\Nestedset\Collection|\Modules\PostSale\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Modules\PostSale\Models\Category|null $category
 * @property-read \Modules\ZoneModule\Models\ZoneDistrict|null $district
 * @property-read mixed $district_province
 * @property mixed $image
 * @property mixed $images
 * @property mixed $url
 * @property-read \Illuminate\Database\Eloquent\Collection|\Newnet\Media\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Modules\ZoneModule\Models\ZoneProvince|null $province
 * @property \Modules\Seo\Models\Meta|null $seometa
 * @property \Modules\Seo\Models\Url|null $seourl
 * @property \Illuminate\Database\Eloquent\Collection|\Newnet\Tags\Models\Tag[] $tags
 * @property-read \Modules\PostSale\Models\Status|null $status
 * @property-read int|null $tags_count
 * @property-read \Modules\ZoneModule\Models\ZoneTownship $township
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereAuthorType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereCustomerAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereCustomerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereCustomerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereCustomerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereIsSticky($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate wherePriceText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereStopAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereTowshipId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate withAllTags($tags, $group = null, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate withAnyTags($tags, $group = null, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate withoutAnyTags()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PostSale\Models\RealEstate withoutTags($tags, $group = null, $locale = null)
 * @mixin \Eloquent
 */
class RealEstate extends Model
{
    use HasMediaTrait;
    use TaggableTrait;
    use SeoableTrait;

    protected $table = 'post_sale__real_estates';

    protected $fillable = [
        'name',
        'description',
        'content',
        'is_active',
        'is_sticky',
        'sort_order',
        'category_id',
        'published_at',
        'categories',
        'image',
        'images',
        'customer_name',
        'customer_phone',
        'customer_email',
        'customer_address',
        'status_id',
        'price',
        'area',
        'province_id',
        'district_id',
        'towship_id',
        'street',
        'type',
        'start_at',
        'stop_at',
        'price_text',
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'is_sticky' => 'boolean',
        'price'     => 'float',
        'area'      => 'float',
    ];

    protected $dates = [
        'published_at',
        'start_at',
        'stop_at',
    ];

    public function author()
    {
        return $this->morphTo();
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'post_sale__real_estate_category');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function getUrl()
    {
        return route('post-sale.web.real-estate.detail', $this->id);
    }

    public function setCategoriesAttribute($value)
    {
        $value = array_filter($value);

        static::saved(function ($model) use ($value) {
            !$value || $this->categories()->sync($value);
        });
    }

    public function setImageAttribute($value)
    {
        $this->mediaAttributes['image'] = $value;
    }

    public function getImageAttribute()
    {
        return $this->getFirstMedia('images') ?: $this->getFirstMedia('image');
    }

    public function setImagesAttribute($value)
    {
        $this->mediaAttributes['images'] = $value;
    }

    public function getImagesAttribute()
    {
        return $this->getMedia('images');
    }

    public function province()
    {
        return $this->belongsTo(ZoneProvince::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(ZoneDistrict::class, 'district_id');
    }

    public function township()
    {
        return $this->belongsTo(ZoneTownship::class, 'township_id');
    }

    public function getDistrictProvinceAttribute()
    {
        return implode(', ', array_filter([
            object_get($this, 'district.name'),
            object_get($this, 'province.name'),
        ]));
    }
}
