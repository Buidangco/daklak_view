<?php

use Modules\PostSale\Http\Controllers\Web\RealEstateController;
use Modules\PostSale\Http\Controllers\Web\CategoryController;

Route::get('post-sale/real-estate/{id}', [RealEstateController::class, 'detail'])->name('post-sale.web.real-estate.detail');
Route::post('post-sale/real-estate/create', [RealEstateController::class, 'create'])->name('post-sale.web.real-estate.create');
Route::get('post-sale/category/{id}', [CategoryController::class, 'detail'])->name('post-sale.web.category.detail');
