<?php

use Modules\PostSale\Http\Controllers\Admin\RealEstateController;
use Modules\PostSale\Http\Controllers\Admin\CategoryController;
use Modules\PostSale\Http\Controllers\Admin\StatusController;

Route::prefix('post-sale')->group(function () {
    Route::prefix('real-estate')->group(function () {
        Route::get('', [RealEstateController::class, 'index'])
            ->name('post-sale.admin.real-estate.index')
            ->middleware('admin.can:post-sale.admin.real-estate.index');

        Route::get('create', [RealEstateController::class, 'create'])
            ->name('post-sale.admin.real-estate.create')
            ->middleware('admin.can:post-sale.admin.real-estate.create');

        Route::post('/', [RealEstateController::class, 'store'])
            ->name('post-sale.admin.real-estate.store')
            ->middleware('admin.can:post-sale.admin.real-estate.create');

        Route::get('{id}/edit', [RealEstateController::class, 'edit'])
            ->name('post-sale.admin.real-estate.edit')
            ->middleware('admin.can:post-sale.admin.real-estate.edit');

        Route::put('{id}', [RealEstateController::class, 'update'])
            ->name('post-sale.admin.real-estate.update')
            ->middleware('admin.can:post-sale.admin.real-estate.edit');

        Route::delete('{id}', [RealEstateController::class, 'destroy'])
            ->name('post-sale.admin.real-estate.destroy')
            ->middleware('admin.can:post-sale.admin.real-estate.destroy');
    });

    Route::prefix('category')->group(function () {
        Route::get('', [CategoryController::class, 'index'])
            ->name('post-sale.admin.category.index')
            ->middleware('admin.can:post-sale.admin.category.index');

        Route::get('create', [CategoryController::class, 'create'])
            ->name('post-sale.admin.category.create')
            ->middleware('admin.can:post-sale.admin.category.create');

        Route::post('/', [CategoryController::class, 'store'])
            ->name('post-sale.admin.category.store')
            ->middleware('admin.can:post-sale.admin.category.create');

        Route::get('{id}/edit', [CategoryController::class, 'edit'])
            ->name('post-sale.admin.category.edit')
            ->middleware('admin.can:post-sale.admin.category.edit');

        Route::put('{id}', [CategoryController::class, 'update'])
            ->name('post-sale.admin.category.update')
            ->middleware('admin.can:post-sale.admin.category.edit');

        Route::get('{id}/move-up', [CategoryController::class, 'moveUp'])
            ->name('post-sale.admin.category.move-up')
            ->middleware('admin.can:post-sale.admin.category.edit');

        Route::get('{id}/move-down', [CategoryController::class, 'moveDown'])
            ->name('post-sale.admin.category.move-down')
            ->middleware('admin.can:post-sale.admin.category.edit');

        Route::delete('{id}', [CategoryController::class, 'destroy'])
            ->name('post-sale.admin.category.destroy')
            ->middleware('admin.can:post-sale.admin.category.destroy');
    });

    Route::prefix('status')->group(function () {
        Route::get('', [StatusController::class, 'index'])
            ->name('post-sale.admin.status.index')
            ->middleware('admin.can:post-sale.admin.status.index');

        Route::get('create', [StatusController::class, 'create'])
            ->name('post-sale.admin.status.create')
            ->middleware('admin.can:post-sale.admin.status.create');

        Route::post('/', [StatusController::class, 'store'])
            ->name('post-sale.admin.status.store')
            ->middleware('admin.can:post-sale.admin.status.create');

        Route::get('{id}/edit', [StatusController::class, 'edit'])
            ->name('post-sale.admin.status.edit')
            ->middleware('admin.can:post-sale.admin.status.edit');

        Route::put('{id}', [StatusController::class, 'update'])
            ->name('post-sale.admin.status.update')
            ->middleware('admin.can:post-sale.admin.status.edit');

        Route::delete('{id}', [StatusController::class, 'destroy'])
            ->name('post-sale.admin.status.destroy')
            ->middleware('admin.can:post-sale.admin.status.destroy');
    });
});
