<?php

use Modules\PostSale\Models\Status;

if (!function_exists('get_post_sale_category_parent_options')) {
    /**
     * Get Post Sale Category Parent Options
     *
     * @return array
     */
    function get_post_sale_category_parent_options()
    {
        $options = [];

        $categoryTreeList = \Modules\PostSale\Models\Category::defaultOrder()->withDepth()->get()->toFlatTree();
        foreach ($categoryTreeList as $item) {
            $options[] = [
                'value' => $item->id,
                'label' => trim(str_pad('', $item->depth * 3, '-')).' '.$item->name,
            ];
        }

        return $options;
    }
}

if (!function_exists('get_post_sale_status_options')) {
    /**
     * Get Post Sale Status Options
     *
     * @return array
     */
    function get_post_sale_status_options()
    {
        $options = [];

        $statuses = Status::orderBy('sort_order')->get();
        foreach ($statuses as $item) {
            $options[] = [
                'value' => $item->id,
                'label' => $item->name,
            ];
        }

        return $options;
    }
}

if (!function_exists('get_post_sale_default_status_id')) {
    function get_post_sale_default_status_id()
    {
        $defaultStatus = Status::where('is_default', 1)->first();

        return $defaultStatus ? $defaultStatus->id : null;
    }
}
