<?php

namespace Modules\OurTeam\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\OurTeam\Http\Requests\OurTeamRequest;
use Modules\OurTeam\Repositories\OurTeamRepositoryInterface;

class OurTeamController extends Controller
{
    /**
     * @var OurTeamRepositoryInterface
     */
    protected $ourTeamRepository;

    public function __construct(OurTeamRepositoryInterface $ourTeamRepository)
    {
        $this->ourTeamRepository = $ourTeamRepository;
    }

    public function index(Request $request)
    {
        $items = $this->ourTeamRepository->paginate($request->input('max', 20));

        return view('our-team::admin.our-team.index', compact('items'));
    }

    public function create()
    {
        return view('our-team::admin.our-team.create');
    }

    public function store(OurTeamRequest $request)
    {
        $item = $this->ourTeamRepository->create($request->all());

        if ($request->input('continue')) {
            return redirect()
                ->route('our-team.admin.our-team.edit', $item->id)
                ->with('success', __('our-team::message.notification.created'));
        }

        return redirect()
            ->route('our-team.admin.our-team.index')
            ->with('success', __('our-team::message.notification.created'));
    }

    public function edit($id)
    {
        $item = $this->ourTeamRepository->find($id);

        return view('our-team::admin.our-team.edit', compact('item'));
    }

    public function update(OurTeamRequest $request, $id)
    {
        $item = $this->ourTeamRepository->updateById($request->all(), $id);

        if ($request->input('continue')) {
            return redirect()
                ->route('our-team.admin.our-team.edit', $item->id)
                ->with('success', __('our-team::message.notification.updated'));
        }

        return redirect()
            ->route('our-team.admin.our-team.index')
            ->with('success', __('our-team::message.notification.updated'));
    }

    public function destroy($id, Request $request)
    {
        $this->ourTeamRepository->delete($id);

        if ($request->wantsJson()) {
            Session::flash('success', __('our-team::message.notification.deleted'));
            return response()->json([
                'success' => true,
            ]);
        }

        return redirect()
            ->route('our-team.admin.our-team.index')
            ->with('success', __('our-team::message.notification.deleted'));
    }
}
