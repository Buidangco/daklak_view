<?php

namespace Modules\OurTeam;

use Illuminate\Support\Facades\Event;
use Modules\Cms\Events\CmsAdminMenuRegistered;
use Newnet\Acl\Facades\Permission;
use Newnet\Core\Facades\AdminMenu;
use Newnet\Core\Support\Module\BaseModuleServiceProvider;
use Modules\OurTeam\Repositories\Eloquent\OurTeamRepository;
use Modules\OurTeam\Repositories\OurTeamRepositoryInterface;
use Modules\OurTeam\Models\OurTeam;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function getModuleNamespace()
    {
        return 'our-team';
    }

    public function register()
    {
        parent::register();

        $this->app->singleton(OurTeamRepositoryInterface::class, function () {
            return new OurTeamRepository(new OurTeam());
        });

        require_once __DIR__.'/../helpers/helpers.php';
    }

    public function registerPermissions()
    {
        Permission::add('our-team.admin.our-team.index', __('our-team::permission.our-team.index'));
        Permission::add('our-team.admin.our-team.create', __('our-team::permission.our-team.create'));
        Permission::add('our-team.admin.our-team.edit', __('our-team::permission.our-team.edit'));
        Permission::add('our-team.admin.our-team.destroy', __('our-team::permission.our-team.destroy'));
    }

    public function registerAdminMenus()
    {
        Event::listen(CmsAdminMenuRegistered::class, function () {
            AdminMenu::addItem(__('our-team::menu.our-team.index'), [
                'id'         => 'our_team',
                'parent'     => CmsAdminMenuRegistered::MENU_ROOT_ID,
                'route'      => 'our-team.admin.our-team.index',
                'permission' => 'our-team.admin.our-team.index',
                'icon'       => 'fas fa-users',
                'order'      => 1000,
            ]);
        });
    }
}
