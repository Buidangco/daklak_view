<?php

namespace Modules\OurTeam\Models;

use Illuminate\Database\Eloquent\Model;
use Newnet\Media\Traits\HasMediaTrait;

/**
 * Modules\OurTeam\Models\OurTeam
 *
 * @property int $id
 * @property string|null $title Xung danh
 * @property string|null $name Ho va ten
 * @property string|null $position Chu vu
 * @property int $is_active
 * @property string|null $content
 * @property int|null $sort_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed $big_image
 * @property mixed $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Newnet\Media\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\OurTeam\Models\OurTeam whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OurTeam extends Model
{
    use HasMediaTrait;

    protected $table = 'our_team__items';

    protected $fillable = [
        'title',
        'name',
        'position',
        'is_active',
        'content',
        'sort_order',
        'image',
        'big_image',
    ];

    protected $casts = [
        'is_active',
    ];

    public function setImageAttribute($value)
    {
        $this->mediaAttributes['image'] = $value;
    }

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    public function setBigImageAttribute($value)
    {
        $this->mediaAttributes['big_image'] = $value;
    }

    public function getBigImageAttribute()
    {
        return $this->getFirstMedia('big_image');
    }
}
