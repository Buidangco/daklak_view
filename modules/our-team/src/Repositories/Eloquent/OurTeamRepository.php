<?php

namespace Modules\OurTeam\Repositories\Eloquent;

use Modules\OurTeam\Repositories\OurTeamRepositoryInterface;
use Newnet\Core\Repositories\BaseRepository;

class OurTeamRepository extends BaseRepository implements OurTeamRepositoryInterface
{

}
