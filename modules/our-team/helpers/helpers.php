<?php

use Modules\OurTeam\Models\OurTeam;

if (!function_exists('get_our_teams')) {
    function get_our_teams()
    {
        return OurTeam::where('is_active', 1)
            ->orderBy('sort_order')
            ->get();
    }
}
