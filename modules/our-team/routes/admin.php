<?php

use Modules\OurTeam\Http\Controllers\Admin\OurTeamController;

Route::prefix('our-team')->group(function () {
    Route::get('', [OurTeamController::class, 'index'])
        ->name('our-team.admin.our-team.index')
        ->middleware('admin.can:our-team.admin.our-team.index');

    Route::get('create', [OurTeamController::class, 'create'])
        ->name('our-team.admin.our-team.create')
        ->middleware('admin.can:our-team.admin.our-team.create');

    Route::post('/', [OurTeamController::class, 'store'])
        ->name('our-team.admin.our-team.store')
        ->middleware('admin.can:our-team.admin.our-team.create');

    Route::get('{id}/edit', [OurTeamController::class, 'edit'])
        ->name('our-team.admin.our-team.edit')
        ->middleware('admin.can:our-team.admin.our-team.edit');

    Route::put('{id}', [OurTeamController::class, 'update'])
        ->name('our-team.admin.our-team.update')
        ->middleware('admin.can:our-team.admin.our-team.edit');

    Route::delete('{id}', [OurTeamController::class, 'destroy'])
        ->name('our-team.admin.our-team.destroy')
        ->middleware('admin.can:our-team.admin.our-team.destroy');
});
