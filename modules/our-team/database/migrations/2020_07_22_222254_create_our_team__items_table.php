<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOurTeamItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_team__items', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('Xung danh')->nullable();
            $table->string('name')->comment('Ho va ten')->nullable();
            $table->string('position')->comment('Chu vu')->nullable();
            $table->boolean('is_active')->default(1);
            $table->longText('content')->nullable();
            $table->unsignedInteger('sort_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('our_team__items');
    }
}
