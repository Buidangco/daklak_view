<div class="row">
    <div class="col-md-8">
        @input(['name' => 'title', 'label' => __('our-team::message.title')])
        @input(['name' => 'name', 'label' => __('our-team::message.name')])
        @input(['name' => 'position', 'label' => __('our-team::message.position')])
        @mediafile(['name' => 'image', 'label' => __('our-team::message.image')])
        @mediafile(['name' => 'big_image', 'label' => __('our-team::message.big_image')])
    </div>
    <div class="col-md-4">
        @checkbox(['name' => 'is_active', 'label' => __('our-team::message.is_active'), 'default' => true])
        @input(['name' => 'sort_order', 'label' => __('our-team::message.sort_order')])
    </div>
</div>
