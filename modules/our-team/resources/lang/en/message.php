<?php
return [
    'title'      => 'Title',
    'name'       => 'Name',
    'position'   => 'Position',
    'big_image'  => 'Image Content',
    'image'      => 'Image',
    'sort_order' => 'Sort Order',
    'created_at' => 'Created At',
    'is_active'  => 'Is Active',

    'index' => [
        'page_title'    => 'OurTeam',
        'page_subtitle' => 'OurTeam',
        'breadcrumb'    => 'OurTeam',
    ],

    'create' => [
        'page_title'    => 'Add OurTeam',
        'page_subtitle' => 'Add OurTeam',
        'breadcrumb'    => 'Add',
    ],

    'edit' => [
        'page_title'    => 'Edit OurTeam',
        'page_subtitle' => 'Edit OurTeam',
        'breadcrumb'    => 'Edit',
    ],

    'notification' => [
        'created' => 'OurTeam successfully created!',
        'updated' => 'OurTeam successfully updated!',
        'deleted' => 'OurTeam successfully deleted!',
    ],
];
