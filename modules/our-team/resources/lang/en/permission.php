<?php
return [
    'our-team' => [
        'index'   => 'List OurTeam',
        'create'  => 'Create OurTeam',
        'edit'    => 'Edit OurTeam',
        'destroy' => 'Delete OurTeam',
    ],
];
