<?php
return [
    'title'      => 'Xưng danh',
    'name'       => 'Họ tên',
    'position'   => 'Vị trí',
    'big_image'  => 'Nội dung',
    'image'      => 'Avatar',
    'sort_order' => 'Sắp xếp',
    'created_at' => 'Ngày tạo',
    'is_active'  => 'Kích hoạt',

    'index' => [
        'page_title'    => 'Nhân sự chủ chốt',
        'page_subtitle' => 'Nhân sự chủ chốt',
        'breadcrumb'    => 'Nhân sự chủ chốt',
    ],

    'create' => [
        'page_title'    => 'Thêm Nhân sự chủ chốt',
        'page_subtitle' => 'Thêm Nhân sự chủ chốt',
        'breadcrumb'    => 'Thêm',
    ],

    'edit' => [
        'page_title'    => 'Sửa Nhân sự chủ chốt',
        'page_subtitle' => 'Sửa Nhân sự chủ chốt',
        'breadcrumb'    => 'Sửa',
    ],

    'notification' => [
        'created' => 'Nhân sự chủ chốt đã được tạo!',
        'updated' => 'Nhân sự chủ chốt đã được cập nhật!',
        'deleted' => 'Nhân sự chủ chốt đã xoá!',
    ],
];
