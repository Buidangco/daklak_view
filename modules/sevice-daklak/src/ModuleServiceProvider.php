<?php

namespace Modules\SeviceDaklak;

use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Support\Facades\Event;
use Newnet\Acl\Facades\Permission;
use Newnet\Core\Facades\AdminMenu;
use Newnet\Core\Support\Module\BaseModuleServiceProvider;
use Modules\SeviceDaklak\Repositories\Eloquent\SeviceDaklakRepository;
use Modules\SeviceDaklak\Repositories\SeviceDaklakRepositoryInterface;
use Modules\SeviceDaklak\Models\SeviceDaklak;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    public function getModuleNamespace()
    {
        return 'sevice-daklak';
    }

    public function register()
    {
        parent::register();

        $this->app->singleton(SeviceDaklakRepositoryInterface::class, function () {
            return new SeviceDaklakRepository(new SeviceDaklak());
        });
    }

    public function registerPermissions()
    {
        Permission::add('sevice-daklak.admin.sevice-daklak.index', __('sevice-daklak::permission.sevice-daklak.index'));
        Permission::add('sevice-daklak.admin.sevice-daklak.create', __('sevice-daklak::permission.sevice-daklak.create'));
        Permission::add('sevice-daklak.admin.sevice-daklak.edit', __('sevice-daklak::permission.sevice-daklak.edit'));
        Permission::add('sevice-daklak.admin.sevice-daklak.destroy', __('sevice-daklak::permission.sevice-daklak.destroy'));
    }

    public function registerAdminMenus()
    {
        Event::listen(RouteMatched::class, function () {
            AdminMenu::addItem(__('sevice-daklak::menu.sevice-daklak.index'), [
                'id'         => 'sevice-daklak_root',
                'route'      => 'sevice-daklak.admin.sevice-daklak.index',
                'permission' => 'sevice-daklak.admin.sevice-daklak.index',
                'icon'       => 'typcn typcn-th-small',
                'order'      => 1000,
            ]);
        });
    }
}
