<?php

namespace Modules\SeviceDaklak\Models;

use Illuminate\Database\Eloquent\Model;

class service_daklak_title extends Model
{
    protected $table = 'service_daklak_title';

    protected $fillable = [
        'id_title',
        'image_title',
        'name_title',
        'content_title',
        'created_at',
    ];
}
