<?php

namespace Modules\SeviceDaklak\Models;

use Illuminate\Database\Eloquent\Model;

class service_daklak_slide extends Model
{
    protected $table = 'service_daklak_slide';

    protected $fillable = [
        'slide_image',
        'slide_name',
        'slide_title',
        'slide_content'
    ];
}
