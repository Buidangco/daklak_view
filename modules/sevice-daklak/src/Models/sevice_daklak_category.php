<?php

namespace Modules\SeviceDaklak\Models;

use Illuminate\Database\Eloquent\Model;

class sevice_daklak_category extends Model
{
       protected $table = 'sevice_daklak_category';

    protected $fillable = [
        'idcategory',
        'namecategory'
    ];
    //  public function __construct(array $attributes = [])
    // {
    //     $attributes['published_at'] = now();

    //     parent::__construct($attributes);
    // }

    // public function author()
    // {
    //     return $this->morphTo();
    // }

    // public function categories()
    // {
    //     return $this->belongsToMany(Category::class, 'cms__post_category');
    // }

    // public function category()
    // {
    //     return $this->belongsTo(Category::class);
    // }

    // public function getUrl()
    // {
    //     return route('cms.web.post.detail', $this->id);
    // }

    // public function setCategoriesAttribute($value)
    // {
    //     $value = array_filter($value);

    //     static::saved(function ($model) use ($value) {
    //         !$value || $this->categories()->sync($value);
    //     });
    // }

    // public function setImageAttribute($value)
    // {
    //     $this->mediaAttributes['image'] = $value;
    // }

    // public function getImageAttribute()
    // {
    //     return $this->getFirstMedia('image');
    // }
}
