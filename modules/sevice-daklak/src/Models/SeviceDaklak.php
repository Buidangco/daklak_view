<?php

namespace Modules\SeviceDaklak\Models;

use Illuminate\Database\Eloquent\Model;

class SeviceDaklak extends Model
{
    protected $table = 'sevice_daklak_category';

    protected $fillable = [
        'idcategory',
        'namecategory'
    ];
}
