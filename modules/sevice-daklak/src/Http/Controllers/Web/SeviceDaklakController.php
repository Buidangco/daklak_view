<?php

namespace Modules\SeviceDaklak\Http\Controllers\Web;

use Illuminate\Routing\Controller;

class SeviceDaklakController extends Controller
{
    public function index()
    {
        return view('sevice-daklak::web.sevice-daklak.index');
    }
}
