<?php

namespace Modules\SeviceDaklak\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\SeviceDaklak\Http\Requests\SeviceDaklakRequest;
use Modules\SeviceDaklak\Repositories\SeviceDaklakRepositoryInterface;

class SeviceDaklakController extends Controller
{
    /**
     * @var SeviceDaklakRepositoryInterface
     */
    protected $seviceDaklakRepository;

    public function __construct(SeviceDaklakRepositoryInterface $seviceDaklakRepository)
    {
        $this->seviceDaklakRepository = $seviceDaklakRepository;
    }

    public function index(Request $request)
    {
        $items = $this->seviceDaklakRepository->paginate($request->input('max', 20));

        return view('sevice-daklak::admin.sevice-daklak.index', compact('items'));
    }

    public function create()
    {
        return view('sevice-daklak::admin.sevice-daklak.create');
    }

    public function store(SeviceDaklakRequest $request)
    {
        $item = $this->seviceDaklakRepository->create($request->all());

        if ($request->input('continue')) {
            return redirect()
                ->route('sevice-daklak.admin.sevice-daklak.edit', $item->id)
                ->with('success', __('sevice-daklak::message.notification.created'));
        }

        return redirect()
            ->route('sevice-daklak.admin.sevice-daklak.index')
            ->with('success', __('sevice-daklak::message.notification.created'));
    }

    public function edit($id)
    {
        $item = $this->seviceDaklakRepository->find($id);

        return view('sevice-daklak::admin.sevice-daklak.edit', compact('item'));
    }

    public function update(SeviceDaklakRequest $request, $id)
    {
        $item = $this->seviceDaklakRepository->updateById($request->all(), $id);

        if ($request->input('continue')) {
            return redirect()
                ->route('sevice-daklak.admin.sevice-daklak.edit', $item->id)
                ->with('success', __('sevice-daklak::message.notification.updated'));
        }

        return redirect()
            ->route('sevice-daklak.admin.sevice-daklak.index')
            ->with('success', __('sevice-daklak::message.notification.updated'));
    }

    public function destroy($id, Request $request)
    {
        $this->seviceDaklakRepository->delete($id);

        if ($request->wantsJson()) {
            Session::flash('success', __('sevice-daklak::message.notification.deleted'));
            return response()->json([
                'success' => true,
            ]);
        }

        return redirect()
            ->route('sevice-daklak.admin.sevice-daklak.index')
            ->with('success', __('sevice-daklak::message.notification.deleted'));
    }
}
