<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceDaklakSlide extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_daklak_slide', function (Blueprint $table) {
            $table->id();
            $table->string('slide_image');
            $table->string('slide_name');
            $table->string('slide_title');
            $table->string('slide_content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_daklak_slide');
    }
}
