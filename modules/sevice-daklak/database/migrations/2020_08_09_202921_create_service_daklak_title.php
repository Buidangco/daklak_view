<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceDaklakTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_daklak_title', function (Blueprint $table) {
            $table->id();
            $table->string('id_title');
            $table->string('image_title');
            $table->string('name_title');
            $table->string('content_title');
            $table->foreign('id_title')->references('idcategory')->on('sevice_daklak_category');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_daklak_title');
    }
}
