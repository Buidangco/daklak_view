<?php
return [
    'name'       => 'Name',
    'created_at' => 'Created At',

    'index' => [
        'page_title'    => 'sevice-daklak',
        'page_subtitle' => 'sevice-daklak',
        'breadcrumb'    => 'sevice-daklak',
    ],

    'create' => [
        'page_title'    => 'Add sevice-daklak',
        'page_subtitle' => 'Add sevice-daklak',
        'breadcrumb'    => 'Add',
    ],

    'edit' => [
        'page_title'    => 'Edit sevice-daklak',
        'page_subtitle' => 'Edit sevice-daklak',
        'breadcrumb'    => 'Edit',
    ],

    'notification' => [
        'created' => 'sevice-daklak successfully created!',
        'updated' => 'sevice-daklak successfully updated!',
        'deleted' => 'sevice-daklak successfully deleted!',
    ],
];
