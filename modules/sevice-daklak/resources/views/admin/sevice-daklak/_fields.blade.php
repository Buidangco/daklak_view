@input(['name' => 'name', 'label' => __('sevice-daklak::message.name')])
@textarea(['name' => 'description', 'label' => __('sevice-daklak::message.description')])
@editor(['name' => 'content', 'label' => __('sevice-daklak::message.content')])
@checkbox(['name' => 'is_active', 'label' => __('sevice-daklak::message.is_active')])