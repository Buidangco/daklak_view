<?php

use Modules\SeviceDaklak\Http\Controllers\Admin\SeviceDaklakController;

Route::prefix('sevice-daklak')->group(function () {
    Route::get('', [SeviceDaklakController::class, 'index'])
        ->name('sevice-daklak.admin.sevice-daklak.index')
        ->middleware('admin.can:sevice-daklak.admin.sevice-daklak.index');

    Route::get('create', [SeviceDaklakController::class, 'create'])
        ->name('sevice-daklak.admin.sevice-daklak.create')
        ->middleware('admin.can:sevice-daklak.admin.sevice-daklak.create');

    Route::post('/', [SeviceDaklakController::class, 'store'])
        ->name('sevice-daklak.admin.sevice-daklak.store')
        ->middleware('admin.can:sevice-daklak.admin.sevice-daklak.create');

    Route::get('{id}/edit', [SeviceDaklakController::class, 'edit'])
        ->name('sevice-daklak.admin.sevice-daklak.edit')
        ->middleware('admin.can:sevice-daklak.admin.sevice-daklak.edit');

    Route::put('{id}', [SeviceDaklakController::class, 'update'])
        ->name('sevice-daklak.admin.sevice-daklak.update')
        ->middleware('admin.can:sevice-daklak.admin.sevice-daklak.edit');

    Route::delete('{id}', [SeviceDaklakController::class, 'destroy'])
        ->name('sevice-daklak.admin.sevice-daklak.destroy')
        ->middleware('admin.can:sevice-daklak.admin.sevice-daklak.destroy');
});
