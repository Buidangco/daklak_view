<?php

use Modules\SeviceDaklak\Http\Controllers\Web\SeviceDaklakController;

Route::prefix('sevice-daklak')->group(function () {
    Route::get('', [SeviceDaklakController::class, 'index'])->name('sevice-daklak.web.sevice-daklak.index');
});
