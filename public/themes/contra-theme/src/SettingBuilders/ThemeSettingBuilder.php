<?php

namespace Themes\ContraTheme\SettingBuilders;

use Newnet\Core\Services\AdminSetting\BaseSettingBuilder;

class ThemeSettingBuilder extends BaseSettingBuilder
{
    public function getTitle()
    {
        return __('theme::setting.panel_title');
    }

    public function save()
    {
        setting($this->request->only([
            'footer_copyright',
            'contact_email',
            'contact_phone',
            'contact_address',
            'contact_map_title',
            'contact_map_lat',
            'contact_map_lng',
        ]));
    }

    public function render()
    {
        $item = new \stdClass();

        $item->footer_copyright = setting('footer_copyright');

        $item->contact_email = setting('contact_email');
        $item->contact_phone = setting('contact_phone');
        $item->contact_address = setting('contact_address');
        $item->contact_map_title = setting('contact_map_title');
        $item->contact_map_lat = setting('contact_map_lat');
        $item->contact_map_lng = setting('contact_map_lng');

        return view('admin.theme-setting', compact('item'));
    }
}
