<?php

namespace Themes\ContraTheme;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use Themes\ContraTheme\SettingBuilders\ThemeSettingBuilder;

class ThemeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'theme');

        \SettingBuilder::themeGroup()->add(ThemeSettingBuilder::class);

        // \View::composer('cms::web.page.home', function (View $view) {
        //     $view->with('foo', $bar);
        // });
    }
}
