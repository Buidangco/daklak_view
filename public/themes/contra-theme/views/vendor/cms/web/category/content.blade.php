<!--Page Title-->
<section class="page-title" style="background-image:url({{ theme_url('img/background/10.jpg') }});">
    <div class="auto-container">
        <div class="inner-container clearfix">
            <div class="title-box">
                <h1>News & Artical</h1>
                <span class="title">The Interior speak for themselves</span>
            </div>
            <ul class="bread-crumb clearfix">
                <li><a href="/">Home</a></li>
                <li>{{ $category->name }}</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->

<!-- Blog Section -->
<section class="blog-section">
    <div class="auto-container">
        <div class="row">
        @foreach($posts as $post)
            <!-- News Block -->
                <div class="news-block-two col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img src="{{ $post->image }}" alt="{{ $post->name }}"></figure>
                            <div class="overlay-box"><a href="{{ $post->url }}"><i class="fa fa-link"></i></a></div>
                        </div>
                        <div class="caption-box">
                            <div class="inner">
                                <h3><a href="{{ $post->url }}">{{ $post->name }}</a></h3>
                                <ul class="info">
                                    <li>06 June 2018,</li>
                                    <li><a href="blog-detail-2">John Smith</a></li>
                                    <li><a href="blog-detail-2">3 Commets</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
</section>
<!--End Blog Section -->
