<nav class="main-menu navbar-expand-md ">
    <div class="navbar-header">
        <!-- Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon flaticon-menu-button"></span>
        </button>
    </div>

    <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
        <ul class="navigation clearfix">
            @foreach($items as $item)
                <li class="dropdown {{ $item->isActive() ? 'current' : '' }}">
                    <a href="{{ $item->getUrl() }}">{{ $item->label }}</a>
                </li>
            @endforeach
        </ul>
    </div>
</nav>
