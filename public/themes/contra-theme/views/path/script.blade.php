@assetadd('jquery', theme_url('js/jquery.js'))
@assetadd('popper', theme_url('js/popper.min.js'))
@assetadd('bootstrap', theme_url('js/bootstrap.min.js'), ['jquery'])
@assetadd('script', theme_url('js/script.js'), ['jquery', 'bootstrap', 'popper'])
@assetadd('fancybox', theme_url('js/jquery.fancybox.js'), ['jquery'])
@assetadd('owl', theme_url('js/owl.js'), ['jquery'])
@assetadd('wow', theme_url('js/wow.js'), ['jquery'])
@assetadd('appear', theme_url('js/appear.js'), ['jquery'])
@assetadd('mixitup', theme_url('js/mixitup.js'), ['jquery'])
@assetadd('color-settings.js', theme_url('js/color-settings.js'), ['jquery'])



