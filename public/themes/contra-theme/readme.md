# Theme contraTheme
Theme name: `contra-theme`

## Install
`npm install`

## Dev
`npm run watch`

## Build
`npm run prod`
