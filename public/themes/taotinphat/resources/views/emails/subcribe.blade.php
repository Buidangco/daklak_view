@component('mail::message')
**Chào bạn,**

Có khách hàng yêu cầu nhận thông tin về dự án

> Email: **{{ trim($customer_email) }}**

@component('mail::button', ['url' => $detail_url])
Xem chi tiết
@endcomponent

Thanks,<br>
{{ config('app.name') }}

---
*Email này được gửi tự động, vui lòng không trả lời*
@endcomponent
