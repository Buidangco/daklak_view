@component('mail::message')
**Chào bạn,**

Có khách hàng yêu cầu liên hệ

> Họ và tên: **{{ trim($customer_name) }}**

> Email: **{{ trim($customer_email) }}**

> Số điện thoại: **{{ trim($customer_phone) }}**

**Nội dung yêu cầu:**

> {{ trim($content) }}

@component('mail::button', ['url' => $detail_url])
Xem chi tiết
@endcomponent

Thanks,<br>
{{ config('app.name') }}

---
*Email này được gửi tự động, vui lòng không trả lời*
@endcomponent
