{{--@input(['name' => 'contact_phone', 'label' => __('theme::setting.contact_phone')])--}}
{{--@input(['name' => 'contact_email', 'label' => __('theme::setting.contact_email')])--}}
{{--@input(['name' => 'contact_address', 'label' => __('theme::setting.contact_address')])--}}
{{--@input(['name' => 'contact_map_title', 'label' => __('theme::setting.contact_map_title')])--}}
{{--@input(['name' => 'contact_map_lat', 'label' => __('theme::setting.contact_map_lat')])--}}
{{--@input(['name' => 'contact_map_lng', 'label' => __('theme::setting.contact_map_lng')])--}}

{{--<hr>--}}

{{--@editor([--}}
{{--    'name' => 'footer_copyright',--}}
{{--    'label' => __('theme::setting.footer_copyright'),--}}
{{--])--}}

{{--<hr>--}}

@input(['name' => 'contact_care_project_admin_emails', 'label' => __('theme::setting.contact_care_project_admin_emails')])

<hr>

@textarea(['name' => 'doi_tac_desc', 'label' => __('theme::setting.doi_tac_desc')])
{{--@gallery(['name' => 'doi_tac_images', 'label' => __('theme::setting.doi_tac_images')])--}}

<hr>

@editor(['name' => 'document_download', 'label' => __('theme::setting.document_download')])

<hr>

@textarea(['name' => 'company_map_iframe', 'label' => __('theme::setting.company_map_iframe'), 'helper' => __('theme::setting.company_map_iframe_help')])
