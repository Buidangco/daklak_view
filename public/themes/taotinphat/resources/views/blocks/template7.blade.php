@if($posts = __theme_get_last_post_of_category([1, 4, 5, 6], 4))
    <div class="section section-new bg-gradient-vertical pt-3 pb-4">
        <div class="container-fluid">
            <div class="row">
                @if($post = $posts->first())
                    <div class="col-md-5 pr-0">
                        <div class="block-news p-5 bg-white animation h-100">
                            <div class="title-group">
                                <h3>{{ $post->name }}</h3>
                            </div>
                            <div class="content">
                                <p class="desc">
                                    {!! $post->description ?: str_words(strip_tags($post->content), 50) !!}
                                </p>
                                <br>
                                <br>
                                <div class="thumbnail">
                                    <img class="img-fluid" src="{{ $post->image }}" alt="{{ $post->name }}">
                                </div>
                            </div>
                            <div class="block-btn text-center pt-5">
                                <a class="btn gradien-red btn-shadown w-50" href="{{ $post->url }}" role="button">
                                    Xem thêm
                                </a>
                            </div>
                        </div>
                    </div>
                @endif

                    <div class="col-md-7">
                        <div class="c-list-news py-0">
                            @foreach($posts->skip(1) as $post)
                                <div class="c-list-item box-shadown animation">
                                    <div class="c-list-content">
                                        <h3 class="c-list-title">{{ $post->name }}</h3>
                                        <p class="c-list-text">{{ $post->description ?: str_words(strip_tags($post->content), 40) }}</p>
                                    </div>
                                    <div class="c-list-thumbnail">
                                        <div class="image-cover image-cover--100h">
                                            <a href="{{ $post->url }}">
                                                <img src="{{ $post->image }}" alt="{{ $post->name }}" class="img">
                                            </a>
                                        </div>
                                    </div>
                                    <a href="{{ $post->url }}" class="link"><em class="ic-triang-r"></em></a>
                                </div>
                            @endforeach
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endif
