

<div class="sticky-top">
<div class="card card-mail mb-4">
<div class="card-body">
<div class="card-title py-3">
<h3>Nhận thông tin dự án mới nhất</h3>
</div>
<form action="{{ route('theme.web.contact.subcribe') }}" method="POST" id="formSubcribe">
@csrf

<div class="form-group">
<input type="email" class="form-control" id="email" placeholder="Địa chỉ email" name="customer_email">
</div>
<div class="form-group">
<button class="btn btn-orange w-100">ĐĂNG KÝ</button>
</div>

<div class="alert alert-danger" style="display: none" id="formSubcribeError"></div>
<div class="alert alert-success" style="display: none" id="formSubcribeSuccess"></div>

<div class="form-group">
<p class="text-center py-3">Đừng lo chúng tôi không spam</p>
</div>
</form>
<span class="icon-email"></span>
</div>
</div>

<div class="cart card-mail">
<div class="card-body">
{!! setting('document_download') !!}
</div>
</div>
</div>