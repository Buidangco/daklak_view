<section class="section section-about template-14">
    <div class="container">
        <div class="section_inner">
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="box-content mb-3">
                        <h5 class="box-title text-center title-md mb-4">Lorem ipsum dolor sit amet, consec</h5>
                        <p class="text text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus iusto ab atque modi quaerat! Ut, quisquam non nulla tempora doloribus fuga sed rerum eligendi eum ipsum dignissimos magni itaque tenetur?Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus iusto ab atque modi quaerat! Ut, quisquam non nulla tempora doloribus fuga sed rerum eligendi eum ipsum dignissimos magni itaque tenetur?</p>
                        <br>
                        <div class="block-video w-100">
                            <a href="https://www.youtube.com/embed/zmOi0wPk3Bw" class="d-block" data-fancybox="video">
                                <img class="img-fluid" src="{{ theme_url('images/video.jpg') }}" alt="">
                            </a>
                        </div>
                        <br>
                        <div class="c-list-active my-5">
                            <div class="c-actice c-active-01">
                                <em class="step-01"></em>
                            </div>
                            <div class="c-actice c-active-012">
                                <em class="step-02"></em>
                            </div>
                            <div class="c-actice c-active-03">
                                <em class="step-03"></em>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>