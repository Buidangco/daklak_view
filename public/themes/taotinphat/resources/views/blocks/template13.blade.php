<section class="section section-news tempalte-13">
    <div class="page-title">
        <h1><span>TIN TỨC</span></h1>
    </div>
    <div class="container">
        <div class="section_inner">
            <div class="row">
                @foreach(__theme_get_last_posts(3) as $lastPost)
                    <div class="col-md-12 col-lg-6">
                        <div class="box-content mb-3">
                            <div class="box-header">
                                <div class="thumbnail mb-3">
                                    <a href="{{ $lastPost->url }}"><img class="img-fluid" src="{{ $lastPost->image }}" alt="{{ $lastPost->name }}"></a>
                                </div>
                            </div>
                            <div class="box-body">
                                <h5 class="box-title text-center title-md mb-4">{{ $lastPost->name }}</h5>
                                <p class="text text-justify">
                                    {!! $lastPost->description ?: str_words(strip_tags($lastPost->content), 50) !!}
                                </p>
                            </div>
                            <div class="block-more text-center bg-white py-3">
                                <a href="{{ $lastPost->url }}" class="btn btn-outline-secondary">
                                    <span>XEM THÊM</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
