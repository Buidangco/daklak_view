<div class="section section-active">
    <div class="section-header bg-white animation">
        <div class="container">
            <h2 class="title">HOẠT ĐỘNG</h2>
            <p class="desc text-justify">{{ $page->home_activity_desc }}</p>
              <nav>
                <div class="nav nav-tabs  c-list-active mt-5 mb-4" id="nav-tab" role="tablist">
                  <a class="nav-item active" data-toggle="tab" href="#video-01" role="tab" aria-controls="video-01" aria-selected="true"><em class="step-01"></em></a>
                  <a class="nav-item " data-toggle="tab" href="#video-02" role="tab" aria-controls="video-02" aria-selected="false"><em class="step-02"></em></a>
                  <a class="nav-item " data-toggle="tab" href="#video-03" role="tab" aria-controls="video-03" aria-selected="false"><em class="step-03"></em></a>
                </div>
              </nav>
        </div>
    </div>
    <div class="section-active-content animation">
        <div class="container">
            <div class="block-video w-75 m-auto">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="video-01" role="tabpanel" >
                        <div class="video-background">
                            <div class="video-foreground">
                                <iframe width="100%" src="{{ __theme_get_youtube_embed(object_get($page, 'home_activity_video_1')) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="video-02" role="tabpanel" >
                        <div class="video-background">
                            <div class="video-foreground">
                                <iframe width="100%" src="{{ __theme_get_youtube_embed(object_get($page, 'home_activity_video_2')) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="video-03" role="tabpanel">
                        <div class="video-background">
                            <div class="video-foreground">
                                <iframe width="100%" src="{{ __theme_get_youtube_embed(object_get($page, 'home_activity_video_3')) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="block-active-slider mt-5">
                <div class="area-slider">
                    <div class="swiper-active-container slider02">
                        <div class="swiper-wrapper">
                            @foreach(__theme_get_last_post_of_category(2, 6) as $post)
                                <div class="swiper-slide">
                                    <div class="card card-05">
                                        <div class="card-img-top box-shadown">
                                            <div class="thumbnail">
                                                <a href="{{ $post->url }}" class="image-cover image-cover--16x9">
                                                    <img src="{{ $post->image }}" alt="{{ $post->name }}" class="img">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card-body p-0">
                                            <a href="{{ $post->url }}">
                                                <div class="card-title">
                                                    {{ $post->name }}
                                                </div>
                                            </a>
                                            <p class="card-text">
                                                {{ $post->description ?: str_words(strip_tags($post->content), 40) }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="swiper-button-next-02"><em class="ic-arrow-triang"></em></div>
                    <div class="swiper-button-prev-02"><em class="ic-arrow-triang"></em></div>
                </div>
            </div>
        </div>
    </div>
</div>
