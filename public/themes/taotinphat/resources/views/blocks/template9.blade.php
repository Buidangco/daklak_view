<div class="section section-processing bg-white ">
    <div class="container">
        <div class="section-header animation pt-4">
            <div class="media align-items-center">
                <div class="d-flex align-self-center mr-3 animation">
                    <h3><img src="{{ theme_url('images/title-02.png') }}" alt=""></h3>
                </div>
                <div class="media-body animation">
                    <p>{{ $page->home_project_desc }}</p>
                </div>
            </div>
        </div>
        <div class="section-inner">
            <div class="row">
                @foreach(__theme_get_list_pending_projects(3) as $project)
                    <div class="col-md-4 col-sm-6 animation">
                        <div class="card card-03">
                            <div class="card-img-top box-shadown">
                                <div class="thumbnail">
                                    <div class="image-cover image-cover--4x3">
                                        <a href="{{ $project->url }}">
                                            <img src="{{ $project->image }}" alt="{{ $project->name }}" class="img">
                                        </a>
                                    </div>
                                </div>
                                <div class="card-title">{{ $project->name }}</div>
                            </div>
                            <div class="card-body">
                                <div class="text-center">
                                    <a class="btn gradien-red btn-shadown" href="{{ $project->url }}" role="button">
                                        Chi tiết
                                    </a>
                                </div>
                                <p class="card-text trimText clamp4">
                                    {!! $project->description ?: str_words(strip_tags($project->content), 40) !!}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="block-button text-center mb-5">
            <em class="ic-arrow-triang"></em>
            <a class="btn gradien-red btn-shadown btn-default" href="{{ url('du-an') }}" role="button">
                Xem tất cả
            </a>
        </div>
    </div>
</div>
