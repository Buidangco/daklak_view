<div class="section section-maps bg-white">
    @if($iframe = setting('company_map_iframe'))
        <iframe src="{{ __theme_get_src_from_iframe($iframe) }}" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    @else
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.9165219569254!2d106.71918331411678!3d10.817700261390158!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175289da643eddf%3A0xb3e62c1eca79fadf!2zMTA1IMSQxrDhu51uZyBCw6xuaCBRdeG7m2ksIFBoxrDhu51uZyAyNywgQsOsbmggVGjhuqFuaCwgSOG7kyBDaMOtIE1pbmgsIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1595262196438!5m2!1sen!2s" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    @endif
</div>
