<section class="section section-news template-16">
    <div class="container">
        <div class="section_inner">
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <a href="#!" class="d-block">
                        <div class="media media-news type-02">
                            <div class="media-body">
                                <h5 class="mt-0">Title headline</h5>
                                <p class="text trimText clamp5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed fugit, veniam sunt accusantium reprehenderit dicta veritatis, architecto atque accusamus quis aliquam mollitia vel! Nobis repudiandae temporibus doloribus id, voluptate vel.</p>
                            </div>
                            <div href="#!" class="d-flex col-4 p-0 pl-3">
                                <div class="image-cover image-cover--16x9 rounded-lg">
                                    <img class="img " src="{{ theme_url('images/16x9.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-12 col-lg-6">
                    <a href="#!" class="d-block">
                        <div class="media media-news type-02">
                            <div class="media-body">
                                <h5 class="mt-0">Title headline</h5>
                                <p class="text trimText clamp5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed fugit, veniam sunt accusantium reprehenderit dicta veritatis, architecto atque accusamus quis aliquam mollitia vel! Nobis repudiandae temporibus doloribus id, voluptate vel.</p>
                            </div>
                            <div href="#!" class="d-flex col-4 p-0 pl-3">
                                <div class="image-cover image-cover--16x9 rounded-lg">
                                    <img class="img " src="{{ theme_url('images/16x9.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-12 col-lg-6">
                    <a href="#!" class="d-block">
                        <div class="media media-news type-02">
                            <div class="media-body">
                                <h5 class="mt-0">Title headline</h5>
                                <p class="text trimText clamp5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed fugit, veniam sunt accusantium reprehenderit dicta veritatis, architecto atque accusamus quis aliquam mollitia vel! Nobis repudiandae temporibus doloribus id, voluptate vel.</p>
                            </div>
                            <div href="#!" class="d-flex col-4 p-0 pl-3">
                                <div class="image-cover image-cover--16x9 rounded-lg">
                                    <img class="img " src="{{ theme_url('images/16x9.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-12 col-lg-6">
                    <a href="#!" class="d-block">
                        <div class="media media-news type-02">
                            <div class="media-body">
                                <h5 class="mt-0">Title headline</h5>
                                <p class="text trimText clamp5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed fugit, veniam sunt accusantium reprehenderit dicta veritatis, architecto atque accusamus quis aliquam mollitia vel! Nobis repudiandae temporibus doloribus id, voluptate vel.</p>
                            </div>
                            <div href="#!" class="d-flex col-4 p-0 pl-3">
                                <div class="image-cover image-cover--16x9 rounded-lg">
                                    <img class="img " src="{{ theme_url('images/16x9.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </a>
                </div>                
            </div>
            <nav aria-label="Page navigation" class="animation animation--on">
                <ul class="pagination justify-content-center pagination-01">
                  <li class="page-item disabled" aria-disabled="true" aria-label="« Previous"><span class="page-link page-prev" aria-hidden="true"><em class="ic-arrow-r prev"></em></span></li>
                  <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                  <li class="page-item"><a class="page-link" href="">2</a></li>
                  <li class="page-item"><a class="page-link" href="">3</a></li>
                  <li class="page-item"><a class="page-link" href="">4</a></li>
                  <li class="page-item"><a class="page-link" href="">5</a></li>
                  <li class="page-item"><a class="page-link page-next" href="#" rel="next" aria-label="Next »"><em class="ic-arrow-r"></em></a></li>
                </ul>
              </nav>
        </div>
    </div>
</section>
