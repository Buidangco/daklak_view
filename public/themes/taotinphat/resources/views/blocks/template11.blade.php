<section class="section section-project bg-gradient template-11">
    <h2 class="title-xl type02"><span>DỰ ÁN</span></h2>
    @foreach(__theme_get_list_pending_projects(2) as $pendingProject)
        <h3 class="title-art type-01"><span class="text-uppercase">{{ $pendingProject->name }}</span></h3>
        <div class="bg-white py-4">
            <div class="container">
                <div class="section_inner">
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <div class="box-content">
                                <div class="box-header">
                                    <div class="thumbnail mb-3">
                                        <a href="{{ $pendingProject->url }}"><img class="img-fluid" src="{{ $pendingProject->image }}" alt=""></a>
                                    </div>
                                </div>
                                <div class="box-body">
                                    {{--<ul class="list-unstyled list-infos">
                                        <li><strong>Lorem :</strong> <span>Lorem ipsum dolor sit amet.</span></li>
                                        <li><strong>Lorem Lorem :</strong> <span>Lorem ipsum dolor sit amet.</span></li>
                                    </ul>--}}
                                    <div class="c-dash text-center my-2">
                                        <img src="{{ theme_url('images/s-ic-dash-01.png') }}" alt="">
                                    </div>
                                    <p class="text text-justify">
                                        {!! $pendingProject->description ?: str_words(strip_tags($pendingProject->content), 50) !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="block-more text-center bg-white py-4">
        <a href="/du-an" class="btn btn-outline-secondary">
            <span>XEM THÊM</span>
        </a>
    </div>
</section>
