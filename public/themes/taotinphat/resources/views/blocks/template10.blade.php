<section class="section section-news template-10">
    <h2 class="title-xl"><span>TIN TỨC</span></h2>
    <div class="container">
        <div class="section_inner">
            <div class="row">
                @foreach(__theme_get_last_posts(2) as $lastPost)
                    <div class="col-md-12 col-lg-6">
                        <div class="media media-news p-2 bg-white">
                            <a href="{{ $lastPost->url }}" class="d-flex w-50 pr-3">
                                <img class="img-fluid" src="{{ $lastPost->image }}" alt="{{ $lastPost->name }}">
                            </a>
                            <div class="media-body">
                                <h5 class="mt-0">{{ $lastPost->published_at->format('d.m.Y') }}</h5>
                                <p class="text trimText clamp5">
                                    {!! $lastPost->description ?: str_words(strip_tags($lastPost->content), 50) !!}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="block-more text-center">
            <a href="/tin-tuc" class="scale-small mb-4">
                <em class="ic-right-red-small shadow"></em>
            </a>
        </div>
    </div>
</section>
