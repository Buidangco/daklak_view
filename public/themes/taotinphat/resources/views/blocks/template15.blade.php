<section class="section template-15">
    <div class="container">
        <div class="section_inner">
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="box-content box-content--float mb-3">
                        <div class="thumbnail col-7 p-0">
                            <div class="image-cover image-cover--16x9">
                                <img src="{{ theme_url('images/16x9.png') }}" alt="" class="img">
                            </div>
                        </div>
                        <div class="box-body">
                            <h3 class="mb-3 text-center"><span class="badge badge-pill py-3 px-5 bg-gradient text-white">TẦM NHÌN</span></h3>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim olore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerciLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                            
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim olore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="box-content box-content--float mb-3">
                        <div class="thumbnail col-7 p-0">
                            <div class="image-cover image-cover--16x9">
                                <img src="{{ theme_url('images/16x9.png') }}" alt="" class="img">
                            </div>
                        </div>
                        <div class="box-body">
                            <h3 class="mb-3 text-center"><span class="badge badge-pill py-3 px-5 bg-gradient text-white">SỨ MỆNH</span></h3>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim olore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerciLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                            
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim olore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="box-content box-content--float mb-3">
                        <div class="thumbnail col-7 p-0">
                            <div class="image-cover image-cover--16x9">
                                <img src="{{ theme_url('images/16x9.png') }}" alt="" class="img">
                            </div>
                        </div>
                        <div class="box-body">
                            <h3 class="mb-3 text-center"><span class="badge badge-pill py-3 px-5 bg-gradient text-white">GIÁ TRỊ CỐT LÕI</span></h3>
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim olore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerciLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                            
                            <p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim olore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>