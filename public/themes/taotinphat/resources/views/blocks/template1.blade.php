
<div class="content-about bg-white">
    <div class="container">
        <div class="sec-01">
            <h2 class="c-title-md animation">THÔNG ĐIỆP CHỦ TỊCH HĐQT</h2>
            <div class="sec-content row align-items-center">
                <div class="infos col-sm-4 animation">
                    <div class="thumbnail">
                        <img src="{{ theme_url('images/about-pic-01.png') }}" alt="" class="img-fluid">
                    </div>
                    <div class="name">
                        <span>Ông: </span>
                        <strong>LÊ VĂN TẠO</strong>
                    </div>
                    <div class="regency">
                        <span>CHỦ TỊCH HĐQT</span>
                    </div>
                </div>
                <div class="message col-sm-8 animation">
                    <div class="text">
                        {!! object_get($page, 'about_thong_diep_chu_tich_hdqt') !!}
                    </div>
                </div>
            </div>
            <div class="sec-content">
                <div class="row">
                    @foreach($page->children as $child)
                        <div class="col-md-4 col-sm-6 animation">
                            <div class="card card-03">
                                <div class="card-img-top box-shadown">
                                    <div class="thumbnail">
                                        <div class="image-cover image-cover--16x9">
                                            <img src="{{ $child->image }}" alt="" class="img">
                                        </div>
                                    </div>
                                    <div class="card-title"><span>{{ $child->name }}</span></div>
                                </div>
                                <div class="card-body">
                                    <div class="text-center">
                                        <a class="btn gradien-red btn-shadown" href="{{ $child->url }}" role="button">Chi tiết</a>
                                    </div>
                                    <p class="card-text trimText clamp5">
                                        {!! $child->description ?: str_limit(strip_tags($child->content), 200) !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
