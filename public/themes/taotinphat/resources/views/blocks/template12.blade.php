<section class="section section-active bg-gradient teamplate-12">
    <h2 class="title-xl"><span>HOẠT ĐỘNG</span></h2>
    <div class="container">
        <div class="section_inner text-white text-justify">
            <div class="content mb-4">
                {{ $page->home_activity_desc }}
            </div>
            <div class="block-video mb-4">
                <div class="video-background">
                    <div class="video-foreground">
                        <iframe width="100%" src="{{ __theme_get_youtube_embed(object_get($page, 'home_activity_video_1')) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="galary mb-4">
                <div class="swiper-container slider-section-active">
                    <div class="swiper-wrapper">
                        @foreach(__theme_get_last_post_of_category(2, 6) as $post)
                            <div class="swiper-slide">
                                <div class="item">
                                    <h5 class="title-md mb-2">
                                        <span class="trimText">{{ $post->name }}</span>
                                    </h5>
                                    <div class="image-cover image-cover--4x3 mb-2 rounded-lg">
                                        <img src="{{ $post->image }}" alt="{{ $post->name }}" class="img">
                                    </div>
                                    <div class="c-link text-center">
                                        <a href="{{ $post->url }}"><em class="ic-right-red-small shadow"></em></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
