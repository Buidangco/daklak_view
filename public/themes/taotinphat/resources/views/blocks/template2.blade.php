<div class="section section-05 bg-white">
    <div class="container">
        <div class="block-title type-01">
            <div class="title-img-02 animation m-auto">
                <img class="img-fluid" src="{{ theme_url('images/title-03.png') }}" alt="">
            </div>
        </div>
        <div class="section-inner animation">
            <article>
                {!! object_get($page, 'about_triet_ly_kinh_doanh') !!}
            </article>
        </div>
    </div>
</div>

@if ($ourTeams = get_our_teams())
    <div class="section-slider-about">
        <div class="container">
            <div class="slider-about animation">
                @foreach($ourTeams as $ourTeam)
                    <div class="item">
                        <a href="javascript:;" class="d-block" data-toggle="modal" data-target="#ourTeam{{ $ourTeam->id }}">
                            <div class="card card-about">
                                <div class="card-img-top">
                                    <div class="thumbnail">
                                        <img class="img-fluid" src="{{ $ourTeam->image }}" alt="{{ $ourTeam->name }}">
                                    </div>
                                </div>
                                <div class="card-body text-center">
                                    <span>{{ $ourTeam->title }}</span>
                                    <h5 class="card-name">{{ $ourTeam->name }}</h5>
                                    <p class="card-text">{{ $ourTeam->position }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                @foreach($ourTeams as $ourTeam)
                    <div class="item">
                        <a href="javascript:;" class="d-block" data-toggle="modal" data-target="#ourTeam{{ $ourTeam->id }}">
                            <div class="card card-about">
                                <div class="card-img-top">
                                    <div class="thumbnail">
                                        <img class="img-fluid" src="{{ $ourTeam->image }}" alt="{{ $ourTeam->name }}">
                                    </div>
                                </div>
                                <div class="card-body text-center">
                                    <span>{{ $ourTeam->title }}</span>
                                    <h5 class="card-name">{{ $ourTeam->name }}</h5>
                                    <p class="card-text">{{ $ourTeam->position }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="block-modal">
            @foreach($ourTeams as $ourTeam)
                <div class="modal fade" id="ourTeam{{ $ourTeam->id }}" tabindex="-1" role="dialog" aria-labelledby="ourTeam{{ $ourTeam->id }}" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="thumbnail">
                                    <img src="{{ $ourTeam->big_image }}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endif
