<div class="mv-slider">
    <div class="swiper-container slider01">
        <div class="swiper-wrapper">
            @foreach($sliderItems as $sliderItem)
                <div class="swiper-slide w-100">
                    <div class="item item01">
                        <div class="thumbnail text-center">
                            <div class="image-cover image-cover--16x9">
                                <a href="javascript:;" class="d-block">
                                    <img src="{{ $sliderItem->image }}" class="img" alt="{{ $sliderItem->name }}">
                                </a>
                            </div>
                        </div>

                        <div class="fr-view">
                            {!! $sliderItem->content !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"><em class="ic-next"></em></div>
        <div class="swiper-button-prev"><em class="ic-prev"></em></div>
    </div>
</div>
