<div class="mv">
    <div class="mv-slider">
        <div class="swiper-container slider01">
            <div class="swiper-wrapper">
                @foreach($project->gallery as $galleryItem)
                    <div class="swiper-slide w-100">
                        <div class="item item01">
                            <div class="thumbnail text-center">
                                <div class="image-cover image-cover--16x9">
                                    <a href="javascript:;" class="d-block">
                                        <img src="{{ $galleryItem }}" class="img" alt="{{ $project->name }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next"><em class="ic-next"></em></div>
            <div class="swiper-button-prev"><em class="ic-prev"></em></div>
        </div>
    </div>
</div>

<div class="screen-06">
    <div class="section section-01 bg-white pb-5">
        <div class="container">
            <div class="project-content fr-view">
                {!! $project->content !!}
            </div>
        </div>
    </div>
</div>

<div class="section section-form bg-white py-5" id="formContact">
    <div class="container">
        <div class="section__inner">
            @if($errors->count())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif

            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @else
                <form action="{{ route('theme.web.contact.care-project') }}#formContact" class="form form-02 shadow" method="POST">
                    @csrf
                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                    <h3 class="title-red text-center">Đăng ký nhận thông tin về dự án</h3>
                    <div class="form-body p-4">
                        <div class="form-row">
                            <div class="col-sm-12">
                                <div class="form-row form-group">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Tên liên hệ: </label>
                                    <div class="col-sm-10 ">
                                        <input type="text" class="form-control" name="customer_name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-row form-group">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Địa chỉ: </label>
                                    <div class="col-sm-10 ">
                                        <input type="text" class="form-control" name="customer_address">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-row form-group">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Điện thoại: </label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" name="customer_phone">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-row form-group">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Email: </label>
                                    <div class="col-sm-5">
                                        <input type="email" class="form-control" name="customer_email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-row form-group">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Nội dung: </label>
                                    <div class="col-sm-10">
                                        <textarea type="email" class="form-control" rows="7" name="content"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="text-center py-4">
                                    <button class="btn gradien-red shadow px-5">Gửi đi</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            @endif
        </div>
    </div>
</div>

@if($project_map_iframe = object_get($project, 'project_map_iframe'))
    <div class="section section-maps bg-white">
        <iframe src="{{ __theme_get_src_from_iframe($project_map_iframe) }}" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
@endif
