<ul class="nav nav-tabs scrollable">
    <li class="nav-item">
        <a class="nav-link active save-tab" data-toggle="pill" href="#projectProjectInfo">
            {{ __('project::project.tabs.info') }}
        </a>
    </li>
    @if(get_all_attributes_of_type(\Modules\Project\Models\Project::class)->count())
        <li class="nav-item">
            <a class="nav-link save-tab" data-toggle="pill" href="#projectProjectAttribute">
                {{ __('project::project.tabs.attribute') }}
            </a>
        </li>
    @endif
    <li class="nav-item">
        <a class="nav-link save-tab" data-toggle="pill" href="#projectProjectSeo">
            {{ __('project::project.tabs.seo') }}
        </a>
    </li>
</ul>

<div class="tab-content mt-3">
    <div class="tab-pane fade show active" id="projectProjectInfo">
        <div class="row">
            <div class="col-12 col-md-9">
                @input(['name' => 'name', 'label' => __('project::project.name')])
                @textarea(['name' => 'description', 'label' => __('project::project.description')])
                @editor(['name' => 'content', 'label' => __('project::project.content')])
                @gallery(['name' => 'gallery', 'label' => __('project::project.gallery')])
            </div>
            <div class="col-12 col-md-3">
                @checkbox(['name' => 'is_active', 'label' => __('project::project.is_active'), 'default' => true])
                @sumoselect(['name' => 'category_id', 'label' => __('project::project.category'), 'multiple' => false, 'options' => get_project_category_parent_options()])
                @tags
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="projectProjectAttribute">
        @attributes(['entityType' => \Modules\Project\Models\Project::class])
    </div>

    <div class="tab-pane fade" id="projectProjectSeo">
        @seo(['slugPrefix' => 'du-an'])
    </div>
</div>
