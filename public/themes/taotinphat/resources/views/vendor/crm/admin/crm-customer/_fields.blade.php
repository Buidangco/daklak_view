<ul class="nav nav-tabs scrollable">
    <li class="nav-item">
        <a class="nav-link active save-tab" data-toggle="pill" href="#customerItem">
            {{ __('crm::crm_customer.tabs.information') }}
        </a>
    </li>
</ul>

<div class="tab-content mt-3">

    <div class="tab-pane fade show active" id="customerItem">
        <div class="row">
            <div class="col-12 col-md-6">
                @input(['name'  => 'first_name', 'label' => __('crm::crm_customer.first_name')])
                @input(['name'  => 'last_name', 'label' => __('crm::crm_customer.last_name')])
                @input(['name'  => 'email', 'label' => __('crm::crm_customer.email')])
                @input(['name'  => 'phone', 'label' => __('crm::crm_customer.phone')])
                @dateinput(['name' => 'birthday', 'label' => __('crm::crm_customer.birthday')])
            </div>
            <div class="col-12 col-md-6">
                <h4>Ghi chú</h4>

                @if(!is_null($item) && $item->notes->count() > 0)
                    @foreach($item->notes as $note)
                        <p><strong>{{ object_get($note, 'creator.name', 'Sytem') }}</strong> - {{ $note->created_at }}</p>
                        <p>{{ $note->content }}</p>
                    @endforeach
                @endif
                @textarea(['name' => 'content_note', 'label' => __('crm::crm_note.content_note')])
            </div>
        </div>
    </div>
</div>
