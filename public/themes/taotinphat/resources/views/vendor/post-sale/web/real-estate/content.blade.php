<div class="page-top bg-white">
    @if($realEstate->images->count()))
        <div class="mv">
            <div class="mv-slider">
                <div class="swiper-container slider01">
                    <div class="swiper-wrapper">
                        @foreach($realEstate->images as $galleryItem)
                            <div class="swiper-slide w-100">
                                <div class="item item01">
                                    <div class="thumbnail text-center">
                                        <div class="image-cover image-cover--16x9">
                                            <a href="javascript:;" class="d-block">
                                                <img src="{{ $galleryItem }}" class="img" alt="{{ $realEstate->name }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-next"><em class="ic-next"></em></div>
                    <div class="swiper-button-prev"><em class="ic-prev"></em></div>
                </div>
            </div>
        </div>
    @endif
    <div class="section section-01 bg-white pb-5">
        <div class="container">            
            <div class="row py-3">
                <div class="col-4 order-2" id="sticky-sidebar">
                    <div class="sticky-top">
                        <div class="flex-column">
                            <div class="card card-contact border-0">
                                <h3 class="title-red text-center">THÔNG TIN LIÊN HỆ</h3>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td class="w-25">
                                                <div class="thumbnail w-60 m-auto">
                                                    <span class="image-cover image-cover--1x1 rounded-circle">
                                                    <img class="img" src="{{ theme_url('images/video.jpg') }}" alt="Circle image">
                                                    </span>
                                                </div>
                                            </td>
                                            <td >
                                                <strong class="text-uppercase ">vÕ PHƯƠNG Hà</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                                            </td>
                                            <td><span>105 Nguyễn Dình Chiểu, Phường 27, Quận Bình Thạnh, TP-HCM</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="contact-phome-number">
                                        <span class="phone-detail d-flex"><i class="fa fa-phone" aria-hidden="true"></i><span data-phone="123456789">12345678***</span></span>
                                        <a href="#!" class="show-phone-detail text-white">BẤM ĐỂ HIỆN SỐ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8" id="main">
                    <div class="section-inner">
                        <div class="text-headline text-justify">
                            <h2 class="title-lg">{{ $realEstate->name }}</h2>

                        </div>
                        <div class="infor-fast">
                            <div class="d-flex">
                               
                            </div>
                            <div class="d-flex">
                                <div class="p-2 flex-fill"> <span>Giá: <strong class="text-primary">4 tỷ</strong></span></div>
                                <div class="p-2 flex-fill"> <span>Diện tích: <strong class="text-primary">86<sup>2</sup></strong></span></div>
                                <div class="p-2 flex-fill text-right"> <span>Quận huyện: <strong class="text-primary">Bình Thạnh, HCM</strong></span></div>
                              </div>
                              <hr class="mb-4">
                        </div>
                        <div class="page-content">
                            <article class="fr-view">
                                {!! $realEstate->content !!}
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-03 bg-white pb-5">
        <div class="container">
            <div class="section-inner">
                <div class="animation">
                    <div class="title-group">
                        <h3>Có thể bạn quan tâm</h3>
                    </div>
                </div>

                @if($relatedRealEstates = __theme_get_related_real_estates($realEstate, 3))
                    <div class="slider05 animation">
                        @foreach($relatedRealEstates as $relatedRealEstate)
                            <div class="item px-3 pb-4">
                                <div class="card card-02">
                                    <div class="card-img-top">
                                        <div class="image-cover image-cover--4x3">
                                            <a href="{{ $relatedRealEstate->url }}">
                                                <img src="{{ $relatedRealEstate->image }}" alt="{{ $relatedRealEstate->name }}" class="img">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="d-flex mb-3">
                                            <div class="flex-fill"> <span>Giá: <strong class="text-primary">4 tỷ</strong></span></div>
                                            <div class="flex-fill text-right"> <span>Diện tích: <strong class="text-primary">86<sup>2</sup></strong></span></div>
                                          </div>
                                        <a href="{{ $relatedRealEstate->url }}" title=">{{ $relatedRealEstate->name }}">
                                            <h4 class="card-title trimText clamp3">{{ $relatedRealEstate->name }}</h4>
                                        </a>
                                        <p class="card-text">
                                            {!! $relatedRealEstate->desctiption ?: str_limit(strip_tags($relatedRealEstate->content), 200) !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@include('blocks.template6')
