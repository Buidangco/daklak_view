<div class="page-tuyen-dung">
    @include('blocks.slider1')
    <div class="pc">
        <div class="section section-01 bg-white">
            <div class="container">
                <div class="section-inner">
                    <div class="row">
                        @foreach($jobs = __theme_get_list_jobs() as $job)
                            <div class="col-md-4 col-sm-6 animation">
                                <div class="card card-01">
                                    <div class="card-body">
                                        <a href="{{ $job->url }}">
                                            <h4 class="card-title">
                                                {{ $job->name }}
                                            </h4>
                                        </a>
                                        <p class="card-text">
                                            {!! $job->description ?: str_limit(strip_tags($job->content), 200) !!}
                                        </p>
                                        <div class="card-btn">
                                            <a class="btn gradien-red btn-shadown" href="{{ $job->url }}" role="button">
                                                Xem
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    {{ $jobs->withQueryString()->onEachSide(1)->render('pagination.ttp') }}
                </div>
            </div>
        </div>
    </div>
    <div class="sp">
        <div class="page-title mb-4 pt-5">
            <h1 class="mb-3"><span class="px-3"><img src="{{ theme_url('images/tuyen-dung-01.jpg') }}" alt="" class="img-fluid"></span></h1>
        </div>
        <div class="thumbnail mb-4">
            <img class="img-fluid" src="{{ theme_url('images/tuyen-dung-02.jpg') }}" alt="">
        </div>
        <div class="thumbnail">
            <img class="img-fluid" src="{{ theme_url('images/tuyen-dung-03.jpg') }}" alt="">
        </div>
        <div class="text-center">
            <h3 class="text-gray">Liên Hệ Phòng Nhân Sự <br>
                Liên hệ: 0988573797 & 02836367636 ( Gặp Ms Hạnh )</h3>
        </div>
    </div>
</div>

@include('blocks.template6')
