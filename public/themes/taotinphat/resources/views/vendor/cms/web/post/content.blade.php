<div>
    <div class="section section-01 bg-white pb-5">
        <div class="container">
            <div class="section-inner">
                <div class="text-headline text-justify">
                    <h2>{{ $post->name }}</h2>
                </div>

                <div class="page-content">
                    <article class="fr-view">
                        {!! $post->content !!}
                    </article>
                </div>
            </div>
        </div>
    </div>

    <div class="section section-03 bg-white pb-5">
        <div class="container">
            <div class="section-inner">
                <div class="animation">
                    <div class="title-group">
                        <h3>Có thể bạn quan tâm</h3>
                    </div>
                </div>

                @if($relatedPosts = __theme_get_related_post($post, 3))
                    <div class="slider05 animation">
                        @foreach($relatedPosts as $relatedPost)
                            <div class="item px-3 pb-4">
                                <div class="card card-02">
                                    <div class="card-img-top">
                                        <div class="image-cover image-cover--4x3">
                                            <a href="{{ $relatedPost->url }}">
                                                <img src="{{ $relatedPost->image }}" alt="{{ $relatedPost->name }}" class="img">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{ $relatedPost->url }}">
                                            <h4 class="card-title">{{ $relatedPost->name }}</h4>
                                        </a>
                                        <p class="card-text">
                                            {!! $relatedPost->desctiption ?: str_limit(strip_tags($relatedPost->content), 200) !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@include('blocks.template6')
