<div class="page-top">
    <div class="section section-01 bg-white  pb-5">
        <div class="container">
            <div class="section-inner">
                <div class="text-headline text-justify">
                    <h2 class="title-lg">{{ $page->name }}</h2>
                </div>

                <div class="page-content">
                    <article class="fr-view">
                        {!! $page->content !!}
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>

@include('blocks.template6')
