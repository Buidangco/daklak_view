<div class="page-mua-ban">
    @if($stickyRealEstates = __theme_get_sticky_real_estates())
        <div class="mv">
            <div class="mv-slider slider03">
                <div class="swiper-container gallery-top pb-0">
                    <div class="swiper-wrapper">
                        @foreach($stickyRealEstates as $stickyRealEstate)
                            <div class="swiper-slide">
                                <div class="thumbnail">
                                    <a href="{{ $stickyRealEstate->url }}" class="d-block image-cover image-cover--16x9">
                                        <img src="{{ $stickyRealEstate->image }}" alt="{{ $stickyRealEstate->name }}" class="img">
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-controll">
                        <div class="swiper-button-next"><em class="ic-control-slider03"></em></div>      
                        <div class="swiper-button-prev reverse"><em class="ic-control-slider03"></em></div>                            
                    </div>
                </div>
                    <div class="container">
                <div class="gallery-thumbs-wrap py-3">
                        <div class="swiper-container gallery-thumbs pb-0">
                            <div class="swiper-wrapper">
                                @foreach($stickyRealEstates as $stickyRealEstate)
                                    <div class="swiper-slide">
                                        <div class="image-cover image-cover--16x9">
                                            <img src="{{ $stickyRealEstate->image }}" alt="{{ $stickyRealEstate->name }}" class="img">
                                        </div>
                                        {{-- <h3 class="title text-center text-white my-3 trimText clamp2">
                                            {{ $stickyRealEstate->name }}
                                        </h3> --}}
                                    </div>
                                @endforeach
                            </div>
                        </div>   
                        
                        <div class="swiper-controll">
                            <div class="swiper-button-next"><img src="{{ theme_url('images/icon/ic-arrow-gray.png') }}" alt="" width="20"></div>      
                            <div class="swiper-button-prev reverse"><img src="{{ theme_url('images/icon/ic-arrow-gray.png') }}" alt="" width="20"></div>                            
                        </div>             
                </div>
                    </div>
                {{-- <div class="swiper-pagination swiper-pagination-03"></div> --}}
            </div>
        </div>
    @endif

    @if($listRealEstates = __theme_get_active_real_estates())
        <div class="sp">
            <div class="page-title mb-4 mt-3">
                <h1><span>ĐĂNG TIN MUA BÁN</span></h1>
            </div>
            {{--<nav aria-label="breadcrumb" class="bg-gradient breadcrumb-01 mb-5">
                <ol class="breadcrumb shadow-sm">
                    <li class="breadcrumb-item"><a href="#">TRANG CHỦ</a></li>
                    <li class="breadcrumb-item"><a href="#">TITLE HEADLINE</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="#">TITLE HEADLINE</a></li>
                </ol>
            </nav>--}}
        </div>
        <div class="section section-s7lock01 bg-white pb-4 mb-5">
            <div class="container">
                <div class="section__inner">
                    <div class="block-news">
                        @foreach($listRealEstates as $listRealEstate)
                            <div class="media media-02 animation">
                                <div class="thumbnail">
                                    <div class="image-cover image-cover--16x9">
                                        <a href="{{ $listRealEstate->url }}">
                                            <img class="img" src="{{ $listRealEstate->image }}" alt="{{ $listRealEstate->name }}">
                                        </a>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="media-detail">
                                        <a href="{{ $listRealEstate->url }}">
                                            <h5 class="mt-0 mb-1 trimText clamp2">
                                                {{ $listRealEstate->name }}
                                            </h5>
                                        </a>
                                        <p class="trimText clamp4">
                                            {!! $listRealEstate->description ?: str_limit(strip_tags($listRealEstate->content), 500) !!}
                                        </p>
                                    </div>
                                    <div class="d-flex media-infos">
                                        <ul class="list-inline list-infos-post">
                                            @if($listRealEstate->price)
                                                <li class="list-inline-item">Giá: <strong class="text-primary">{{ number_format_short($listRealEstate->price) }}</strong></li>
                                            @endif
                                            @if($listRealEstate->area)
                                                <li class="list-inline-item">Diện tích: <strong class="text-primary">{{ $listRealEstate->area }} m<sup>2</sup></strong></li>
                                            @endif
                                            @if($listRealEstate->district)
                                                <li class="list-inline-item">Quận huyện: <strong class="text-primary">{{ $listRealEstate->district_province }}</strong></li>
                                            @endif
                                        </ul>
                                        <span class="time ml-auto">
                                            {{ $listRealEstate->created_at->format('d/m/Y') }}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            {{ $listRealEstates->withQueryString()->onEachSide(1)->render('pagination.ttp') }}
        </div>
        <div class="sp">
            {{ $listRealEstates->withQueryString()->onEachSide(1)->render('pagination.ttp_mobile') }}
            <hr class="hr-red">
        </div>
    @endif
    <div class="section section-form bg-white" id="dangtin">
        <div class="container">
            <div class="section__inner animation pt-5">
                @if($errors->count())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @else
                    <form action="{{ route('post-sale.web.real-estate.create') }}#dangtin" class="form form-02 shadow" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h3 class="title-red text-center">Đăng Thông Tin Mua Bán</h3>
                        <div class="form-body p-4">
                            <div class="form-row">
                                <div class="col-sm-12 mb-4">
                                    <div class="form-row form-group">
                                        <label class="col-sm-2 col-form-label">Tiêu đề: </label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-row form-group">
                                        <label class="col-sm-4 col-form-label">Hình thức: </label>
                                        <div class="col-sm-8">
                                            <select class="form-control" required>
                                                <option>-- Lựa chọn --</option>
                                                <option value="buy">Cần mua</option>
                                                <option value="sale" selected>Cần bán</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-row form-group">
                                        <label class="col-sm-4 col-form-label">Đường/Phố: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="street" value="{{ old('street') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-row form-group">
                                        <label class="col-sm-4 col-form-label">Tỉnh/Thành phố: </label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="province_id" id="province_id" required>
                                                <option>-- Lựa chọn --</option>
                                                @foreach(get_zone_provice_options() as $province)
                                                    <option value="{{ $province['value'] }}" {{ $province['value'] == old('province_id') ? 'selected' : '' }}>
                                                        {{ $province['label'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-row form-group">
                                        <label class="col-sm-4 col-form-label">Diện tích: </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="area" value="{{ old('area') }}">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">m<sup>2</sup></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-row form-group">
                                        <label class="col-sm-4 col-form-label">Quận huyện: </label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="district_id" id="district_id">
                                                <option>-- Lựa chọn --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-row form-group">
                                        <label class="col-sm-4 col-form-label">Giá: </label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="price" value="{{ old('price') }}">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">triệu đồng</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label class="col-sm-2 col-form-label">Mô tả: </label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" rows="7" name="content">{{ old('content') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group row form-group-upload mt-4">
                                        <div class="col-sm-6 text-center">
                                            <div class="upload-btn w-50 ml-auto">
                                                <label for="up-file">Tải hình ảnh lên</label>
                                                <input type="file" class="form-control d-none" id="up-file" placeholder="" name="images[]" multiple accept="image/*">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 text-center">
                                            <button class="btn gradien-red w-50 shadow">Gửi đi</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="title-red text-center">Thông Tin Liên Hệ</h3>
                        <div class="form-body p-4">
                            <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Tên liên hệ: </label>
                                        <div class="col-sm-10 ">
                                            <input type="text" class="form-control" name="customer_name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Địa chỉ: </label>
                                        <div class="col-sm-10 ">
                                            <input type="text" class="form-control" name="customer_address">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Điện thoại: </label>
                                        <div class="col-sm-5">
                                            <input type="tel" class="form-control" name="customer_phone" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Email: </label>
                                        <div class="col-sm-5">
                                            <input type="email" class="form-control" name="customer_email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="title-red text-center">Lịch Đăng Tin</h3>
                        <div class="form-body p-4">
                            <div class="form-row">
                                <div class="col-sm-6">
                                    <div class="form-row form-group">
                                        <label class="col-sm-4 col-form-label">Ngày bắt đầu: </label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" name="start_at">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-row form-group">
                                        <label class="col-sm-4 col-form-label">Ngày kết thúc:</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" name="end_at">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>

    @include('blocks.template6')
</div>

@assetadd('zone_address', theme_url('js/zone_address.js'), ['jquery'])
