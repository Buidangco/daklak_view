<div class="page-top">
    <div class="page-title sp bg-white py-3">
        <h1> <em class="s-ic-home">home page</em></h1>
    </div>
    @include('blocks.slider1')
    <div class="pc">
        @include('blocks.template8')
        @include('blocks.template9')
        @include('blocks.template7')
    </div>
    <div class="sp">
        @include('blocks.template12')
        @include('blocks.template11')
        @include('blocks.template13')
    </div>
</div>

@include('blocks.template6')
