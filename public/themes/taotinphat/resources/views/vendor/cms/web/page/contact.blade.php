<div class="page-top">
    <div class="section section-01 bg-white pb-5">
        <div class="container">
            <div class="section-inner pb-5">
                <div class="page-content">
                    <article class="fr-view">
                        {!! $page->content !!}
                    </article>
                </div>
            </div>

            <div class="section-inner mt-5">
                @if($errors->count())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif

                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @else
                    <form action="{{ route('theme.web.contact.contact') }}#formContact" class="form form-02 shadow" method="POST">
                        @csrf

                        <h3 class="title-red text-center">Liên hệ</h3>
                        <div class="form-body p-4">
                            <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Họ và tên: </label>
                                        <div class="col-sm-10 ">
                                            <input type="text" class="form-control" name="customer_name">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Điện thoại: </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="customer_phone">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Email: </label>
                                        <div class="col-sm-5">
                                            <input type="email" class="form-control" name="customer_email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-row form-group">
                                        <label for="staticEmail" class="col-sm-2 col-form-label">Nội dung: </label>
                                        <div class="col-sm-10">
                                            <textarea type="email" class="form-control" rows="7" name="content"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="text-center py-4">
                                        <button class="btn gradien-red shadow px-5">Gửi đi</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>

@include('blocks.template6')
