<div>
    @include('blocks.slider1')

    <div class="section section-03 bg-white pb-5">
        <div class="container">
            <div class="section-inner">
                @if($stickyPosts = __theme_get_last_post_sticky(3))
                    <div class="text-headline text-justify">
                        <h2>Thông tin nổi bật</h2>
                    </div>
                    <div class="row">
                        @foreach($stickyPosts as $post)
                            <div class="col-md-4 col-sm-6 animation">
                                <div class="card card-02">
                                    <div class="card-img-top-remove">
                                        <div class="image-cover image-cover--16x9">
                                            <a href="{{ $post->url }}">
                                                <img src="{{ $post->image }}" alt="{{ $post->name }}" class="img">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <a href="{{ $post->url }}">
                                            <h4 class="card-title">{{ $post->name }}</h4>
                                        </a>
                                        <p class="card-text">
                                            {!! $post->desctiption ?: str_limit(strip_tags($post->content), 200) !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif

                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-headline text-justify mt-5 pt-3">
                            <h2>Tin tức mới nhất</h2>
                        </div>
                    </div>
                    <div class="col-md-8">
                        @if($posts = __theme_get_last_posts(request('max', 9), $stickyPosts))
                            @foreach($posts as $post)
                                <div class="c-list-news pl-0">
                                    <a href="{{ $post->url }}" class="d-block" >
                                        <div class="c-list-item box-shadown animation">
                                            <div class="c-list-content">
                                                <h3 class="c-list-title">{{ $post->name }}</h3>
                                                <p class="c-list-text">
                                                    {!! $post->desctiption ?: str_limit(strip_tags($post->content), 200) !!}
                                                </p>
                                            </div>
                                            <div class="c-list-thumbnail">
                                                <div class="image-cover image-cover--100h">
                                                    <img src="{{ $post->image }}" alt="{{ $post->name }}" class="img">
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                            {!! $posts->withQueryString()->onEachSide(1)->render('pagination.ttp') !!}
                        @endif
                    </div>
                    <div class="col-md-4" id="sticky-sidebar">
                            @include('blocks.email-du-an')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('blocks.template6')
