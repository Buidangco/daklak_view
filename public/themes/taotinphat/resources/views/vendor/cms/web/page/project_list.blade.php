<div class="page-du-an">
    @include('blocks.slider1')

    <div class="section section-processing bg-white ">
        <div class="container">
            <div class="section-inner">
                <div class="row">
                    @foreach($projectLists as $project)
                        <div class="col-md-4 col-sm-6 animation">
                            <div class="card card-03">
                                <div class="card-img-top box-shadown">
                                    <div class="thumbnail">
                                        <div class="image-cover image-cover--4x3">
                                            <a href="{{ $project->url }}">
                                                <img src="{{ $project->image }}" alt="{{ $project->name }}" class="img">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-title trimText">{{ $project->name }}</div>
                                </div>
                                <div class="card-body">
                                    <div class="text-center">
                                        <a class="btn gradien-red btn-shadown" href="{{ $project->url }}" role="button">
                                            Chi tiết
                                        </a>
                                    </div>
                                    <p class="card-text trimText clamp4">
                                        {!! $project->description ?: str_limit(strip_tags($project->content), 200) !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                {!! $projectLists->withQueryString()->onEachSide(1)->render('pagination.ttp') !!}
            </div>
        </div>
    </div>
</div>

@include('blocks.template6')
