<ul class="nav nav-tabs scrollable">
    <li class="nav-item">
        <a class="nav-link active save-tab" data-toggle="pill" href="#cmsCategoryInfo">
            {{ __('cms::category.tabs.info') }}
        </a>
    </li>
    @if(get_all_attributes_of_type(\Modules\Cms\Models\Category::class)->count())
        <li class="nav-item">
            <a class="nav-link save-tab" data-toggle="pill" href="#cmsCategoryAttribute">
                {{ __('cms::category.tabs.attribute') }}
            </a>
        </li>
    @endif
    <li class="nav-item">
        <a class="nav-link save-tab" data-toggle="pill" href="#cmsCategorySeo">
            {{ __('cms::category.tabs.seo') }}
        </a>
    </li>
</ul>

<div class="tab-content mt-3">
    <div class="tab-pane fade show active" id="cmsCategoryInfo">
        <div class="row">
            <div class="col-12 col-md-9">
                @input(['name' => 'name', 'label' => __('cms::category.name')])
                @textarea(['name' => 'description', 'label' => __('cms::category.description')])
                @editor(['name' => 'content', 'label' => __('cms::category.content')])
            </div>
            <div class="col-12 col-md-3">
                @checkbox(['name' => 'is_active', 'label' => __('cms::category.is_active'), 'default' => true])
                @select(['name' => 'parent_id', 'label' => __('cms::category.parent'), 'options' => get_category_parent_options()])
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="cmsCategoryAttribute">
        @attributes(['entityType' => \Modules\Cms\Models\Category::class])
    </div>

    <div class="tab-pane fade" id="cmsCategorySeo">
        @seo(['slugPrefix' => 'category'])
    </div>
</div>
