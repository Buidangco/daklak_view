<div class="page-sub">
    <div class="section section-01 bg-white  pb-5">
        <div class="container">
            <div class="section-inner">
                <div class="text-headline text-justify">
                    <h2 class="title-lg">{{ $job->name }}</h2>
                </div>

                <div class="job-content fr-view">
                    {!! $job->content !!}
                </div>

                <div class="section section-form bg-white" id="applyform">
                    <div class="container">
                        <div class="section__inner animation">
                            @if($errors->count())
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            @endif

                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @else
                                <form action="{{ route('recruitment.web.applicant.store') }}#applyform" class="form form-01" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="job_id" value="{{ $job->id }}">

                                    <h3 class="title text-center text-uppercase">Ứng tuyển</h3>
                                    <div class="form-group row line pc">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" placeholder="Họ và tên" id="form-control-01" name="name" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row line pc">
                                        <div class="col-sm-12">
                                            <input type="email" class="form-control" placeholder="Email" id="form-control-02" name="email" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row line sp">
                                        <div class="col-sm-12">
                                        <label class="text-center">Thông tin cơ bản</label>
                                            <textarea class="form-control" rows="5"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row line pc">
                                        <div class="col-sm-12">
                                            <input type="tel" class="form-control" placeholder="Số điện thoại" id="form-control-03" name="phone" value="{{ old('phone') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row form-group-upload">
                                        <label class="col-sm-12 sp text-center">File CV</label>
                                        <div class="col-sm-6">
                                            <div class="upload-btn">
                                                <label for="up-file">File CV</label>
                                                <input type="file" class="form-control d-none" id="up-file" placeholder="" name="cv">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="btn btn-send">Gửi đi</button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('blocks.template6')
