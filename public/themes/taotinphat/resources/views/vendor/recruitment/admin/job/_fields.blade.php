<ul class="nav nav-tabs scrollable">
    <li class="nav-item">
        <a class="nav-link active save-tab" data-toggle="pill" href="#recruitmentJobInfo">
            {{ __('recruitment::job.tabs.info') }}
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link save-tab" data-toggle="pill" href="#recruitmentJobSeo">
            {{ __('recruitment::job.tabs.seo') }}
        </a>
    </li>
</ul>

<div class="tab-content mt-3">
    <div class="tab-pane fade show active" id="recruitmentJobInfo">
        <div class="row">
            <div class="col-12 col-md-9">
                @input(['name' => 'name', 'label' => __('recruitment::job.name')])
                @textarea(['name' => 'description', 'label' => __('recruitment::job.description')])
                @editor(['name' => 'content', 'label' => __('recruitment::job.content')])
            </div>
            <div class="col-12 col-md-3">
                @checkbox(['name' => 'is_active', 'label' => __('recruitment::job.is_active'), 'default' => true])
                <div class="row mx-n1">
                    <div class="col px-1">
                        @input(['name' => 'min_salary', 'label' => '', 'placeholder' => __('recruitment::job.min_salary'), 'mask' => 'money'])
                    </div>
                    <div class="col-auto px-1">
                        <div class="pt-3">-</div>
                    </div>
                    <div class="col px-1">
                        @input(['name' => 'max_salary', 'label' => '', 'placeholder' => __('recruitment::job.max_salary'), 'mask' => 'money'])
                    </div>
                </div>
                @sumoselect(['name' => 'category_id', 'label' => __('recruitment::job.category'), 'options' => get_recruitment_category_options()])
                @sumoselect(['name' => 'location_id', 'label' => __('recruitment::job.location'), 'options' => get_recruitment_location_options()])
                @sumoselect(['name' => 'type_id', 'label' => __('recruitment::job.type'), 'options' => get_recruitment_type_options()])
                @dateinput(['name' => 'expired_at', 'label' => __('recruitment::job.expired_at'), 'value' => object_get($item, 'expired_at') ? $item->expired_at->format('Y-m-d') : ''])
                @mediafile(['name' => 'image', 'label' => __('recruitment::job.image')])
                @input(['name' => 'sort_order', 'label' => __('recruitment::job.sort_order')])
                @tags
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="recruitmentJobSeo">
        @seo(['slugPrefix' => 'tuyen-dung'])
    </div>
</div>
