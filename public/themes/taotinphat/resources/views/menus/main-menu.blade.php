<ul class="navbar-nav ml-auto main-menu">
    @foreach($items as $item)
        <li class="nav-item menu-item menu-item-{{ $item->id }} {{ $item->class }} {{ $item->isActive() ? 'active' : '' }}">
            <a class="nav-link {{ $item->isActive() ? 'active' : '' }}" href="{{ $item->getUrl() }}" target="{{ $item->target }}">
                {!! $item->icon !!}
                <span class="menu-item-label">
                    {{ $item->label }}
                </span>
            </a>
            @if($item->children->count())
                @include('menus.main-menu-child', ['items' => $item->children])
            @endif
        </li>
    @endforeach
</ul>
