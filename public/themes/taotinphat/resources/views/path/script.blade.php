{{--@assetadd('polyfill', 'https://cdn.polyfill.io/v2/polyfill.min.js')--}}
@assetadd('jquery', theme_url('js/jquery-3.2.1.min.js'))
@assetadd('bootstrap', theme_url('js/bootstrap.min.js'), ['jquery'])
@assetadd('swiper', theme_url('js/swiper.min.js'), ['jquery'])
@assetadd('slick', theme_url('js/slick.min.js'), ['jquery'])
@assetadd('main', theme_url('js/main.js'), ['jquery', 'bootstrap'])
@assetadd('custom', theme_url('js/custom.js'), ['jquery', 'bootstrap'])
<script src="https://use.fontawesome.com/846970aa94.js"></script>
