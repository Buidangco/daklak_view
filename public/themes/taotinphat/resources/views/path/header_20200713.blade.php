<header class="header">
    <div class="header_top">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light p-0">
                <a href="{{ url('/') }}" class="logo">
                    <img src="{{ theme_url('images/logo.png') }}" alt="" class="pc">
                    <img src="{{ theme_url('images/logo-sp.png') }}" alt="" class="sp">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-menu"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarMain">
                    {!! FrontendMenu::render('main-menu', 'menus.main-menu') !!}
                    <form class="form form--search">
                        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
                    </form>
                </div>
            </nav>
        </div>
    </div>
</header>
