<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,600;0,900;1,600;1,900&display=swap" rel="stylesheet">
@assetadd('bootstrap', theme_url('css/lib/bootstrap.min.css'))
@assetadd('swiper', theme_url('css/lib/swiper.min.css'))
@assetadd('slick', theme_url('css/lib/slick.css'))
@assetadd('fancybox', theme_url('css/lib/jquery.fancybox.css'))
@assetadd('style', theme_url('css/style.css'))
@assetadd('style.custom', theme_url('css/custom.css'))
@assetadd('froala_style', asset("vendor/newnet-admin/plugins/froala/css/froala_style.min.css"))
