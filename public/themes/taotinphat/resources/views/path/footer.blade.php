<div class="section section-partner bg-white">
    <div class="container">
        <div class="section-inner">
            <div class="block block-01">
                <div class="media align-items-center">
                    <div class="d-flex align-self-center mr-3">
                        <a class="btn gradien-red" href="javascript:;" role="button">Đối tác</a>
                    </div>
                    <div class="media-body">
                        <p>{{ setting('doi_tac_desc') }}</p>
                    </div>
                </div>
                <div class="swiper-container slider04">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="thumbnail text-center">
                                    <img class="img-fluid" src="{{ theme_url('images/par-img-01.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="thumbnail text-center">
                                    <img class="img-fluid" src="{{ theme_url('images/par-img-02.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="thumbnail text-center">
                                    <img class="img-fluid" src="{{ theme_url('images/par-img-03.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="thumbnail text-center">
                                    <img class="img-fluid" src="{{ theme_url('images/par-img-04.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="thumbnail text-center">
                                    <img class="img-fluid" src="{{ theme_url('images/par-img-05.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="thumbnail text-center">
                                    <img class="img-fluid" src="{{ theme_url('images/par-img-06.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="item">
                                <div class="thumbnail text-center">
                                    <img class="img-fluid" src="{{ theme_url('images/par-img-07.jpg') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
    <div class="footer_top">
        <div class="pc">
            <ul class="list-unstyled text-center text-white">
                <li>Công Ty TNHH Phát Triển Hạ Tầng Bất Động Sản Tạo Tín Phát.</li>
                <li>Địa chỉ: 105 Bình Quới, P.27, Q.Bình thạnh, Tp.HCM</li>
                <li>Email: Taotinphat@gmail.com</li>
                <li>Điện thoại: <a href="tel:+842836367636" class="text-white">02836367636</a></li>
                <li>Website: Taotinphat.com</li>
            </ul>
        </div>
        <div class="sp">
            <h3 class="f-title-01"><span>LIÊN HỆ</span></h3>
            <ul class="list-unstyled list-social">
                <li><a href="#!"><em class="ic-face"></em></a></li>
                <li><a href="#!"><em class="ic-phone"></em></a></li>
                <li><a href="#!"><em class="ic-youtube"></em></a></li>
            </ul>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="pc">
            <p>Kết nối với chúng tôi</p>
            <ul class="list-unstyled list-social">
                <li><a href="#!"><em class="ic-face"></em></a></li>
                <li><a href="#!"><em class="ic-phone"></em></a></li>
                <li><a href="#!"><em class="ic-youtube"></em></a></li>
            </ul>
        </div>
        <div class="sp">
            <h3 class="f-title-01"><span>Công Ty TNHH Phát Triển Hạ Tầng Bất Động Sản Tạo Tín Phát.</span></h3>
            <ul class="list-unstyled list-infos">
                <li><a href="#!"><em class="icon-local"></em><span>Tp. Hồ Chí Minh: 105 Bình Quới, P.27, Q.Bình thạnh, Tp.HCM</span></a></li>
                <li><a href="#!"><em class="icon-mail"></em><span>Taotinphat@gmail.com</span></a></li>
                <li><a href="#!"><em class="icon-phone"></em><span>02836367636</span></a></li>
                <li><a href="#!"><em class="icon-global"></em><span>Taotinphat.com</span></a></li>
            </ul>
        </div>
    </div>
    </div>
    <a href="#!" id="back-top" style=""><em class="ic-next"></em></a>
</footer>
