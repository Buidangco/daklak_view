<header class="header">
    <div class="header_top">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light p-0">
                <a href="{{ url('/') }}" class="logo">
                    <img src="{{ theme_url('images/logo.png') }}" alt="" class="pc">
                    <img src="{{ theme_url('images/logo-sp.png') }}" alt="" class="sp">
                </a>
                <a href="#!" class="sp js-active-search"><em class="s-ic-search-1"></em></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-menu"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarMain">
                    {!! FrontendMenu::render('main-menu', 'menus.main-menu') !!}

                    <form class="form form--search pc" action="{{ route('theme.web.search.index') }}">
                        <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="s">
                        <button class="sub-mit"><em class="ic-search"></em></button>
                    </form>
                </div>
                <form class="form form--search sp" action="{{ route('theme.web.search.index') }}">
                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" name="s">
                    <button class="sub-mit"><em class="ic-search"></em></button>
                </form>
            </nav>
        </div>
    </div>
</header>
