@extends('master')

@section('content')
    <div class="section section-03 bg-white pb-5">
        <div class="container">
            <div class="section-inner">
                <h1>Kết quả tìm kiếm: {{ request('s') }}</h1>

                <div class="search-result">
                    @foreach($posts as $post)
                        <div class="c-list-news pl-0">
                            <a href="{{ $post->url }}" class="d-block" >
                                <div class="c-list-item box-shadown animation">
                                    <div class="c-list-content">
                                        <h3 class="c-list-title">{{ $post->name }}</h3>
                                        <p class="c-list-text">
                                            {!! $post->desctiption ?: str_limit(strip_tags($post->content), 200) !!}
                                        </p>
                                    </div>
                                    <div class="c-list-thumbnail">
                                        <div class=" image-cover image-cover--100h">
                                            <img src="{{ $post->image }}" alt="{{ $post->name }}" class="img">
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>

                {!! $posts->withQueryString()->onEachSide(1)->render('pagination.ttp') !!}
            </div>
        </div>
    </div>

    @include('blocks.template6')
@stop
