<?php
return [
    'panel_title' => 'Theme Setting',

    'footer_copyright' => 'Footer Copyright',

    'contact_email'     => 'Contact Email',
    'contact_phone'     => 'Contact Phone',
    'contact_address'   => 'Contact Address',
    'contact_map_title' => 'Map Title',
    'contact_map_lat'   => 'Map Lat',
    'contact_map_lng'   => 'Map Lng',

    'doi_tac_desc'   => 'Đối tác',
    'doi_tac_images' => 'Logo đối tác',

    'contact_care_project_admin_emails' => 'Admin Email',

    'document_download' => 'Tài liệu tải về',

    'company_map_iframe'      => 'Bản đồ công ty',
    'company_map_iframe_help' => '<a href="#">Xem hướng dẫn</a>',
];
