// formSubcribe
(function () {
    $('#formSubcribe').on('submit', function (e) {
        e.preventDefault();

        const form = $(this);
        let formError = $('#formSubcribeError');
        let formSuccess = $('#formSubcribeSuccess');

        formError.hide();
        formSuccess.hide();

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function (res) {
                formError.hide();
                formSuccess.show().html(res.message);
            },
            error: function (err) {
                let errMessages = [];
                Object.values(err.responseJSON.errors).forEach(val => {
                    errMessages = errMessages.concat(val);
                });

                formSuccess.hide();
                formError.show().html(errMessages.join('<br/>'));
            }
        })
    });
})();
