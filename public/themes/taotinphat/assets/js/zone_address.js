function CustomerAddress() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#province_id').change(function () {
        $.ajax({
            url: '/zone/province/' + $(this).val() + '/districts',
            success: function (response) {
                $('#district_id').html(response.districts);
                $.ajax({
                    url: '/zone/district/' + response.firstItemId + '/townships',
                    success: function (response) {
                        $('#township_id').html(response.townships)
                    }
                });
            }
        });

    });

    $('#district_id').click(function () {
        $.ajax({
            url: '/zone/district/' + $(this).val() + '/townships',
            success: function (response) {
                $('#township_id').html(response.townships)
            }
        });
    });

    if (location.href.indexOf('edit') > 0) {
        $.ajax({
            url: '/zone/province/' + $('#province_id').val() + '/districts',
            data: {
                district_id: $('#zone_district_id').val()
            },
            success: function (response) {
                $('#district_id').html(response.districts);
                $.ajax({
                    url: '/zone/district/' + $('#district_id').val() + '/townships',
                    data: {
                        township_id: $('#zone_township_id').val()
                    },
                    success: function (response) {
                        $('#township_id').html(response.townships)
                    }
                });
            }
        });
    }
}

$(document).ready(function () {
    new CustomerAddress();
});
