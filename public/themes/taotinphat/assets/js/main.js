! function(t) { "use strict"; "function" == typeof define && define.amd ? define(["jquery"], t) : "undefined" != typeof module && module.exports ? module.exports = t(require("jquery")) : t(jQuery) }(function(t) {
    var e = -1,
        o = -1,
        i = function(t) { return parseFloat(t) || 0 },
        n = function(e) {
            var o = 1,
                n = t(e),
                a = null,
                r = [];
            return n.each(function() {
                var e = t(this),
                    n = e.offset().top - i(e.css("margin-top")),
                    s = r.length > 0 ? r[r.length - 1] : null;
                null === s ? r.push(e) : Math.floor(Math.abs(a - n)) <= o ? r[r.length - 1] = s.add(e) : r.push(e), a = n
            }), r
        },
        a = function(e) {
            var o = {
                byRow: !0,
                property: "height",
                target: null,
                remove: !1
            };
            return "object" == typeof e ? t.extend(o, e) : ("boolean" == typeof e ? o.byRow = e : "remove" === e && (o.remove = !0), o)
        },
        r = t.fn.matchHeight = function(e) { var o = a(e); if (o.remove) { var i = this; return this.css(o.property, ""), t.each(r._groups, function(t, e) { e.elements = e.elements.not(i) }), this } return this.length <= 1 && !o.target ? this : (r._groups.push({ elements: this, options: o }), r._apply(this, o), this) };
    r.version = "master", r._groups = [], r._throttle = 80, r._maintainScroll = !1, r._beforeUpdate = null,
        r._afterUpdate = null, r._rows = n, r._parse = i, r._parseOptions = a, r._apply = function(e, o) {
            var s = a(o),
                h = t(e),
                c = [h],
                l = t(window).scrollTop(),
                p = t("html").outerHeight(!0),
                d = h.parents().filter(":hidden");
            return d.each(function() {
                var e = t(this);
                e.data("style-cache", e.attr("style"))
            }), d.css("display", "block"), s.byRow && !s.target && (h.each(function() {
                var e = t(this),
                    o = e.css("display");
                "inline-block" !== o && "flex" !== o && "inline-flex" !== o && (o = "block"), e.data("style-cache", e.attr("style")), e.css({
                    display: o,
                    "padding-top": "0",
                    "padding-bottom": "0",
                    "margin-top": "0",
                    "margin-bottom": "0",
                    "border-top-width": "0",
                    "border-bottom-width": "0",
                    height: "100px",
                    overflow: "hidden"
                })
            }), c = n(h), h.each(function() {
                var e = t(this);
                e.attr("style", e.data("style-cache") || "")
            })), t.each(c, function(e, o) {
                var n = t(o),
                    a = 0;
                if (s.target) a = s.target.outerHeight(!1);
                else {
                    if (s.byRow && n.length <= 1) return void n.css(s.property, "");
                    n.each(function() {
                        var e = t(this),
                            o = e.css("display");
                        "inline-block" !== o && "flex" !== o && "inline-flex" !== o && (o = "block");
                        var i = { display: o };
                        i[s.property] = "",
                            e.css(i), e.outerHeight(!1) > a && (a = e.outerHeight(!1)), e.css("display", "")
                    })
                }
                n.each(function() {
                    var e = t(this),
                        o = 0;
                    s.target && e.is(s.target) || ("border-box" !== e.css("box-sizing") && (o += i(e.css("border-top-width")) + i(e.css("border-bottom-width")), o += i(e.css("padding-top")) + i(e.css("padding-bottom"))), e.css(s.property, a - o + "px"))
                })
            }), d.each(function() {
                var e = t(this);
                e.attr("style", e.data("style-cache") || null)
            }), r._maintainScroll && t(window).scrollTop(l / p * t("html").outerHeight(!0)), this
        }, r._applyDataApi = function() {
            var e = {};
            t("[data-match-height], [data-mh]").each(function() {
                var o = t(this),
                    i = o.attr("data-mh") || o.attr("data-match-height");
                i in e ? e[i] = e[i].add(o) : e[i] = o
            }), t.each(e, function() { this.matchHeight(!0) })
        };
    var s = function(e) { r._beforeUpdate && r._beforeUpdate(e, r._groups), t.each(r._groups, function() { r._apply(this.elements, this.options) }), r._afterUpdate && r._afterUpdate(e, r._groups) };
    r._update = function(i, n) {
        if (n && "resize" === n.type) {
            var a = t(window).width();
            if (a === e) return;
            e = a
        }
        i ? -1 === o && (o = setTimeout(function() {
            s(n), o = -1
        }, r._throttle)) : s(n)
    }, t(r._applyDataApi), t(window).bind("load", function(t) { r._update(!1, t) }), t(window).bind("resize orientationchange", function(t) { r._update(!0, t) })
});

let OBJ = {};
(function($) {
    /************************************************************
     * Predefined letiables
     *************************************************************/
    let $window = $(window),
        $document = $(document),
        $html = $('html'),
        $body = $('body');
    /**
     * exists
     * @return true
     */
    $.fn.exists = function() {
        return this.length > 0;
    };
    /**
     * isMobile - Check mobile screen
     * @return void
     */
    $.fn.isMobile = function() {
        let screen = $window.outerWidth();
        return !!(screen < 751);
    };
    /**
     * @return void
     */
    let ismobile;
    OBJ.uaSetting = function() {
            let _ua = (function(u) {
                return {
                    Tablet: (u.indexOf('windows') !== -1 && u.indexOf('touch') !== -1 && u.indexOf('tablet pc') === -1) || u.indexOf('ipad') !== -1 || (u.indexOf('android') !== -1 && u.indexOf('mobile') === -1) || (u.indexOf('firefox') !== -1 && u.indexOf('tablet') !== -1) || u.indexOf('kindle') !== -1 || u.indexOf('silk') !== -1 || u.indexOf('playbook') !== -1,
                    Mobile: (u.indexOf('windows') !== -1 && u.indexOf('phone') !== -1) || u.indexOf('iphone') !== -1 || u.indexOf('ipod') !== -1 || (u.indexOf('android') !== -1 && u.indexOf('mobile') !== -1) || (u.indexOf('firefox') !== -1 && u.indexOf('mobile') !== -1) || u.indexOf('blackberry') !== -1,
                }
            })(window.navigator.userAgent.toLowerCase());
            if (_ua.Tablet || _ua.Mobile) {
                $body.addClass('sp');
                ismobile = true;
            } else {
                ismobile = false;
            }
        }
        /**
         *  open or close menu in mobile
         */
        /**
         *  custom for browser
         */
    OBJ.slideDetail = function() {
        if ($('.slide-detail')) {
            $('.slide-detail').slick();
        }
        if ($('.slider-project')) {
            $('.slider-project').slick({
                infinite: true,
                slidesToShow: 2,
                arrows: false,
            });
        }
        if ($('.slide-s')) {
            $('.slide-s').slick({
                infinite: true,
                slidesToShow: 5,
                arrows: false,
                centerMode: true,
            });
        }
    }
    OBJ.fixBrowser = function() {
        var ua = navigator.userAgent.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i),
            browser;
        if (navigator.userAgent.match(/Edge/i) || navigator.userAgent.match(/Trident.*rv[ :]*11\./i)) {
            browser = "msie";
            $('html').addClass('msie');
        } else {
            browser = ua[1].toLowerCase();
        }
        $html.addClass(browser);
    }
    OBJ.showPhoneNumber = function() {
        $('.show-phone-detail').click(function(e){
            let phone = $(this).parents('.contact-phome-number').find('[data-phone]').attr('data-phone');
            $(this).parents('.contact-phome-number').find('[data-phone]').text(phone);
            //$(this).hide();
        })
    }
    OBJ.sliderBanner = function() {
        if ($('.slider01').length) {
            let slider01 = new Swiper('.slider01', {
                pagination: {
                    el: '.swiper-pagination',
                },
                // autoplay: {
                //     delay: 5000,
                // },
                resizeReInit: true,
                noSwiping: true,
                noSwipingClass: 'slick-carousel',
                effect: 'fade',
                loop: true,
                navigation: {
                    nextEl: '.slider01 .swiper-button-next',
                    prevEl: '.slider01 .swiper-button-prev',
                },

            });

        }
        if ($('.slider03').length) {
            let galleryThumbs = new Swiper('.gallery-thumbs', {
                spaceBetween: 20,
            //    slidesPerView: 4,
                loop: false,
                slidesPerView: 'auto',
                autoResize: false,
                navigation: {
                    nextEl: '.gallery-thumbs-wrap .swiper-button-next',
                    prevEl: '.gallery-thumbs-wrap .swiper-button-prev',
                },
            });
            let galleryTop = new Swiper('.gallery-top', {
                loop: true,
                slidesPerView: 2,
                spaceBetween: 5,
                navigation: {
                    nextEl: '.gallery-top .swiper-button-next',
                    prevEl: '.gallery-top .swiper-button-prev',
                },
                thumbs: {
                    swiper: galleryThumbs
                },
            });
        }
        if ($('.slider02').length) {
            let slider02 = new Swiper('.slider02', {
                slidesPerView: 3,
                spaceBetween: 30,
                loop: true,
                watchSlidesProgress: true,
                watchSlidesVisibility: true,
                navigation: {
                    nextEl: '.swiper-button-next-02',
                    prevEl: '.swiper-button-prev-02',
                }
            });
        }
        if ($('.slider04').length) {
            let slider04 = new Swiper('.slider04', {
                slidesPerView: 5,
                spaceBetween: 0,
                loop: true,
                autoplay: {
                    delay: 3000,
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 3
                    },
                    // when window width is >= 640px
                    767: {
                        slidesPerView: 5
                    }
                }
            });
        }
        if ($('.slider-section-active').length) {
            let galleryThumbs = new Swiper('.slider-section-active', {
                spaceBetween: 20,
                slidesPerView: 3,
                loop: true,
                resizeReInit: true,
                autoplay: {
                    delay: 5000,
                },
            });
        }
        if ($('.slider-about').length) {
            $('.slider-about').slick({
                centerMode: true,
                centerPadding: 0,
                slidesToShow: 3,
                dots: false,
                infinite: true,               
                arrows: true,
                focusOnSelect: true,
                initialSlide: 1,
            });
            let activeOpenModal = function() {
                $('.slider-about').find('[data-toggle="modal"]').removeAttr("data-toggle");
                $('.slider-about').find('.slick-current a').attr("data-toggle","modal");
            }
            $('.slider-about').on('afterChange', function(event, slick, currentSlide, nextSlide){
                activeOpenModal();
            });
            activeOpenModal();
        }

        if($('.slider-05'.length)) {
             $('.slider05').slick({
                slidesToShow: 3,
                adaptiveHeight: true,
                responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                      }
                    },
                    {
                      breakpoint: 767,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ]
            });
        }
        
    }

    OBJ.scrollAnimation = function() {
        let scrollOff = $('.animation'),
            headerTopOff = $('.header_top').innerHeight(),
            windowsTop = $(window).scrollTop();
        if (scrollOff.length > 0) {
            scrollOff.each(function() {
                var scrollOffTop = $(this).offset().top,
                    wh = $(window).height();
                if ($(this)) {
                    $(this).addClass('animation--off');
                    (windowsTop + wh > scrollOffTop && $(this).hasClass('animation--off')) ? $(this).removeClass('animation--off').addClass('animation--on'): $(this).removeClass('animation--on').addClass('animation--off');
                }
            });
        }
        if (windowsTop > headerTopOff) {
            $body.addClass('activeHeader');
        } else {
            $body.removeClass('activeHeader');
        }

    }
    OBJ.backToTop = function() {
        $("#back-top").hide();
        $(window).scroll(function() {
            if ($(this).scrollTop() > 300) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top').click(function() {
            $('body,html').animate({ scrollTop: 0 }, 800);
            return false;
        });
    }
    OBJ.setHeight = function() {
        // $('.card-02 .card-title').matchHeight();
        $('.card-03 .card-title').matchHeight();
        $('.title-group h3').matchHeight();
        $('.slider05 .card-02').matchHeight();
        $('.slider02 .card-05').matchHeight();
    }
    OBJ.customFileUpload = function() {
        $('input[type=file]').change(function(e) {
            let name = e.target.files[0].name;
            console.log('name');

            $('.upload-btn label').text(name);
        });
    }
    OBJ.openMenuSub = function() {
        $('.ic-menu-sub').click(function(e) {
            e.stopPropagation();
            $(this).toggleClass('active');
            $(this).next('.sub-menu').slideToggle('fast');
        });
    }
    OBJ.formSearch = function() {        
        if($('.form--search')) {
           $document.on('click', '.js-active-search', function(e) {
                e.stopPropagation();
                $body.toggleClass('active-search');
            })
            $document.on('click', '.form--search', function(e) {
                e.stopPropagation();
            })
            $document.on('click', function(e) {
                if ($body.hasClass('active-search')) {
                    $body.removeClass('active-search');
                }
            }) 
        }
        if($('.sub-mit')) {
            $document.on('click', '.sub-mit', function(e) {
                e.preventDefault();
                let valInput = $(this).parents('.form--search').find('input').val();
                if(valInput.length > 0) {
                    $(this).parents('.form--search').submit();
                }else {
                    $(this).parents('.form--search').find('input').toggleClass('active');
                }
            })
        }
    }

    $window.scroll(function() {
        OBJ.uaSetting();
        OBJ.scrollAnimation();
    });
    $document.ready(function() {
        OBJ.openMenuSub();
        OBJ.fixBrowser();
        OBJ.sliderBanner();
        // OBJ.setHeight();
        OBJ.scrollAnimation();
        OBJ.slideDetail();
        OBJ.formSearch();
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var iframe = $(e.relatedTarget.hash).find('iframe'); 
            var src = iframe.attr('src');
            iframe.attr('src', '');
            iframe.attr('src', src);
        });
    })
    $window.on('load', function() {
        OBJ.setHeight();
        OBJ.backToTop();
        OBJ.customFileUpload();
        OBJ.showPhoneNumber();
    })
})(jQuery);