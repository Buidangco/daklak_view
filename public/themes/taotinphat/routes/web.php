<?php

use Themes\Taotinphat\Http\Controllers\Web\ContactController;
use Themes\Taotinphat\Http\Controllers\Web\SearchController;
use Themes\Taotinphat\Http\Controllers\Web\ZoneController;

Route::middleware(['web'])->group(function () {
    Route::post('care-project', [ContactController::class, 'careProject'])->name('theme.web.contact.care-project');
    Route::post('subcribe', [ContactController::class, 'subcribe'])->name('theme.web.contact.subcribe');
    Route::post('contact', [ContactController::class, 'contact'])->name('theme.web.contact.contact');

    Route::get('tim-kiem', [SearchController::class, 'index'])->name('theme.web.search.index');

    Route::get('zone/province/{id}/districts', [ZoneController::class, 'districts'])->name('theme.web.zone.districts');
    Route::get('zone/district/{id}/townships', [ZoneController::class, 'townships'])->name('theme.web.zone.townships');
});
