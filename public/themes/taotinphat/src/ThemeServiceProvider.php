<?php

namespace Themes\Taotinphat;

use Illuminate\Support\ServiceProvider;
use Themes\Taotinphat\SettingBuilders\ThemeSettingBuilder;

class ThemeServiceProvider extends ServiceProvider
{
    public function register()
    {
        parent::register();

        require_once __DIR__.'/helpers.php';
    }

    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'theme');

        \SettingBuilder::themeGroup()->add(ThemeSettingBuilder::class);

        \PageLayout::add('tin_mua_ban', 'Tin mua bán');

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        \Asset::addVersioning();
    }
}
