<?php

use Modules\Cms\Models\Category;
use Modules\Cms\Models\Post;
use Modules\PostSale\Models\RealEstate;
use Modules\Project\Models\Project;
use Modules\Recruitment\Models\Job;

if (!function_exists('__theme_get_list_categories')) {
    function __theme_get_list_categories()
    {
        return Category::where('is_active', 1)->get();
    }
}

if (!function_exists('__theme_get_last_posts')) {
    function __theme_get_last_posts($limit = 10, $ignore = null)
    {
        $ignoreIds = [];
        if (is_array($ignore)) {
            $ignoreIds = $ignore;
        } elseif ($ignore) {
            $ignoreIds = $ignore->pluck('id')->toArray();
        }

        return Post::where('is_active', 1)
            ->whereNotIn('id', $ignoreIds)
            ->orderBy('published_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate($limit);
    }
}

if (!function_exists('__theme_get_last_post_sticky')) {
    function __theme_get_last_post_sticky($limit = 10)
    {
        return Post::where('is_active', 1)
            ->where('is_sticky', 1)
            ->orderBy('published_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->take($limit)
            ->get();
    }
}

if (!function_exists('__theme_get_last_post_of_category')) {
    /**
     * Get post of category
     *
     * @param int|array $categoryId
     * @param  int  $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    function __theme_get_last_post_of_category($categoryId, $limit = 10)
    {
        return Post::where('is_active', 1)
            ->whereHas('categories', function ($q) use ($categoryId) {
                $categoryIds = (array) $categoryId;
                $q->whereIn('category_id', $categoryIds);
            })
            ->orderBy('published_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate($limit);
    }
}

if (!function_exists('__theme_get_list_pending_projects')) {
    function __theme_get_list_pending_projects($limit = 10)
    {
        return Project::where('is_active', 1)
            ->hasAttribute('da_hoan_thanh', 0)
            ->orderByDesc('published_at')
            ->orderByDesc('id')
            ->take($limit)
            ->get();
    }
}

if (!function_exists('__theme_get_list_jobs')) {
    function __theme_get_list_jobs($itemPerPage = 10)
    {
        return Job::where('is_active', 1)
            ->paginate($itemPerPage);
    }
}

if (!function_exists('__theme_get_related_post')) {
    function __theme_get_related_post(Post $post, $limit = 10)
    {
        return Post::where('id', '!=', $post->id)
            ->whereHas('categories', function ($q) use ($post) {
                $q->whereIn('category_id', $post->categories->pluck('id')->toArray());
            })
            ->where('is_active', 1)
            ->take($limit)
            ->get();
    }
}

if (!function_exists('__theme_get_sticky_real_estates')) {
    function __theme_get_sticky_real_estates($limit = 10)
    {
        return RealEstate::where('is_active', 1)
            ->where('is_sticky', 1)
            ->orderByDesc('published_at')
            ->orderByDesc('id')
            ->take($limit)
            ->get();
    }
}

if (!function_exists('__theme_get_active_real_estates')) {
    function __theme_get_active_real_estates($limit = 10)
    {
        return RealEstate::where('is_active', 1)
            ->orderByDesc('published_at')
            ->orderByDesc('id')
            ->paginate($limit);
    }
}

if (!function_exists('__theme_get_related_real_estates')) {
    function __theme_get_related_real_estates(RealEstate $realEstate, $limit = 10)
    {
        return RealEstate::where('id', '!=', $realEstate->id)
            ->whereHas('categories', function ($q) use ($realEstate) {
                $q->whereIn('category_id', $realEstate->categories->pluck('id')->toArray());
            })
            ->where('is_active', 1)
            ->orderByDesc('published_at')
            ->orderByDesc('id')
            ->take($limit)
            ->get();
    }
}

if (!function_exists('__theme_get_youtube_embed')) {
    function __theme_get_youtube_embed($youtubeUrl)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

        if (preg_match($longUrlRegex, $youtubeUrl, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        } elseif (preg_match($shortUrlRegex, $youtubeUrl, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        } else {
            $youtube_id = null;
        }

        return 'https://www.youtube.com/embed/' . $youtube_id;
    }
}

if (!function_exists('str_words')) {
    /**
     * Limit the number of words in a string.
     *
     * @param  string  $value
     * @param  int  $words
     * @param  string  $end
     * @return string
     */
    function str_words($value, $words = 100, $end = '...')
    {
        return \Illuminate\Support\Str::words($value, $words, $end);
    }
}

if (!function_exists('__theme_get_src_from_iframe')) {
    function __theme_get_src_from_iframe($iframe_string)
    {
        preg_match('/src="([^"]+)"/', $iframe_string, $match);

        return $match[1] ?? null;
    }
}

if (!function_exists('number_format_short')) {
    /**
     * Converts a number into a short version, eg: 1000 -> 1k
     *
     * @link https://gist.github.com/RadGH/84edff0cc81e6326029c
     *
     * @param $n
     * @param  int  $precision
     * @return string
     */
    function number_format_short($n, $precision = 1)
    {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else {
            if ($n < 900000) {
                // 0.9k-850k
                $n_format = number_format($n / 1000, $precision);
                $suffix = 'nghìn';
            } else {
                if ($n < 900000000) {
                    // 0.9m-850m
                    $n_format = number_format($n / 1000000, $precision);
                    $suffix = 'triệu';
                } else {
                    if ($n < 900000000000) {
                        // 0.9b-850b
                        $n_format = number_format($n / 1000000000, $precision);
                        $suffix = 'tỷ';
                    } else {
                        // 0.9t+
                        $n_format = number_format($n / 1000000000000, $precision);
                        $suffix = 'nghìn tỷ';
                    }
                }
            }
        }
        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ($precision > 0) {
            $dotzero = '.'.str_repeat('0', $precision);
            $n_format = str_replace($dotzero, '', $n_format);
        }
        return $n_format.' '.$suffix;
    }
}
