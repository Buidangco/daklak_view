<?php

namespace Themes\Taotinphat\SettingBuilders;

use Newnet\Core\Services\AdminSetting\BaseSettingBuilder;

class ThemeSettingBuilder extends BaseSettingBuilder
{
    public function getTitle()
    {
        return __('theme::setting.panel_title');
    }

    public function save()
    {
        setting($this->request->only([
            'footer_copyright',
            'contact_email',
            'contact_phone',
            'contact_address',
            'contact_map_title',
            'contact_map_lat',
            'contact_map_lng',
            'doi_tac_desc',
            'doi_tac_images',
            'contact_care_project_admin_emails',
            'document_download',
            'company_map_iframe',
        ]));
    }

    public function render()
    {
        $item = new \stdClass();

        $item->footer_copyright = setting('footer_copyright');

        $item->contact_email = setting('contact_email');
        $item->contact_phone = setting('contact_phone');
        $item->contact_address = setting('contact_address');
        $item->contact_map_title = setting('contact_map_title');
        $item->contact_map_lat = setting('contact_map_lat');
        $item->contact_map_lng = setting('contact_map_lng');

        $item->doi_tac_desc = setting('doi_tac_desc');
        $item->doi_tac_images = setting('doi_tac_images');

        $item->contact_care_project_admin_emails = setting('contact_care_project_admin_emails');

        $item->document_download = setting('document_download');

        $item->company_map_iframe = setting('company_map_iframe');

        return view('admin.theme-setting', compact('item'));
    }
}
