<?php

namespace Themes\Taotinphat\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\CRM\Models\CrmCustomer;

class Subcribe extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var CrmCustomer
     */
    public $crmCustomer;

    /**
     * Create a new message instance.
     *
     * @param  CrmCustomer  $crmCustomer
     */
    public function __construct(CrmCustomer $crmCustomer)
    {
        $this->crmCustomer = $crmCustomer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('emails.subcribe', [
                'customer_email' => $this->crmCustomer->email,
                'detail_url'     => route('crm.admin.crm_customer.edit', $this->crmCustomer->id),
            ])
            ->subject("[TaoTinPhat] Nhận thông tin dự án");
    }
}
