<?php

namespace Themes\Taotinphat\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\CRM\Models\CrmCustomer;
use Modules\Project\Models\Project;

class CareProject extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Project
     */
    public $project;

    /**
     * @var CrmCustomer
     */
    public $crmCustomer;

    /**
     * @var string
     */
    public $content;

    /**
     * Create a new message instance.
     *
     * @param  Project  $project
     * @param  CrmCustomer  $crmCustomer
     */
    public function __construct(Project $project, CrmCustomer $crmCustomer, $content)
    {
        $this->project = $project;
        $this->crmCustomer = $crmCustomer;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('emails.project.care', [
                'project_name'     => $this->project->name,
                'project_url'      => $this->project->url,
                'customer_name'    => $this->crmCustomer->last_name.' '.$this->crmCustomer->first_name,
                'customer_phone'   => $this->crmCustomer->phone,
                'customer_email'   => $this->crmCustomer->email,
                'customer_address' => object_get($this, 'crmCustomer.crm_address'),
                'content'          => $this->content,
                'detail_url'       => route('crm.admin.crm_customer.edit', $this->crmCustomer->id),
            ])
            ->subject("[TaoTinPhat] Nhận thông tin dự án - {$this->project->name}");
    }
}
