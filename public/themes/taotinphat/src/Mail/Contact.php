<?php

namespace Themes\Taotinphat\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\CRM\Models\CrmCustomer;

class Contact extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var CrmCustomer
     */
    public $crmCustomer;

    /**
     * @var string
     */
    public $content;

    /**
     * Create a new message instance.
     *
     * @param  CrmCustomer  $crmCustomer
     * @param $content
     */
    public function __construct(CrmCustomer $crmCustomer, $content)
    {
        $this->crmCustomer = $crmCustomer;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('emails.contact', [
                'customer_email' => $this->crmCustomer->email,
                'customer_name'  => $this->crmCustomer->last_name.' '.$this->crmCustomer->first_name,
                'customer_phone' => $this->crmCustomer->phone,
                'content'        => $this->content,
                'detail_url'     => route('crm.admin.crm_customer.edit', $this->crmCustomer->id),
            ])
            ->subject("[TaoTinPhat] Yêu cầu liên hệ");
    }
}
