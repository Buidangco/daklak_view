<?php

namespace Themes\Taotinphat\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CareProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name'  => 'required',
            'customer_phone' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'customer_name'  => 'Họ và tên',
            'customer_phone' => 'Số điện thoại',
        ];
    }
}
