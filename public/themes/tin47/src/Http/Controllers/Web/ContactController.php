<?php

namespace Themes\Taotinphat\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Mail;
use Modules\CRM\Models\CrmCustomer;
use Modules\CRM\Models\CrmNote;
use Modules\Project\Models\Project;
use Themes\Taotinphat\Http\Requests\CareProjectRequest;
use Themes\Taotinphat\Http\Requests\ContactRequest;
use Themes\Taotinphat\Http\Requests\SubcribeRequest;
use Themes\Taotinphat\Mail\CareProject;
use Themes\Taotinphat\Mail\Contact;
use Themes\Taotinphat\Mail\Subcribe;

class ContactController extends Controller
{
    public function careProject(CareProjectRequest $request)
    {
        $crmCustomer = $this->createCrmCustomer($request);

        $project = Project::find($request->input('project_id'));

        CrmNote::create([
            'crm_customer_id' => $crmCustomer->id,
            'content'         => "Dự án: {$project->name}",
        ]);

        if ($content = $request->input('content')) {
            CrmNote::create([
                'crm_customer_id' => $crmCustomer->id,
                'content'         => "Nội dung yêu cầu: ".$request->input('content'),
            ]);
        }

        $users = $this->getEmailOfAdmin();

        Mail::to($users)->queue(new CareProject($project, $crmCustomer, $content));

        return back()->with(['success' => 'Cảm ơn bạn đã gửi yêu cầu!']);
    }

    public function subcribe(SubcribeRequest $request)
    {
        $crmCustomer = $this->createCrmCustomer($request);

        CrmNote::create([
            'crm_customer_id' => $crmCustomer->id,
            'content'         => "Đăng ký nhận tin dự án",
        ]);

        $users = $this->getEmailOfAdmin();

        Mail::to($users)->queue(new Subcribe($crmCustomer));

        if ($request->expectsJson()) {
            return [
                'success' => true,
                'message' => 'Cảm ơn bạn đã đăng ký!',
            ];
        }

        return back()->with(['success' => 'Cảm ơn bạn đã đăng ký!']);
    }

    public function contact(ContactRequest $request)
    {
        $crmCustomer = $this->createCrmCustomer($request);
        $content = $request->input('content');

        CrmNote::create([
            'crm_customer_id' => $crmCustomer->id,
            'content'         => "Gửi liên hệ",
        ]);

        $users = $this->getEmailOfAdmin();

        Mail::to($users)->queue(new Contact($crmCustomer, $content));

        if ($request->expectsJson()) {
            return [
                'success' => true,
                'message' => 'Cảm ơn bạn đã gửi yêu cầu!',
            ];
        }

        return back()->with(['success' => 'Cảm ơn bạn đã gửi yêu cầu!']);
    }

    protected function createCrmCustomer(Request $request)
    {
        $email = $request->input('customer_email');
        $phone = $request->input('customer_phone');
        $address = $request->input('customer_address');
        $fullName = explode(' ', $request->input('customer_name'));

        $firstName = last($fullName);
        array_pop($fullName);
        $lastName = implode(' ', $fullName);

        return CrmCustomer::create([
            'first_name'  => $firstName,
            'last_name'   => $lastName,
            'phone'       => $phone,
            'email'       => $email,
            'crm_address' => $address,
        ]);
    }

    /**
     * @return string[]
     */
    protected function getEmailOfAdmin()
    {
        return explode(',', setting('contact_care_project_admin_emails') ?: 'kimtrien@gmail.com');
    }
}
