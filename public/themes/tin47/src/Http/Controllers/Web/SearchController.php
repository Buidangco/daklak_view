<?php

namespace Themes\Taotinphat\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cms\Models\Post;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $s = $request->input('s');

        $posts = Post::query()
            ->where('is_active', 1)
            ->where(function ($q) use ($s) {
                $keywords = explode(' ', $s);
                foreach ($keywords as $keyword) {
                    if ($keyword = trim($keyword)) {
                        $q->where('name', 'like', "%{$keyword}%");
                    }
                }
            })
            ->orderByDesc('published_at')
            ->orderByDesc('id')
            ->paginate(20);

        return view('search')->with([
            'posts' => $posts,
        ]);
    }
}
