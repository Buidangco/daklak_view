<?php

namespace Themes\Taotinphat\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Cms\Models\Post;
use Modules\ZoneModule\Repositories\Eloquent\ZoneDistrictRepositoryInterface;
use Modules\ZoneModule\Repositories\Eloquent\ZoneProvinceRepositoryInterface;

class ZoneController extends Controller
{
    /**
     * @var ZoneProvinceRepositoryInterface
     */
    private $zoneProvinceRepository;

    /**
     * @var ZoneDistrictRepositoryInterface
     */
    private $zoneDistrictRepository;

    public function __construct(ZoneProvinceRepositoryInterface $zoneProvinceRepository, ZoneDistrictRepositoryInterface $zoneDistrictRepository)
    {
        $this->zoneProvinceRepository = $zoneProvinceRepository;
        $this->zoneDistrictRepository = $zoneDistrictRepository;
    }

    public function districts($id, Request $request)
    {
        $item = $this->zoneProvinceRepository->getById($id);
        $districts = $item->districts()->orderBy('sort_order')->get();
        $firstItemId = count($districts) > 0 ? $districts[0]->id : '';

        $district_id = $request->input('district_id');
        $isTownship = false;
        $renderHtml = view('zone::admin.provinces.zone-ajax', compact('districts', 'isTownship', 'district_id'))->render();

        return response()->json(['districts' => $renderHtml, 'firstItemId' => $firstItemId]);
    }

    public function townships($id, Request $request)
    {
        $item = $this->zoneDistrictRepository->getById($id);
        $townships = $item->townships()->orderBy('sort_order')->get();
        $firstItemId = count($townships) > 0 ? $townships[0]->id : '';

        $township_id = $request->input('township_id');
        $isTownship = true;
        $renderHtml = view('zone::admin.provinces.zone-ajax', compact('townships', 'isTownship', 'township_id'))->render();

        return response()->json(['townships' => $renderHtml, 'firstItemId' => $firstItemId]);
    }
}
