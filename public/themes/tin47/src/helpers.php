<?php
use Modules\Cms\Models\Category;
use Modules\Cms\Models\Post;
use Modules\Cms\Models\Post_Category;
use Carbon\Carbon;


            if(!function_exists('__sevice_sevice_cms__posts')){
        function __sevice_sevice_cms__posts()
        {
            return Post::all()->whereIn(
                'category_id',array(7,8,9,10,11,12,13));
        }
    }

      if(!function_exists('__sevice_sevice_cms__posts_all')){
        function __sevice_sevice_cms__posts_all()
        {
            return Post::whereIn('category_id',array(7,8,9,10,11,12,13))->paginate(3);;
        }
}

         if(!function_exists('__sevice_sevice_cms__posts_all_1')){
        function __sevice_sevice_cms__posts_all_1()
        {
            return Post::whereIn('category_id',array(7,8,9,10,11,12,13))
            ->orderBy('published_at', 'DESC')
            ->limit(3)
            ->get();
        }
    }

     if(!function_exists('__sevice_sevice_cms__posts_view')){
        function __sevice_sevice_cms__posts_view()
        {
            return Post::whereIn('category_id',array(7,8,9,10,11,12,13))
            ->orderBy('published_at', 'DESC')
            ->where('published_at','>',Carbon::now()->toDateString())
            ->limit(1)
            ->get();
        }
    }

     if(!function_exists('__sevice_sevice_cms__posts_view_1')){
        function __sevice_sevice_cms__posts_view_1()
        {
            return Post::whereIn('category_id',array(7,8,9,10,11,12,13))
            ->orderBy('published_at', 'DESC')
            ->limit(5)
            ->get();
        }
    }

     if(!function_exists('__sevice_sevice_cms__Category1')){
        function __sevice_sevice_cms__Category1($id,$itemPerPage = 2)
        {
              return Post::whereIn('category_id',array(7,8,9,10,11,12,13))
              ->where('category_id',$id)
            ->orderBy('published_at', 'DESC')
            ->paginate($itemPerPage);
        }
    }

    if(!function_exists('__sevice_sevice_cms__Category_min')){
        function __sevice_sevice_cms__Category_min($id)
        {
              return Post::whereIn('category_id',array(7,8,9,10,11,12,13))
              ->where('category_id',$id)
              ->orderBy('published_at', 'DESC')
              ->limit(1)
              ->get();
        }
    }

    if(!function_exists('__sevice_sevice_cms__Category_post')){
        function __sevice_sevice_cms__Category_post($id)
        {
              return Post::whereIn('category_id',array(7,8,9,10,11,12,13))
              ->where('category_id',$id)
              ->orderBy('published_at', 'DESC')
              ->limit(7)
              ->get();
        }
    }

