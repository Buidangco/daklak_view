<?php

namespace Themes\Tin47;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use Themes\Tin47\SettingBuilders\ThemeSettingBuilder;

class ThemeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'theme');

        \SettingBuilder::themeGroup()->add(ThemeSettingBuilder::class);

        // \View::composer('cms::web.page.home', function (View $view) {
        //     $view->with('foo', $bar);
        // });
    }
    // public function register()
    // {
    //     parent::register();

    //     require_once _DIR_.'/helpers.php';
    // }
        public function register()
    {
        parent::register();

        require_once __DIR__.'/helpers.php';
    }
}
