
<section id="blog-slider-section" class="blog-slider-section w100dt mb-50">
            <div class="container">
{!! SliderRender::render('home1','sliders.home1') !!}
            </div>
            <!-- container -->
        </section>
        <!-- /#blog-slider-section -->
        <!-- ==================== blog-slider-section end ==================== -->
        <!-- ==================== daily-lifestyle-section Start ==================== -->
        <section id="daily-lifestyle-section" class="daily-lifestyle-section mb-50">
            <div class="container">
                <div class="owl-carousel small-carousel owl-theme">
                    @if($sms_post=__sevice_sevice_cms__posts())
        @foreach($sms_post as $row)
                    <div class="item">
                        <div class="card horizontal" style="height: 257px;">
                            <div class="card-image">
                                <img src="{{$row->Image}}" alt="Image" style="width: 140px;
    height: 140px;">
                                <span class="effect"></span>
                            </div>
                            <!-- /.card-image -->
                            <div class="card-stacked">
                                <div class="card-content">
                                    <a href="#" class="tag left l-blue mb-10">{{$row->category->name}}</a>
                                    <a href="single-blog.html" class="sm-name">{{$row->description}}</a>
                                </div>
                                <!-- /.card-content -->
                                <div class="card-action">
                                    <p class="hero left">
                                        BY - <a href="#" class="l-blue">SujonMaji</a>
                                    </p>
                                    <ul class="post-mate right">
                                        <li>
                                            <a href="#" class="m-0"><i class="icofont icofont-comment"></i> 32</a>
                                        </li>
                                    </ul>
                                    <!-- /.post-mate -->
                                </div>
                                <!-- /.card-action -->
                            </div>
                            <!-- /.card-stacked -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.item -->

          @endforeach
        @endif
                </div>
                <!-- /.small-carousel -->
            </div>
            <!-- container -->
        </section>
        <!-- /#daily-lifestyle-section -->
        <!-- ==================== daily-lifestyle-section End ==================== -->
        

      



        <!-- ==================== blog-section start ==================== -->
        <section id="blog-section" class="blog-section w100dt mb-50">
            <div class="container">
                <div class="row">

                    <div class="col s12 m8 l8">
                             @if($sms_post=__sevice_sevice_cms__posts_all())
        @foreach($sms_post as $row1)
                        <div class="blogs mb-30">
                            <div class="card">
                                <div class="card-image">
                                    <img src="{{$row1->Image}}" alt="Image">
                                    <a class="btn-floating center-align cmn-bgcolor halfway-fab waves-effect waves-light">
                                        <i class="icofont icofont-camera-alt"></i>
                                    </a>
                                </div>
                                <!-- /.card-image -->
                                <div class="card-content w100dt">
                                    <p>
                                        <a href="#" class="tag left w100dt l-blue mb-30">{{$row1->category->name}}</a>
                                    </p>
                                    <a href="single-blog.html" class="card-title">
                                        {{$row1->name}}
                                    </a>
                                    <p class="mb-30">
                                       {{$row1->description}}
                                    </p>
                                    <ul class="post-mate-time left">
                                        <li>
                                            <p class="hero left">
                                                By - <a href="#" class="l-blue">{{$row1->category->name}}</a>
                                            </p>
                                        </li>
                                        <li>
                                            <i class="icofont icofont-ui-calendar"></i> {{$row1->published_at}}
                                        </li>
                                    </ul>

                                    <ul class="post-mate right">
                                        <li class="like">
                                            <a href="#">
                                                <i class="icofont icofont-heart-alt"></i> 55
                                            </a>
                                        </li>
                                        <li class="comment">
                                            <a href="#">
                                                <i class="icofont icofont-comment"></i> 32
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.card-content -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.blogs -->
                        @endforeach
                        @endif
                        {{$sms_post->links()}}
                    </div>  
                    <!-- colm8 -->




                    <div class="col s12 m4 l4">

               <!--          <div class="sidebar-testimonial mb-30">
                            <div class="sidebar-title center-align">
                                <h2>Hi Its Me!</h2>
                            </div>
                          

                            <div class="carousel carousel-slider center" data-indicators="true">
                                <div class="carousel-item">
                                    <div class="item-img">
                                        <span>R</span>
                                    </div>
                                    <h2>Rakibul Hassan</h2>
                                    <small>Web Design & Development</small>
                                    <p>
                                        Sedut perspiciatis unde omnis iste natus errorsit voluptatem accusantium doloremque.
                                    </p>
                                </div>
                                <div class="carousel-item">
                                    <div class="item-img">
                                        <span>O</span>
                                    </div>
                                    <h2>Rakibul Hassan</h2>
                                    <small>Web Design & Development</small>
                                    <p>
                                        Sedut perspiciatis unde omnis iste natus errorsit voluptatem accusantium doloremque.
                                    </p>
                                </div>
                                <div class="carousel-item">
                                    <div class="item-img">
                                        <span>K</span>
                                    </div>
                                    <h2>Rakibul Hassan</h2>
                                    <small>Web Design & Development</small>
                                    <p>
                                        Sedut perspiciatis unde omnis iste natus errorsit voluptatem accusantium doloremque.
                                    </p>
                                </div>
                                <div class="carousel-item">
                                    <div class="item-img">
                                        <span>I</span>
                                    </div>
                                    <h2>Rakibul Hassan</h2>
                                    <small>Web Design & Development</small>
                                    <p>
                                        Sedut perspiciatis unde omnis iste natus errorsit voluptatem accusantium doloremque.
                                    </p>
                                </div>
                                <div class="carousel-item">
                                    <div class="item-img">
                                        <span>B</span>
                                    </div>
                                    <h2>Rakibul Hassan</h2>
                                    <small>Web Design & Development</small>
                                    <p>
                                        Sedut perspiciatis unde omnis iste natus errorsit voluptatem accusantium doloremque.
                                    </p>
                                </div>
                            </div>
                        </div> -->
                        <!-- /.sidebar-testimonial -->




              <!--           <div class="sidebar-followme w100dt mb-30">
                            <div class="sidebar-title center-align">
                                <h2>Follow Me</h2>
                            </div>
                           

                            <ul class="followme-links w100dt">
                                <li class="mrb15">
                                    <a href="#" class="facebook">
                                        <i class="icofont icofont-social-facebook"></i>
                                        <small class="Followers white-text">105 Likes</small>
                                    </a>
                                </li>
                                <li class="mrb15">
                                    <a href="#" class="twitter">
                                        <i class="icofont icofont-social-twitter"></i>
                                        <small class="Followers white-text">50 Follows</small>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="google-plus">
                                        <i class="icofont icofont-social-google-plus"></i>
                                        <small class="Followers white-text">39 Follows</small>
                                    </a>
                                </li>

                                <li class="mrb15">
                                    <a href="#" class="linkedin">
                                        <i class="icofont icofont-brand-linkedin"></i>
                                        <small class="Followers white-text">50 Follows</small>
                                    </a>
                                </li>
                                <li class="mrb15">
                                    <a href="#" class="pinterest">
                                        <i class="icofont icofont-social-pinterest"></i>
                                        <small class="Followers white-text">50 Follows</small>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="instagram">
                                        <i class="icofont icofont-social-instagram"></i>
                                        <small class="Followers white-text">39 Likes</small>
                                    </a>
                                </li>
                            </ul>

                        </div> -->
                        <!-- /.sidebar-followme -->




                        <div class="featured-posts w100dt mb-30">
                            <div class="sidebar-title center-align">
                                <h2>Các bài viết</h2>
                            </div>
                            <!-- /.sidebar-title -->

                            <div class="sidebar-posts">
                                @if($post_all=__sevice_sevice_cms__posts_all_1())
                                @foreach($post_all as $items)
                                <div class="card">
                                    <div class="card-image mb-10">
                                        <img src="{{$items->Image}}" alt="Image">
                                        <span class="effect"></span>
                                    </div>
                                    <!-- /.card-image -->
                                    <div class="card-content w100dt">
                                        <p>
                                            <a href="#" class="tag left w100dt l-blue mb-10">{{$items->category->name}}</a>
                                        </p>
                                        <a href="single-blog.html" class="card-title">
                                            {{$items->description}}
                                        </a>
                                        <ul class="post-mate-time left">
                                            <li>
                                                <p class="hero left">
                                                    By - <a href="#" class="l-blue">{{$items->category->name}}</a>
                                                </p>
                                            </li>
                                        </ul>

                                        <ul class="post-mate right">
                                            <li class="like">
                                                <a href="#">
                                                    <i class="icofont icofont-heart-alt"></i> 55
                                                </a>
                                            </li>
                                            <li class="comment">
                                                <a href="#">
                                                    <i class="icofont icofont-comment"></i> 32
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-content -->
                                </div>
                                <!-- /.card -->
                                @endforeach
                                @endif
                               
                                <!-- /.card -->
                            </div>
                            <!-- /.sidebar-posts -->

                        </div>
                        <!-- /.featured-posts -->




                        <div class="top-post w100dt mb-30">
                            <div class="sidebar-title center-align">
                                <h2>Bài Đăng</h2>
                            </div>
                            <!-- /.sidebar-title -->

                            <ul id="tabs-swipe-demo" class="top-post-tab tabs tabs-fixed-width">
                                <li class="tab"><a href="#test-swipe-1" class="active">Lượt xem nhiều</a></li>
                                <li class="tab"><a href="#test-swipe-2">Gần nhất</a></li>
                               <!--  <li class="tab"><a href="#test-swipe-3">Bình luận</a></li> -->
                            </ul>

                            <div id="test-swipe-1" class="tab-contant most-view">

                                <div class="sidebar-posts">
                                    @if($view_post=__sevice_sevice_cms__posts_view())
                                    @foreach($view_post as $view)
                                    <div class="card">
                                        <div class="card-image mb-10">
                                            <img src="{{$view->Image}}" alt="Image">
                                            <span class="effect"></span>
                                        </div>
                                        <!-- /.card-image -->
                                        <div class="card-content w100dt">
                                            <p>
                                                <a href="#" class="tag left w100dt l-blue mb-10">{{$items->category->name}}</a>
                                            </p>
                                            <a href="single-blog.html" class="card-title">
                                                 {{$items->description}}
                                            </a>
                                            <ul class="post-mate-time left">
                                                <li>
                                                    <p class="hero left">
                                                        By - <a href="#" class="l-blue">{{$items->category->name}}</a>
                                                    </p>
                                                </li>
                                            </ul>

                                            <ul class="post-mate right">
                                                <li class="like">
                                                    <a href="#">
                                                        <i class="icofont icofont-heart-alt"></i> 55
                                                    </a>
                                                </li>
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.card-content -->
                                    </div>
                                    @endforeach
                                    @endif
                                    <!-- /.card -->
                                     @if($view_post_1=__sevice_sevice_cms__posts_view_1())
                                    @foreach($view_post_1 as $view)
                                    <div class="hot-post w100dt mb-10">
                                        <div class="hot-post-image">
                                            <img src="{{$view->Image}}" alt="Image">
                                            <span class="effect"></span>
                                        </div>
                                        <!-- /.hot-post-image -->
                                        <div class="hot-post-stacked">
                                            <p>
                                                <a href="#" class="tag left w100dt l-blue mb-10">{{$view->category->name}}</a>
                                            </p>
                                            <a href="single-blog.html" class="sm-name mb-10"> {{$view->name}}</a>
                                            <div class="hot-post-action">
                                                <p class="hero left">
                                                    BY - <a href="#" class="l-blue">{{$view->category->name}}</a>
                                                </p>
                                                <ul class="post-mate right">
                                                    <li class="comment">
                                                        <a href="#">
                                                            <i class="icofont icofont-comment"></i> 32
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!-- /.post-mate -->
                                            </div>
                                            <!-- /.hot-post-action -->
                                        </div>
                                        <!-- /.hot-post-stacked -->
                                    </div>
                                    <!-- /.hot-post -->
                                    @endforeach
                                    @endif
                                    <!-- /.hot-post -->
                                </div>
                                <!-- /.sidebar-posts -->

                            </div>
                            <!-- /.most-view -->


                            <div id="test-swipe-2" class="tab-contant recent-post">
                                 @if($view_post_1=__sevice_sevice_cms__posts_view_1())
                                    @foreach($view_post_1 as $view1)
                                <div class="hot-post w100dt p-15">
                                    <div class="hot-post-image">
                                        <img src="{{$view1->Image}}" alt="Image">
                                        <span class="effect"></span>
                                    </div>
                                    <!-- /.hot-post-image -->
                                    <div class="hot-post-stacked">
                                        <p>
                                            <a href="#" class="tag left w100dt l-blue mb-10">{{$view1->category->name}}</a>
                                        </p>
                                        <a href="single-blog.html" class="sm-name mb-10">{{$view1->name}}</a>
                                        <div class="hot-post-action">
                                            <p class="hero left">
                                                BY - <a href="#" class="l-blue">{{$view1->category->name}}</a>
                                            </p>
                                            <ul class="post-mate right">
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul>
                                            <!-- /.post-mate -->
                                        </div>
                                        <!-- /.hot-post-action -->
                                    </div>
                                    <!-- /.hot-post-stacked -->
                                </div>
                                @endforeach
                                @endif

                            </div>
                            <!-- /.recent-post -->


                          <!--   <div id="test-swipe-3" class="tab-contant Comments-post"> -->

                          <!--       <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img1.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img2.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                               

                                <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img3.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                               

                                <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img1.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                              

                                <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img1.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                               

                                <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img1.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img2.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img3.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                              

                                <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img1.png')}}" alt="Image" class="circle responsive-img">
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div> -->
                                

                    <!--             <div class="card-panel grey lighten-5 z-depth-1">
                                    <div class="row valign-wrapper">
                                        <div class="col s3">
                                            <img src="{{ theme_url('img/img1.png')}}" alt="Image" class="circle responsive-img"> 
                                        </div>
                                        <div class="col s9">
                                            <span class="black-text">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                

                            </div> -->
                            <!-- /.tab-contant -->
                        </div>
                        <!-- /.top-post -->




                        <div class="sidebar-subscribe w100dt">
                            <div class="sidebar-title center-align">
                                <h2>Subscribe</h2>
                            </div>
                            <!-- /.sidebar-title -->

                            <div class="subscribe">
                                <p class="mb-30">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmo.
                                </p>
                                <form action="#">
                                    <div class="input-field">
                                        <input id="email" type="email" class="validate">
                                        <label class="left-align" for="email">Enter email address</label>
                                    </div>
                                    <a class="waves-effect waves-light">SUBMIT NOW</a>
                                </form>
                            </div>
                            <!-- /.subscribe -->

                        </div>
                        <!-- /.sidebar-subscribe -->
                        
                    </div>  
                    <!-- colm4 -->
                    
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </section>
        <!-- /#blog-section -->
        <!-- ==================== blog-section end ==================== -->





        <!-- ==================== instag leftram-section Start ==================== -->
        <section  id="instagram-section" class="instagram-section w100dt" style="background-image:url({{theme_url('img/bg-fotter.png')}}) !important">

            <div class="instagram-link w100dt" style="    width: 700px !important   ;">
                <a href="#">
                    <span>CƠ QUAN CHỦ QUẢN: CÔNG TY TNHH TRUYỀN THÔNG ĐẮK LẮK 24H</span><br>
                    <span>Địa chỉ: 73 Ybih Aleo - P.Tân Lợi - Tp. Buôn Ma Thuột - tỉnh Đắk Lắk</span><br>
                    <span>Điện thoại: (0262)3.959.333 - 3.959.334 - 3.959.332 - Hotline: 0942.06.6000</span><br>
                    <span>Giấy phép số: 02/GP-STTTT - Cấp ngày 27/05/2019 - Sở Thông tin và Truyền thông tỉnh Đắk Lắk</span>
                </a>
            </div>

        </section>