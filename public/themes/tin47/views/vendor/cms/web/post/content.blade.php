
   <section id="breadcrumb-section" class="breadcrumb-section w100dt mb-30">
            <div class="container">

                <nav class="breadcrumb-nav w100dt">
                    <div class="page-name hide-on-small-only left">
                        <h4>{{$post->category->name}}</h4>
                    </div>
                    <div class="nav-wrapper right">
                        <a href="index.html" class="breadcrumb">Home</a>
                        <a href="single-blog.html" class="breadcrumb active">{{$post->category->name}}</a>
                    </div>
                    <!-- /.nav-wrapper -->
                </nav>
                <!-- /.breadcrumb-nav -->

            </div>
            <!-- container -->
        </section>
    <section id="single-blog-section" class="single-blog-section w100dt mb-50">
            <div class="container">
                <div class="row">

                    <div class="col m8 s12">

                        <div class="blogs mb-30">
                          
                            <div class="card">
                               <!--  <div class="card-image">
                                    <img src="img/selfie.jpg" alt="Image">
                                </div> -->
                                <!-- /.card-image -->
                                <div class="card-content w100dt">
                                    <p>
                                        <a href="#" class="tag left w100dt l-blue mb-30">{{$post->category->name}}</a>
                                    </p>
                                    <a href="#" class="card-title mb-30">
                                        {{$post->name}}
                                    </a>
                                    <p class="mb-30">
                                        {{$post->description}}
                                    </p>
                                    <ul class="post-mate-time left mb-30">
                                        <li>
                                            <p class="hero left">
                                                By - <a href="#" class="l-blue">{{$post->category->name}}  </a>
                                            </p>
                                        </li>
                                        <li>
                                            <i class="icofont icofont-ui-calendar"></i>{{$post->published_at}}
                                        </li>
                                    </ul>

                                    <ul class="post-mate right mb-30">
                                        <li class="like">
                                            <a href="#">
                                                <i class="icofont icofont-heart-alt"></i> 55
                                            </a>
                                        </li>
                                        <li class="comment">
                                            <a href="#">
                                                <i class="icofont icofont-comment"></i> 32
                                            </a>
                                        </li>
                                    </ul>

                                    <p class="w100dt mb-10">
                                       {!! $post->content !!}
                                    </p>
                                   <!--  <p class="w100dt mb-50">
                                        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.
                                    </p>


                                    <img class="mb-30" src="{{$post->Image}}" alt="Image">
                                    <p class="w100dt">
                                        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.
                                    </p>
                                    <blockquote class="grey lighten-2">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde error cumque voluptatibus autem voluptatum corrupti cum facilis reprehenderit fugiat beatae.
                                    </blockquote>
                                    <p class="w100dt mb-50">
                                        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.
                                    </p>


                                    <img class="mb-10" src="img/travel2.jpg" alt="Image">
                                    <p class="w100dt mb-50">
                                        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.
                                    </p>

                                    <ul class="tag-list left">
                                        <li><a href="#!" class="waves-effect">#Fashion</a></li>
                                        <li><a href="#!" class="waves-effect">#Lifestyle</a></li>
                                        <li><a href="#!" class="waves-effect">#Travel</a></li>
                                    </ul> -->

                                    <!-- <ul class="share-links right">
                                        <li><a href="#" class="facebook"><i class="icofont icofont-social-facebook"></i></a></li>
                                        <li><a href="#" class="twitter"><i class="icofont icofont-social-twitter"></i></a></li>
                                        <li><a href="#" class="google-plus"><i class="icofont icofont-social-google-plus"></i></a></li>
                                        <li><a href="#" class="linkedin"><i class="icofont icofont-brand-linkedin"></i></a></li>
                                        <li><a href="#" class="pinterest"><i class="icofont icofont-social-pinterest"></i></a></li>
                                        <li><a href="#" class="instagram"><i class="icofont icofont-social-instagram"></i></a></li>
                                    </ul> -->

                                </div>
                                <!-- /.card-content -->
                            </div>
                         
                            <!-- /.card -->
                        </div>
                        <!-- /.blogs -->

   

                    </div>  
                    <!-- colm8 -->




                    <div class="col s12 m4 l4">

         
                        <!-- /.sidebar-testimonial -->
                        <!-- /.sidebar-followme -->




                        <div class="featured-posts w100dt mb-30">
                            <div class="sidebar-title center-align">
                                   @if($post2=__sevice_sevice_cms__Category_min($post->category->id))
                                @foreach($post2 as $row)
                                <h2>{{$row->name}}</h2>
                                @endforeach
                                 @endif
                            </div>
                            <!-- /.sidebar-title -->

                            <div class="sidebar-posts">
                                @if($post2=__sevice_sevice_cms__Category_min($post->category->id))
                                @foreach($post2 as $row)
                                <div class="card">
                                    <div class="card-image mb-10">
                                        <img src="{{$row->Image}}" alt="Image">
                                        <span class="effect"></span>
                                    </div>
                                    <!-- /.card-image -->
                                    <div class="card-content w100dt">
                                        <p>
                                            @foreach($row->categories as $categorys)
                                            <a href="{{$categorys->url}}" class="tag left w100dt l-blue mb-10">{{$categorys->name}}</a>
                                            @endforeach
                                        </p>
                                       
                                        <a href="{{$row->url}}" class="card-title">
                                            {{$row->description}}
                                        </a>
                                      
                                        <ul class="post-mate-time left">
                                            <li>
                                                <p class="hero left">
                                                    By - 
                                                      @foreach($row->categories as $categorys)
                                                    <a href="{{$categorys->url}}" class="l-blue">{{$categorys->name}}</a>
                                                     @endforeach
                                                </p>
                                            </li>
                                        </ul>

                                        <ul class="post-mate right">
                                            <li class="like">
                                                <a href="#">
                                                    <i class="icofont icofont-heart-alt"></i> 55
                                                </a>
                                            </li>
                                            <li class="comment">
                                                <a href="#">
                                                    <i class="icofont icofont-comment"></i> 32
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-content -->
                                </div>
                                <!-- /.card -->
                               
                                @endforeach
                                 @endif
                            </div>
                            <!-- /.sidebar-posts -->

                        </div>
                        <!-- /.featured-posts -->




                        <div class="top-post w100dt mb-30">
                            <!-- /.sidebar-title -->

                            <ul id="tabs-swipe-demo" class="top-post-tab tabs tabs-fixed-width">
                                <li class="tab"><a href="#test-swipe-1" class="active">Most Views</a></li>
                            <div class="indicator" style="right: 226.672px; left: 0px;"></div></ul>

                            <div id="test-swipe-1" class="tab-contant most-view active" style="    height: 43em !important;">

                                <div class="sidebar-posts">
                                    <div class="card">
                                        <!-- <div class="card-image mb-10">
                                            <img src="img/fashion.jpg" alt="Image">
                                            <span class="effect"></span>
                                        </div> -->
                                        <!-- /.card-image -->
                                        <div class="card-content w100dt">
                                            <ul style="list-style-type: disc !important;">
                                                  
                                                   @if($post2=__sevice_sevice_cms__Category_post($post->category->id))
                                                   @foreach($post2 as $row)
                                                <li style="list-style-type: disc !important;"><a href="{{$row->url}}" class="card-title">{{$row->description}}</a></li>
                                                @endforeach
                                                @endif
                                            </ul>
                                           <!--  <p>
                                                <a href="#" class="tag left w100dt l-blue mb-10">Fashion</a>
                                            </p> -->
                                            <!-- <ul class="post-mate-time left">
                                                <li>
                                                    <p class="hero left">
                                                        By - <a href="#" class="l-blue">SujonMaji</a>
                                                    </p>
                                                </li>
                                            </ul>

                                            <ul class="post-mate right">
                                                <li class="like">
                                                    <a href="#">
                                                        <i class="icofont icofont-heart-alt"></i> 55
                                                    </a>
                                                </li>
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul> -->
                                        </div>
                                        <!-- /.card-content -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.sidebar-posts -->

                            </div>
                            <!-- /.most-view -->
                        </div>
                        
                    </div>  
                    <!-- colm4 -->
                    
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </section>
  <section  id="instagram-section" class="instagram-section w100dt" style="background-image:url({{theme_url('img/bg-fotter.png')}}) !important">

            <div class="instagram-link w100dt" style="    width: 700px !important   ;">
                <a href="#">
                    <span>CƠ QUAN CHỦ QUẢN: CÔNG TY TNHH TRUYỀN THÔNG ĐẮK LẮK 24H</span><br>
                    <span>Địa chỉ: 73 Ybih Aleo - P.Tân Lợi - Tp. Buôn Ma Thuột - tỉnh Đắk Lắk</span><br>
                    <span>Điện thoại: (0262)3.959.333 - 3.959.334 - 3.959.332 - Hotline: 0942.06.6000</span><br>
                    <span>Giấy phép số: 02/GP-STTTT - Cấp ngày 27/05/2019 - Sở Thông tin và Truyền thông tỉnh Đắk Lắk</span>
                </a>
            </div>

        </section>