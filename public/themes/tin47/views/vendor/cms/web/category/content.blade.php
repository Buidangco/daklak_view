
<section id="breadcrumb-section" class="breadcrumb-section w100dt mb-30">
            <div class="container">

                <nav class="breadcrumb-nav w100dt">
                    <div class="page-name hide-on-small-only left">
                        <h4>{{ $category->name }}</h4>
                    </div>
                    <div class="nav-wrapper right">
                        <a href="#!" class="breadcrumb">Home</a>
                        <a href="#!" class="breadcrumb active">{{ $category->name }}</a>
                    </div>
                    <!-- /.nav-wrapper -->
                </nav>
                <!-- /.breadcrumb-nav -->

            </div>
            <!-- container -->
        </section>
    <section id="cateogry-section" class="cateogry-section w100dt mb-50">
            <div class="container">
                <div class="row">
              
                    <div class="col s12 m8 l8">
                        <?php
                        $number=1;
                        ?>
                        <div class="row">
                    @foreach($posts as $post)
                    <?php
                    $number++;
                    ?>
                      @continue($number <= 2)
                            <div class="col m6 s12">
                                <div class="blogs mb-30">
                                    <div class="card" style="    height: 44em;">
                                        <div class="card-image">
                                            <img src="{{ $post->getFirstMediaUrl('image') }}" alt="Image" style="height: 17em;">
                                            <a class="btn-floating center-align cmn-bgcolor halfway-fab waves-effect waves-light">
                                                <i class="icofont icofont-camera-alt"></i>
                                            </a>
                                        </div>
                                        <!-- /.card-image -->
                                        <div class="card-content w100dt">
                                            <p>
                                                 @foreach($post->categories as $metaCategory)
                                                <a href="{{$metaCategory->url}}" class="tag left w100dt l-blue mb-30">{{$metaCategory->name}}</a>
                                                @endforeach
                                            </p>
                                            <a href="{{ $post->url }}" class="card-title">
                                                {{$post->name}}
                                            </a>
                                            <p class="mb-30">
                                                {{$post->description}}
                                            </p>
                                            <ul class="post-mate-time left">
                                                <li>
                                                    <p class="hero left">
                                                        By - <a href="#" class="l-blue">SujonMaji</a>
                                                    </p>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-ui-calendar"></i>

                                                </li>
                                            </ul>

                                            <ul class="post-mate right">
                                                <li class="like">
                                                    <a href="#">
                                                        <i class="icofont icofont-heart-alt"></i> 55
                                                    </a>
                                                </li>
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.card-content -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.blogs -->
                            </div>
                            @break($number>3)
                             @endforeach
                           
                            <!-- colm6 -->

                   
                            <!-- colm6 -->
                            @if($user=__sevice_sevice_cms__Category1($category->id))
                            @foreach($user as $row)
                            <div class="col m12 s12">
                                <div class="blogs mb-30">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="{{$row->Image}}" alt="Image">
                                            <a class="btn-floating center-align cmn-bgcolor halfway-fab waves-effect waves-light">
                                                <i class="icofont icofont-camera-alt"></i>
                                            </a>
                                        </div>
                                        <!-- /.card-image -->
                                        <div class="card-content w100dt">
                                            <p>
                                                 @foreach($row->categories as $metaCategory)
                                                <a href="{{$metaCategory->url}}" class="tag left w100dt l-blue mb-30">{{$metaCategory->name}}</a>
                                                @endforeach
                                            </p>
                                            <a href="{{$post->url}}" class="card-title">
                                                {{$row->name}}
                                            </a>
                                            <p class="mb-30">
                                                 {{$row->description}}
                                            </p>
                                            <ul class="post-mate-time left">
                                                <li>
                                                    <p class="hero left">
                                                        By - <a href="#" class="l-blue">SujonMaji</a>
                                                    </p>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-ui-calendar"></i> 5 February'17
                                                </li>
                                            </ul>

                                            <ul class="post-mate right">
                                                <li class="like">
                                                    <a href="#">
                                                        <i class="icofont icofont-heart-alt"></i> 55
                                                    </a>
                                                </li>
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.card-content -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                                <!-- /.blogs -->
                            </div>
                            @endforeach
                            @endif
                           {{ $user->links() }}
                            <!-- colm6 -->


                <!--             <div class="col m6 s12">
                                <div class="blogs mb-30">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="img/fashion2.jpg" alt="Image">
                                            <a class="btn-floating center-align cmn-bgcolor halfway-fab waves-effect waves-light">
                                                <i class="icofont icofont-camera-alt"></i>
                                            </a>
                                        </div>
                                   
                                        <div class="card-content w100dt">
                                            <p>
                                                <a href="#" class="tag left w100dt l-blue mb-30">Fashion</a>
                                            </p>
                                            <a href="single-blog.html" class="card-title">
                                                Labore Etdolore Magna Aliqua Utero Ratione
                                            </a>
                                            <p class="mb-30">
                                                Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.
                                            </p>
                                            <ul class="post-mate-time left">
                                                <li>
                                                    <p class="hero left">
                                                        By - <a href="#" class="l-blue">SujonMaji</a>
                                                    </p>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-ui-calendar"></i> 5 February'17
                                                </li>
                                            </ul>

                                            <ul class="post-mate right">
                                                <li class="like">
                                                    <a href="#">
                                                        <i class="icofont icofont-heart-alt"></i> 55
                                                    </a>
                                                </li>
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                     
                                    </div>
                                  
                                </div>
                              
                            </div>
                           

                            <div class="col m6 s12">
                                <div class="blogs mb-30">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="img/travel2.jpg" alt="Image">
                                            <a class="btn-floating center-align cmn-bgcolor halfway-fab waves-effect waves-light">
                                                <i class="icofont icofont-camera-alt"></i>
                                            </a>
                                        </div>
                                      
                                        <div class="card-content w100dt">
                                            <p>
                                                <a href="#" class="tag left w100dt l-blue mb-30">Travel</a>
                                            </p>
                                            <a href="single-blog.html" class="card-title">
                                                Labore Etdolore Magna Aliqua Utero Ratione
                                            </a>
                                            <p class="mb-30">
                                                Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.
                                            </p>
                                            <ul class="post-mate-time left">
                                                <li>
                                                    <p class="hero left">
                                                        By - <a href="#" class="l-blue">SujonMaji</a>
                                                    </p>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-ui-calendar"></i> 5 February'17
                                                </li>
                                            </ul>

                                            <ul class="post-mate right">
                                                <li class="like">
                                                    <a href="#">
                                                        <i class="icofont icofont-heart-alt"></i> 55
                                                    </a>
                                                </li>
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                       
                                    </div>
                              
                                </div>
                             
                            </div>
                       

                            <div class="col m12 s12">
                                <div class="blogs mb-30">
                                    <div class="card">
                                        <div class="card-image">
                                            <img src="img/selfie.jpg" alt="Image">
                                            <a class="btn-floating center-align cmn-bgcolor halfway-fab waves-effect waves-light">
                                                <i class="icofont icofont-camera-alt"></i>
                                            </a>
                                        </div>
                                  
                                        <div class="card-content w100dt">
                                            <p>
                                                <a href="#" class="tag left w100dt l-blue mb-30">Lifestyle</a>
                                            </p>
                                            <a href="single-blog.html" class="card-title">
                                                Labore Etdolore Magna Aliqua Utero Ratione
                                            </a>
                                            <p class="mb-30">
                                                Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam.
                                            </p>
                                            <ul class="post-mate-time left">
                                                <li>
                                                    <p class="hero left">
                                                        By - <a href="#" class="l-blue">SujonMaji</a>
                                                    </p>
                                                </li>
                                                <li>
                                                    <i class="icofont icofont-ui-calendar"></i> 5 February'17
                                                </li>
                                            </ul>

                                            <ul class="post-mate right">
                                                <li class="like">
                                                    <a href="#">
                                                        <i class="icofont icofont-heart-alt"></i> 55
                                                    </a>
                                                </li>
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                       
                                    </div>
                                    
                                </div>
                              
                            </div> -->
                           
                        </div>
                       

                       <!--  <ul class="pagination w100dt">
                            <li class="waves-effect"><a href="#!"><i class="icofont icofont-simple-left"></i></a></li>
                            <li class="active"><a href="#!">1</a></li>
                            <li class="waves-effect"><a href="#!">2</a></li>
                            <li class="waves-effect"><a href="#!">3</a></li>
                            <li class="waves-effect"><a href="#!">4</a></li>
                            <li class="waves-effect"><a href="#!">5</a></li>
                            <li class="waves-effect"><a href="#!"><i class="icofont icofont-simple-right"></i></a></li>
                        </ul> -->

                    </div>  
                    <!-- colm8 -->




                    <div class="col s12 m4 l4">

                        <div class="featured-posts w100dt mb-30">
                            <div class="sidebar-title center-align">
                                <h2>Các bài viết</h2>
                            </div>
                            <!-- /.sidebar-title -->

                            <div class="sidebar-posts" style="    overflow: auto;height: 37em;">
                                 @foreach($posts as $post)
                                <div class="card">
                                    <div class="card-image mb-10">
                                        <img src="{{$post->Image}}" alt="Image">
                                        <span class="effect"></span>
                                    </div>
                                    <!-- /.card-image -->
                                    <div class="card-content w100dt">
                                        <p>
                                            <a href="#" class="tag left w100dt l-blue mb-10">{{$post->category->name}}</a>
                                        </p>
                                        <a href="single-blog.html" class="card-title">
                                           {{$post->description}}
                                        </a>
                                        <ul class="post-mate-time left">
                                            <li>
                                                <p class="hero left">
                                                    By - <a href="#" class="l-blue">{{$post->category->name}}</a>
                                                </p>
                                            </li>
                                        </ul>

                                        <ul class="post-mate right">
                                            <li class="like">
                                                <a href="#">
                                                    <i class="icofont icofont-heart-alt"></i> 55
                                                </a>
                                            </li>
                                            <li class="comment">
                                                <a href="#">
                                                    <i class="icofont icofont-comment"></i> 32
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- /.card-content -->
                                </div>
                                <!-- /.card -->
                                @endforeach
                            </div>
                            <!-- /.sidebar-posts -->

                        </div>
                        <!-- /.featured-posts -->




                        <div class="top-post w100dt mb-30">
                            <div class="sidebar-title center-align">
                                <h2>Bài viết hàng đầu</h2>
                            </div>
                            <!-- /.sidebar-title -->

                            <ul id="tabs-swipe-demo" class="top-post-tab tabs tabs-fixed-width">
                                <li class="tab"><a href="#test-swipe-1" class="active">Bài viết mới nhất</a></li>
                            </ul>

                            <div id="test-swipe-1" class="tab-contant most-view">

                                <div class="sidebar-posts">
                                     @if($post2=__sevice_sevice_cms__Category_post($post->category->id))
                                    @foreach($post2 as $row)
                                    <div class="card">
                                        <div class="card-image mb-10">
                                            <img src="{{$row->Image}}" alt="Image">
                                            <span class="effect"></span>
                                        </div>
                                        <!-- /.card-image -->
                                        <div class="card-content w100dt">
                                            <p>
                                                @foreach($row->categories as $categori)
                                                <a href="{{$categori->url}}" class="tag left w100dt l-blue mb-10">{{$categori->name}}</a>
                                                @endforeach
                                            </p>
                                            <a href="{{$row->url}}" class="card-title">
                                               {{$row->name}}
                                            </a>
                                            <ul class="post-mate-time left">
                                                <li>
                                                    <p class="hero left">
                                                        By - <a href="#" class="l-blue">SujonMaji</a>
                                                    </p>
                                                </li>
                                            </ul>

                                            <ul class="post-mate right">
                                                <li class="like">
                                                    <a href="#">
                                                        <i class="icofont icofont-heart-alt"></i> 55
                                                    </a>
                                                </li>
                                                <li class="comment">
                                                    <a href="#">
                                                        <i class="icofont icofont-comment"></i> 32
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.card-content -->
                                    </div>
                                    @endforeach
                                    @endif
                                    <!-- /.card -->
                                    <!-- /.hot-post -->
                                </div>
                                <!-- /.sidebar-posts -->

                            </div>
                            <!-- /.most-view -->
                            <!-- /.tab-contant -->
                        </div>
                        <!-- /.top-post -->
                        <!-- /.sidebar-subscribe -->
                        
                    </div>  
                    <!-- colm4 -->
                    
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </section>
          <section  id="instagram-section" class="instagram-section w100dt" style="background-image:url({{theme_url('img/bg-fotter.png')}}) !important">

            <div class="instagram-link w100dt" style="    width: 700px !important   ;">
                <a href="#">
                    <span>CƠ QUAN CHỦ QUẢN: CÔNG TY TNHH TRUYỀN THÔNG ĐẮK LẮK 24H</span><br>
                    <span>Địa chỉ: 73 Ybih Aleo - P.Tân Lợi - Tp. Buôn Ma Thuột - tỉnh Đắk Lắk</span><br>
                    <span>Điện thoại: (0262)3.959.333 - 3.959.334 - 3.959.332 - Hotline: 0942.06.6000</span><br>
                    <span>Giấy phép số: 02/GP-STTTT - Cấp ngày 27/05/2019 - Sở Thông tin và Truyền thông tỉnh Đắk Lắk</span>
                </a>
            </div>

        </section>