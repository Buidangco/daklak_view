@input(['name' => 'contact_phone', 'label' => __('theme::setting.contact_phone')])
@input(['name' => 'contact_email', 'label' => __('theme::setting.contact_email')])
@input(['name' => 'contact_address', 'label' => __('theme::setting.contact_address')])
@input(['name' => 'contact_map_title', 'label' => __('theme::setting.contact_map_title')])
@input(['name' => 'contact_map_lat', 'label' => __('theme::setting.contact_map_lat')])
@input(['name' => 'contact_map_lng', 'label' => __('theme::setting.contact_map_lng')])

<hr>

@editor([
    'name' => 'footer_copyright',
    'label' => __('theme::setting.footer_copyright'),
])
