	<footer id="footer-section" class="footer-section w100dt">
			<div class="container">

				<div class="footer-logo w100dt center-align mb-30">
					<a href="#" class="brand-logo">
						<img src="{{ theme_url('img/logo.png')}}" alt="brand-logo">
					</a>
				</div>
				<!-- /.footer-logo -->

				<ul class="footer-social-links w100dt center-align mb-30">
					<li><a href="#" class="facebook">FACEBOOK</a></li>
					<li><a href="#" class="twitter">TWITTER</a></li>
					<li><a href="#" class="google-plus">GOOGLE+</a></li>
					<li><a href="#" class="linkedin">LINKDIN</a></li>
					<li><a href="#" class="pinterest">PINTEREST</a></li>
					<li><a href="#" class="instagram">INSTAGRAM</a></li>
				</ul>

				<!-- <p class="center-align">
					<i class="icofont icofont-heart-alt l-blue"></i>  
					All Right Reserved, Deasined by 
					<a href="#" class="l-blue">HTMLmate</a>
				</p> -->

			</div>
			<!-- container -->
		</footer>
