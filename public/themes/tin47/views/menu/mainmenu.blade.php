           
            <nav class="nav-extended main-nav">
                <div class="container">
                    <div class="row">
                        <div class="nav-wrapper w100dt">

                            <div class="logo left">
                                <a href="index.html" class="brand-logo">
                                    <img src="{{ theme_url('img/logo.png')}}" alt="brand-logo">
                                </a>
                            </div>
                            <div>
                                <a href="#" data-activates="mobile-demo" class="button-collapse">
                                    <i class="icofont icofont-navigation-menu"></i>
                                </a>
                                <ul style="    width: 81% !important; " id="nav-mobile" class="main-menu center-align hide-on-med-and-down">
                                      
            @if($stickyPosts = $items)
                    @foreach($stickyPosts as $item)
                <li >
                    <a href="{{ $item->getUrl() }}" style="padding: 28px 13px !important">{{ $item->label }}</a>
                </li>
            @endforeach
                @endif
                                </ul>
                                <!-- /.main-menu -->

                                <!-- ******************** sidebar-menu ******************** -->
                                <ul class="side-nav" id="mobile-demo">
                                    <li class="snavlogo center-align"><img src="{{ theme_url('img/logo.png')}}" alt="logo"></li>
                                    <li class="active"><a href="index.html">HOME</a></li>
                                    <li><a href="cateogry.html">CATEGORIES</a></li>
                                    <li><a href="single-blog.html">SINGLE BLOG</a></li>
                                    <li><a href="contact.html">CONTACT</a></li>
                                    <li><a href="404.html">404 PAGE</a></li>
                                </ul>
                            </div>
                            <!-- main-menu -->

                            <a href="#" class="search-trigger right">
                                <i class="icofont icofont-search"></i>
                            </a>
                            <!-- search -->
                            <div id="myNav" class="overlay">
                                <a href="javascript:void(0)" class="closebtn">&times;</a>
                                <div class="overlay-content">
                                    <form>
                                        <input type="text" name="search" placeholder="Search...">
                                        <br>
                                        <button class="waves-effect waves-light" type="submit" name="action">Search</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                        <!-- /.nav-wrapper -->
                    </div>
                    <!-- row -->
                </div>
                <!-- container -->
            </nav>