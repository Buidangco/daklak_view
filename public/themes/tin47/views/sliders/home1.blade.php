<div class="slider">
                    <ul class="slides">
                        @if($slide=$sliderItems)
                        @foreach($slide as $row)
                        <li class="slider-item">
                            <img src="{{ $row->image }}" alt="Image">
                            <div class="caption center-align">
                                <a href="#" class="tag l-blue w100dt mb-30">{{ $row->name }}</a>
                                <h1 class="card-title mb-10">
                                   {{ $row->name }}
                                </h1>
                                <p>
                                   {{ $row->description }}
                                </p>
                                <a href="single-blog.html" class="custom-btn waves-effect waves-light">READ MORE</a>
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>