<?php
return [
    'panel_title' => 'Theme Setting',

    'footer_copyright' => 'Footer Copyright',

    'contact_email'     => 'Contact Email',
    'contact_phone'     => 'Contact Phone',
    'contact_address'   => 'Contact Address',
    'contact_map_title' => 'Map Title',
    'contact_map_lat'   => 'Map Lat',
    'contact_map_lng'   => 'Map Lng',
];
