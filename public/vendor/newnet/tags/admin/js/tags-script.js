$(document).ready(function () {
    'use strict';

    $("select.tags").each(function () {
        let placeholder = $(this).attr('placeholder');

        $(this).select2({
            placeholder: placeholder,
            tags: true,
            tokenSeparators: [',']
        });
    });
});
