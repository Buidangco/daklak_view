$(document).ready(function () {
    "use strict"; // Start of use strict

    $('input[data-mask]').each(function () {
        let data = $(this).data();

        if (data.mask === 'money') {
            $(this).mask('000.000.000.000.000', {reverse: true});
        } else {
            $(this).mask(data.mask, data.maskOption);
        }
    }).closest('form').on('submit', function (e) {
        $('input[data-mask]').unmask();
    })
});
