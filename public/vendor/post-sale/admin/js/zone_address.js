if ($('#province_id').val() !== '') {
    $.ajax({
        url: adminPath + '/zone/province/' + $('#province_id').val() + '/districts',
        data : {
            district_id: $('#old_district_id').val()
        },
        success: function (response) {
            $('#district_id').html(response.districts);
            $.ajax({
                url: adminPath + '/zone/district/' + $('#old_district_id').val() + '/townships',
                data: {
                    township_id: $('#old_township_id').val()
                },
                success: function (response) {
                    $('#township_id').html(response.townships)
                }
            });
        }
    });
}

$('body').on('change', '#province_id', function (e) {
    $.ajax({
        url: adminPath + '/zone/province/' + $(this).val() + '/districts',
        success: function (response) {
            $('#district_id').html(response.districts);

            $.ajax({
                url: adminPath + '/zone/district/' + response.firstItemId + '/townships',
                success: function (response) {
                    $('#township_id').html(response.townships);
                }
            });
        }
    });
});

$('body').on('change', '#district_id', function (e) {
    $.ajax({
        url: adminPath + '/zone/district/' + $(this).val() + '/townships',
        success: function (response) {
            $('#township_id').html(response.townships);
        }
    });
});
